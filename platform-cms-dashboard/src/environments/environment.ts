// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  title: 'Taiho CMS Development',
  api: {
    url: 'http://localhost:1337',
  },
  froala: {
    key: 'VB8D7E5G4eC3I3B7B10C4B4C3A3C2I2xuiiiD-11ceeombB-13bqzdywcC-16vhvoiC-7xioeE4gjk==',
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
