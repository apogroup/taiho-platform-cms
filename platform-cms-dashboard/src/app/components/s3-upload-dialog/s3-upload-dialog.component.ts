import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-s3-upload-dialog',
  templateUrl: './s3-upload-dialog.component.html',
  styleUrls: ['./s3-upload-dialog.component.scss']
})
export class S3UploadDialog implements OnInit {

  /////////////////////////////////////////////////////////////////////////
  // Variables
  /////////////////////////////////////////////////////////////////////////

  public message: any = '';

  /////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////

    constructor(
      public dialogRef: MatDialogRef<S3UploadDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any,
  ) { 
      // dialogRef.disableClose = true;
  }

  ngOnInit() {

    this.message = this.data.message;

  }

  /////////////////////////////////////////////////////////////////////////
  // Methods
  /////////////////////////////////////////////////////////////////////////

  //-------------------------------------------------------------------
  //  Setters
  //-------------------------------------------------------------------



  //-------------------------------------------------------------------
  //  Dialog
  //-------------------------------------------------------------------

  close() {
    this.dialogRef.close();
  }

}
