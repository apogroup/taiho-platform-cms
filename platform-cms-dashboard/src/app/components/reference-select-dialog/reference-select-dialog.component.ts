import { Component, OnInit, Inject, NgZone, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSort, MatTableDataSource } from '@angular/material';

import { ReferenceService } from 'app/services/dashboard/reference.service';

import { Reference } from 'app/models/reference.model';
import { ReferenceVersion } from 'app/models/referenceversion.model';


@Component({
    selector: 'reference-select-dialog',
    templateUrl: 'reference-select-dialog.component.html',
    styleUrls: ['reference-select-dialog.component.scss']
})
export class ReferenceSelectDialog implements OnInit {

    @ViewChild('masterverisonsTable', { read: MatSort }) MasterVersionsTableSort: MatSort;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public references: Array<Reference> = [];
    public versions: Array<ReferenceVersion> = [];
    public masterversions: Array<ReferenceVersion> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public MasterVersionsTableDisplayedColumns: string[] = [
        'Select',
        'UniqueID',
        'ShortCitation',
        'DisplayName',
    ];
    public MasterVersionsTableDataSource = new MatTableDataSource([]);

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        public dialogRef: MatDialogRef<ReferenceSelectDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private referenceService: ReferenceService,
        private zone: NgZone
    ) { 
        // dialogRef.disableClose = true;
    }

    ngOnInit() {

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        if (this.data && this.data.selectedText) {
            this.filterValue = this.data.selectedText;
            this.applyFilter(this.filterValue);
        }

        //-------------------------------------------------------------------
        // Subscribe to All References
        //-------------------------------------------------------------------
        // Extrapolate Reference MasterVersion[s] to masterversions
        //-------------------------------------------------------------------

        this.subscriptions.references = this.referenceService.references$.subscribe(response => {
            this.zone.run(() => {
                this.references = [...response];
                this.masterversions = [...this.references].filter(reference => reference.MasterVersion).map(reference => {
                    let index = [...this.versions].findIndex(version => reference.MasterVersion.id == version.id);
                    if (index > -1) return this.versions[index];
                    return;
                });
                if (this.versions.length !== 0) {
                    this.setMasterVersionsTable([...this.masterversions]);
                }
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to All ReferenceVersions
        //-------------------------------------------------------------------

        this.subscriptions.referenceversions = this.referenceService.referenceversions$.subscribe(response => {
            this.zone.run(() => {
                this.versions = [...response];
                this.masterversions = [...this.references].filter(reference => reference.MasterVersion).map(reference => {
                    let index = [...this.versions].findIndex(version => reference.MasterVersion.id == version.id);
                    if (index > -1) return this.versions[index];
                    return;
                });
                this.setMasterVersionsTable([...this.masterversions]);
            });
        });

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setMasterVersionsTable([...this.masterversions]);
        });

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        this.applyFilter(this.filterValue);

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------
    setMasterVersionsTable(data: Array<ReferenceVersion>) {
        this.MasterVersionsTableDataSource = new MatTableDataSource(data);
        this.MasterVersionsTableDataSource.sort = this.MasterVersionsTableSort;
        this.MasterVersionsTableDataSource.sort.active = 'createdAt';
        this.MasterVersionsTableDataSource.sort.direction = 'desc';
        this.MasterVersionsTableDataSource.filterPredicate = (data: any, filter: string) => {
            let ref = [
                data.UniqueID,
                data.ShortCitation,
                data.Citation,
                data.DisplayName,
                data.title,
            ];
            let dataString = JSON.stringify(ref);
            let filterSubstrings = filter.split(" ");
            return filterSubstrings.some(textBlock => dataString.toLowerCase().includes(textBlock.toLowerCase()))
        }
    }

    applyFilter(filterValue: string) {
        this.MasterVersionsTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.MasterVersionsTableDataSource.filter = '';
    }
    
    selectReference(reference) {
        this.dialogRef.close(reference);
    }

    //-------------------------------------------------------------------
    //  Dialog
    //-------------------------------------------------------------------

    // onNoClick(): void {
    //     this.dialogRef.close();
    // }

    close() {
        this.dialogRef.close();
    }

}