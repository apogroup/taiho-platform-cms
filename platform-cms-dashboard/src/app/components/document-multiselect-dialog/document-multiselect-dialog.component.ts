import { Component, OnInit, Inject, NgZone, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSort, MatTableDataSource } from '@angular/material';

import { DocumentService } from 'app/services/dashboard/document.service';

import { Document } from 'app/models/document.model';
import { DocumentVersion } from 'app/models/documentversion.model';


@Component({
    selector: 'document-multiselect-dialog',
    templateUrl: 'document-multiselect-dialog.component.html',
    styleUrls: ['document-multiselect-dialog.component.scss']
})
export class DocumentMultiSelectDialog implements OnInit {

    @ViewChild('masterverisonsTable', { read: MatSort }) MasterVersionsTableSort: MatSort;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public documents: Array<Document> = [];
    public versions: Array<DocumentVersion> = [];
    public masterversions: Array<DocumentVersion> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public MasterVersionsTableDisplayedColumns: string[] = [
        'Select',
        'Title',
        'FilePath',
        'SourceFilePath',
        'References',
    ];
    public MasterVersionsTableDataSource = new MatTableDataSource([]);
    public selection = new SelectionModel<DocumentVersion>(true, []);

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        public dialogRef: MatDialogRef<DocumentMultiSelectDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private documentService: DocumentService,
        private zone: NgZone
    ) { 
        // dialogRef.disableClose = true;
    }

    ngOnInit() {

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        if (this.data && this.data.selectedText) {
            this.filterValue = this.data.selectedText;
            this.applyFilter(this.filterValue);
        }

        //-------------------------------------------------------------------
        // Subscribe to All Documents
        //-------------------------------------------------------------------
        // Extrapolate Document MasterVersion[s] to masterversions
        //-------------------------------------------------------------------

        this.subscriptions.documents = this.documentService.documents$.subscribe(response => {
            this.zone.run(() => {
                this.documents = [...response];
                this.masterversions = [...this.documents].filter(document => document.MasterVersion).map(document => {
                    let index = [...this.versions].findIndex(version => document.MasterVersion.id == version.id);
                    if (index > -1) return this.versions[index];
                    return;
                });
                if (this.versions.length !== 0) {
                    this.setMasterVersionsTable([...this.masterversions]);
                }
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to All DocumentVersions
        //-------------------------------------------------------------------

        this.subscriptions.documentversions = this.documentService.documentversions$.subscribe(response => {
            this.zone.run(() => {
                this.versions = [...response];
                this.masterversions = [...this.documents].filter(document => document.MasterVersion).map(document => {
                    let index = [...this.versions].findIndex(version => document.MasterVersion.id == version.id);
                    if (index > -1) return this.versions[index];
                    return;
                });
                this.setMasterVersionsTable([...this.masterversions]);
            });
        });

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setMasterVersionsTable([...this.masterversions]);
        });

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        this.applyFilter(this.filterValue);

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------
    setMasterVersionsTable(data: Array<DocumentVersion>) {
        this.MasterVersionsTableDataSource = new MatTableDataSource(data);
        this.MasterVersionsTableDataSource.sort = this.MasterVersionsTableSort;
        this.MasterVersionsTableDataSource.sort.active = 'createdAt';
        this.MasterVersionsTableDataSource.sort.direction = 'desc';
        this.MasterVersionsTableDataSource.filterPredicate = (data: any, filter: string) => {
            let doc = [
                data.Title,
                data.Filename,
            ];
            let dataString = JSON.stringify(doc);
            let filterSubstrings = filter.split(" ");
            return filterSubstrings.some(textBlock => dataString.toLowerCase().includes(textBlock.toLowerCase()))
        }

        //-------------------------------------------------------------------
        // Table Selection
        //-------------------------------------------------------------------
        
        let selected = [];

        if (this.data && this.data.documentIds) {
            let prevSelected = this.data.documentIds;
            selected = data.filter(masterversion => {
                return prevSelected.includes(masterversion.Document.id);
            });
        }
        
        this.selection = new SelectionModel<DocumentVersion>(true, selected);
    }

    applyFilter(filterValue: string) {
        this.MasterVersionsTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.MasterVersionsTableDataSource.filter = '';
    }
    
    // selectDocument(document) {
    //     this.dialogRef.close(this.selection);
    // }

    //-------------------------------------------------------------------
    //  Dialog
    //-------------------------------------------------------------------

    // onNoClick(): void {
    //     this.dialogRef.close();
    // }

    saveClose() {
        let selection = [ ...this.selection.selected ];
        this.dialogRef.close(selection);
    }

    close() {
        this.dialogRef.close();
    }

}