import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { environment } from 'environments/environment';

declare var $: any;
declare var window: any;

@Component({
    selector: 'htmleditor-dialog',
    templateUrl: 'htmleditor-dialog.component.html',
    styleUrls: ['htmleditor-dialog.component.scss']
})
export class HTMLEditorDialog implements OnInit {

    @ViewChild('ngxEditor') ngxEditor: any;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    public model;

    public editorOptions: Object = {
        key: environment.froala.key,
        toolbarButtons: ['bold', 'italic', 'underline', '|', 'superscript', 'subscript', '|', 'referenceSelect', '|', 'undo', 'redo', '|', 'html'],
        pluginsEnabled: ['reference', 'charCounter', 'codeBeautifier', 'codeView'],
        multiLine: true,
        charCounterCount: true,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        zIndex: 999,
        codeMirror: window.CodeMirror,
        codeMirrorOptions: {
            indentWithTabs: true,
            lineNumbers: true,
            lineWrapping: true,
            mode: 'text/html',
            // theme: 'monokai',
            tabMode: 'indent',
            tabSize: 4
        }
    };

    constructor(
        public dialogRef: MatDialogRef<HTMLEditorDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() {
        this.model = this.data.control.value;
        this.editorOptions = { ...this.editorOptions, ...this.data.options };
    }

    onNoClick(): void {
        this.dialogRef.close(this.model);
    }

    close() {
        this.dialogRef.close(this.model);
    }

}