import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';

import { WebsocketService } from 'app/services/websocket/websocket.service';
import { ConnectionStatus } from 'app/models/connection.status.model';

@Component({
    selector: 'app-connection',
    templateUrl: 'connection.component.html',
    styleUrls: ['connection.component.scss']
})
export class ConnectionComponent implements OnDestroy, OnInit {

    /////////////////////////////////////////////////////////////////////////
    // Component Variables
    /////////////////////////////////////////////////////////////////////////

    public connection: ConnectionStatus = new ConnectionStatus;

    private subscription: any;
    
    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private websocket: WebsocketService,
        private zone: NgZone,
    ) {}

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Subscribe to Subscription
        //-------------------------------------------------------------------

        this.subscription = this.websocket.connection$.subscribe(response => { 
            this.zone.run(() => {
                this.connection = response;
            });
        });

    }

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscription
        //-------------------------------------------------------------------

        this.subscription.unsubscribe();

    }

}