import { Component, OnInit, OnDestroy, ViewChild, ViewChildren, QueryList, NgZone } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';

import { FormBuilder, FormArray, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { GlossaryService } from 'app/services/dashboard/glossary.service';

import { GlossaryTerm } from 'app/models/glossaryterm.model';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';

import { environment } from 'environments/environment';

declare var $: any;
declare var window: any;

@Component({
    selector: 'dashboard-glossary',
    templateUrl: './glossary.component.html',
    styleUrls: ['./glossary.component.scss']
})
export class GlossaryComponent implements OnInit, OnDestroy {

    @ViewChild('termsTable', { read: MatSort }) TermsTableSort: MatSort;
    @ViewChild('tablePaginator') TermsTablePaginator: MatPaginator;
    @ViewChildren('tableRowTermInputs') tableRowTermInputs: QueryList<any>;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public terms: Array<GlossaryTerm> = [];

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';

    //-------------------------------------------------------------------
    // Form Variables Term
    //-------------------------------------------------------------------
    public createForm: FormGroup;

    //-------------------------------------------------------------------
    // Form Variables Term Table
    //-------------------------------------------------------------------
    public TermsTableForm: FormGroup;
    public TermsTableRowForms: FormArray = new FormArray([]);
    public TermsTableRowEditState: Array<boolean> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public TermsTableDisplayedColumns: string[] = [
        'Term',
        'Definition',
        'Disabled',
        'createdAt',
        'updatedAt',
        // 'id',
        'Actions',
    ];
    public TermsTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Froala Editor Options
    //-------------------------------------------------------------------
    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', '|', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: [],
        multiLine: false,
        charCounterCount: false,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        theme: 'dark',
        codeMirror: window.CodeMirror,
        codeMirrorOptions: {
            indentWithTabs: true,
            lineNumbers: true,
            lineWrapping: true,
            mode: 'text/html',
            // theme: 'monokai',
            tabMode: 'indent',
            tabSize: 4
        }
    };

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private htmlEditorDialogRef;

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private modelService: ModelService,
        private glossaryService: GlossaryService,

        private formBuilder: FormBuilder,
        private zone: NgZone,
        public dialog: MatDialog,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("GlossaryComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("GlossaryComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------
        this.applyFilter(this.filterValue);

        //-------------------------------------------------------------------
		// Subscribe to Subscriptions
		//-------------------------------------------------------------------

		// this.glossaryService.subscribeToTerms();
        
        //-------------------------------------------------------------------
        // Set Form Variables Term Table
        //-------------------------------------------------------------------

        this.TermsTableRowForms = this.formBuilder.array([]);
        this.TermsTableForm = this.formBuilder.group({ 'TermsTableRowForms': this.TermsTableRowForms });

        //-------------------------------------------------------------------
        // Subscribe to All GlossaryTerms
        //-------------------------------------------------------------------

        this.subscriptions.terms = this.glossaryService.terms$.subscribe(response => {
            this.zone.run(() => {
                this.terms = [...response];
                this.setTermsTable([...response]);
                this.setTermsTableRowForms([...response]);
            });
        });

        //-------------------------------------------------------------------
        // Set Form Variables Term
        //-------------------------------------------------------------------
        
        this.createForm = this.formBuilder.group({
            ...new GlossaryTerm,
        });
        this.createForm.get('Term').setValidators([Validators.required]);

    }
    

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.terms.unsubscribe();

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setTermsTable([...this.terms]);
        });

        //-------------------------------------------------------------------
        // Subscribe to children
        //-------------------------------------------------------------------
        this.tableRowTermInputs.changes.subscribe(change => this.tableRowTermInputs = change);

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        this.applyFilter(this.filterValue);

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------

    setTermsTable(data: Array<GlossaryTerm>) {
        this.TermsTableDataSource = new MatTableDataSource(data);
        this.TermsTableDataSource.paginator = this.TermsTablePaginator;
        this.TermsTableDataSource.sort = this.TermsTableSort;
        this.TermsTableDataSource.sort.active = 'createdAt';
        this.TermsTableDataSource.sort.direction = 'desc';
    }

    setTermsTableRowForms(data: Array<GlossaryTerm>) {
        this.zone.runOutsideAngular(() => {
            for (let term of data) {
                let row = this.formBuilder.group({ ...term });
                row.get('Term').setValidators([Validators.required]);
                this.TermsTableRowForms.controls[term.id] = row;
            };
        });
    }

    setTableRowCellInputFocus(rowId, control) {
        setTimeout(() => {
            let index = this.tableRowTermInputs.toArray().findIndex(el => el.nativeElement.id == `${rowId}-${control}`);
            if (index !== -1) { this.tableRowTermInputs.toArray()[index].nativeElement.focus(); };
        }, 100); // DOM input initialization (ngIf)
    }

    applyFilter(filterValue: string) {
        this.TermsTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.TermsTableDataSource.filter = '';
    }

    resetTermsTableRowForm(data: GlossaryTerm) {
        let row = this.formBuilder.group({ ...data });
        row.get('Term').setValidators([Validators.required]);
        this.TermsTableRowForms.controls[data.id] = row;
    }

    resetCreateForm() {
        let reset = { ...new GlossaryTerm };
        this.createForm.get('Term').reset();
        this.createForm.get('Definition').reset();
        this.createForm.get('Term').setValue(reset.Term);
        this.createForm.get('Definition').setValue(reset.Definition);
        this.createForm.get('Disabled').setValue(reset.Disabled);
        this.createForm.markAsPristine();
    }

    //-------------------------------------------------------------------
    //  CRUD
    //-------------------------------------------------------------------

    createTerm() {
        if (this.createForm.valid) {
            let term = this.createForm.value;
            this.glossaryService.createTerm(term);
            this.resetCreateForm();
        } else {
            this.createForm.get('Term').markAsTouched();
        }
    }

    updateTerm(term) {
        this.glossaryService.updateTerm(term);
    }

    deleteTerm(termElement) {
        let term = { ...termElement };
        this.glossaryService.deleteTerm(term);
    }

    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////
    
    destroyTerms() {
        this.glossaryService.destroyTerms();
    }

    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------

    openHTMLEditorDialog(control: AbstractControl, options?: Object): void {
        this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
            width: '50%',
            data: {
                control,
                options
            }
        });

        this.htmlEditorDialogRef.afterClosed().subscribe(data => {
            if (data !== control.value) { control.markAsDirty(); }
            if (data && data !== '') { control.setValue(data); }
        });
    }

}