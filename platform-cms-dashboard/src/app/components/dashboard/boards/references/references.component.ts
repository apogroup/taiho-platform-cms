import { Component, OnInit, OnDestroy, ViewChild, ViewChildren, QueryList, NgZone } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource, MatSortable } from '@angular/material';
import { MatDialog } from '@angular/material';

import { FormBuilder, FormArray, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { ReferenceService } from 'app/services/dashboard/reference.service';
import { TagService } from 'app/services/dashboard/tag.service';
import { S3ObjectService } from 'app/services/dashboard/s3object.service';

import { Reference } from 'app/models/reference.model';
import { ReferenceVersion } from 'app/models/referenceversion.model';

import { Tag } from 'app/models/tag.model';
import { Subtag } from 'app/models/subtag.model';

import { S3Object } from 'app/models/s3object.model';

import { ReferenceVersionDialog } from 'app/components/dashboard/boards/references/reference-version-dialog/reference-version-dialog.component';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';
import { FileSelectDialog } from 'app/components/file-select-dialog/file-select-dialog.component';

import { environment } from 'environments/environment';

import pick from 'lodash/pick';
import isBoolean from 'lodash/isBoolean';
import isNil from 'lodash/isNil';
import isString from 'lodash/isString';
import traverse from 'traverse';

declare var $: any;

@Component({
    selector: 'dashboard-references',
    templateUrl: './references.component.html',
    styleUrls: ['./references.component.scss']
})
export class ReferencesComponent implements OnInit, OnDestroy {

    @ViewChild('masterverisonsTable', { read: MatSort }) MasterVersionsTableSort: MatSort;
    @ViewChild('tablePaginator') MasterVersionsTablePaginator: MatPaginator;
    @ViewChildren('tableRowReferenceMasterVersionInputs') tableRowReferenceMasterVersionInputs: QueryList<any>;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public references: Array<Reference> = [];
    public versions: Array<ReferenceVersion> = [];
    public masterversions: Array<ReferenceVersion> = [];
    
    public tags: Array<Tag> = [];
    public subtags: Array<Subtag> = [];

    public s3objects: Array<S3Object> = [];

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';

    //-------------------------------------------------------------------
    // Form Variables Reference 
    //-------------------------------------------------------------------
    public createForm: FormGroup;

    //-------------------------------------------------------------------
    // Form Variables Reference Table
    //-------------------------------------------------------------------
    public MasterVersionsTableForm: FormGroup;
    public MasterVersionsTableRowForms: FormArray = new FormArray([]);
    public MasterVersionsTableRowEditState: Array<boolean> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public TableColumnDefinitions = [
        { def: 'UniqueID', name: 'UID', displayed: true, hideable: false },
        { def: 'VersionId', name: 'Version', displayed: true, hideable: false },
        { def: 'DisplayName', name: 'Display Name', displayed: true, hideable: true },
        { def: 'Title', name: 'Title', displayed: false, hideable: true },
        { def: 'ShortCitation', name: 'Short Citation', displayed: true, hideable: true },
        { def: 'Citation', name: 'Citation', displayed: false, hideable: true },
        { def: 'FilePath', name: 'File', displayed: true, hideable: true },
        { def: 'AnnotatedFilePath', name: 'Annotated File', displayed: false, hideable: true },
        { def: 'SourceFilePath', name: 'Source File', displayed: false, hideable: true },
        { def: 'Tags', name: 'Tags', displayed: false, hideable: true },
        { def: 'Subtags', name: 'Subtags', displayed: false, hideable: true },
        { def: 'Annotated', name: 'Annotated', displayed: false, hideable: true },
        { def: 'Disabled', name: 'Hidden', displayed: false, hideable: true },
        { def: 'Cache', name: 'Cache', displayed: false, hideable: true },
        { def: 'createdAt', name: 'Created', displayed: true, hideable: false },
        { def: 'updatedAt', name: 'Last Updated', displayed: false, hideable: true },
        { def: 'Actions', name: 'Actions', displayed: true, hideable: false }
    ];
    public MasterVersionsTableDisplayedColumns: string[] = [
        'UniqueID',
        'VersionId',
        'DisplayName',
        'Title',
        'ShortCitation',
        'Citation',
        'FilePath',
        'AnnotatedFilePath',
        'SourceFilePath',
        'Tags',
        'Subtags',
        'Annotated',
        'Disabled',
        'Cache',
        'createdAt',
        'updatedAt',
        'Actions',
    ];
    public MasterVersionsTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private htmlEditorDialogRef;
    private referenceVersionDialogRef;
    private fileSelectDialogRef;

    //-------------------------------------------------------------------
    // Froala Editor Options
    //-------------------------------------------------------------------
    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: [],
        multiLine: false,
        charCounterCount: false, 
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        theme: 'dark',
    };

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private modelService: ModelService,
        private referenceService: ReferenceService,
        private tagService: TagService,
        private s3ObjectService: S3ObjectService,

        private formBuilder: FormBuilder,
        private zone: NgZone,
        public dialog: MatDialog,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("ReferencesComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("ReferencesComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Subscribe to Subscriptions
        //-------------------------------------------------------------------

        // this.referenceService.subscribeToReferences();
        // this.referenceService.subscribeToReferenceVersions();

        //-------------------------------------------------------------------
        // Set Form Variables Reference Table
        //-------------------------------------------------------------------
        
        this.MasterVersionsTableRowForms = this.formBuilder.array([]);
        this.MasterVersionsTableForm = this.formBuilder.group({ 'MasterVersionsTableRowForms': this.MasterVersionsTableRowForms });
        // this.log('Doc Table Forms', this.MasterVersionsTableForm);

        //-------------------------------------------------------------------
        // Subscribe to All References
        //-------------------------------------------------------------------
        // Extrapolate Reference MasterVersion[s] to masterversions
        //-------------------------------------------------------------------

        this.subscriptions.references = this.referenceService.references$.subscribe(response => {
            this.zone.run(() => {
                this.references = [...response];
                this.masterversions = [...this.references].filter(reference => reference.MasterVersion).map(reference => {
                    let index = [...this.versions].findIndex(version => reference.MasterVersion.id == version.id);
                    if (index > -1) return this.versions[index];
                    return;
                });
                if(this.versions.length !== 0) {
                    this.setMasterVersionsTable([...this.masterversions]);
                    this.setMasterVersionsTableRowForms([...this.masterversions]);
                }
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to All ReferenceVersions
        //-------------------------------------------------------------------

        this.subscriptions.referenceversions = this.referenceService.referenceversions$.subscribe(response => {
            this.zone.run(() => {
                this.versions = [...response].map(version => {
                    if(!version.FilePath) { version.FilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.AnnotatedFilePath) { version.AnnotatedFilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.SourceFilePath) { version.SourceFilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.ThumbnailPath) { version.ThumbnailPath = { key: null, eTag: null, lastModified: null, size: null } }
                    return version;
                });
                this.masterversions = [...this.references].filter(reference => reference.MasterVersion).map(reference => {
                    let index = [...this.versions].findIndex(version => reference.MasterVersion.id == version.id);
                    if (index > -1) return this.versions[index];
                    return;
                });
                this.setMasterVersionsTable([...this.masterversions]);
                this.setMasterVersionsTableRowForms([...this.masterversions]);
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to Tags & Subtags
        //-------------------------------------------------------------------

        this.subscriptions.tags = this.tagService.tags$.subscribe(response => {
            this.zone.run(() => {
                this.tags = [...response];

                this.subtags = [];
                [...response].forEach(tag => tag.Subtags.forEach(subtag => this.subtags.push(subtag)));
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to S3Objects
        //-------------------------------------------------------------------

        this.subscriptions.s3objects = this.s3ObjectService.s3objects$.subscribe(response => {
            this.zone.run(() => {
                this.s3objects = [...response];
            })
        })

        //-------------------------------------------------------------------
        // Set Form Variables Reference
        //-------------------------------------------------------------------
        this.createForm = this.formBuilder.group({
            'TagsSelect': [],
            'SubtagsSelect': [],
            ...new ReferenceVersion,
        });
        this.createForm.get('UniqueID').setValidators([Validators.required, this.uniqueIDValidator.bind(this)]);
        this.createForm.get('DisplayName').setValidators([Validators.required]);
        this.createForm.get('Title').setValidators([Validators.required]);
        this.createForm.get('VersionId').setValidators([Validators.required]);
        this.createForm.get('ShortCitation').setValidators([Validators.required]);
        // this.log('CF', this.createForm);

    }
    

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.references.unsubscribe();
        this.subscriptions.referenceversions.unsubscribe();
        this.subscriptions.tags.unsubscribe();
        this.subscriptions.s3objects.unsubscribe();

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setMasterVersionsTable([...this.masterversions]);
        });

        //-------------------------------------------------------------------
        // Subscribe to children
        //-------------------------------------------------------------------
        // this.tableRowTagInputs.changes.subscribe(change => this.tableRowTagInputs = change);

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------
    getDisplayedColumns(): string[] {
        return this.TableColumnDefinitions
            .filter(cd => cd.displayed == true)
            .map(cd => cd.def);
    }

    toggleColumnDisplayed(event, column) {
        let index = this.TableColumnDefinitions.findIndex(item => item.def === column.def);
        if (index > -1) { this.TableColumnDefinitions[index].displayed = event.checked; }
    }

    setMasterVersionsTable(data: Array<ReferenceVersion>) {
        this.MasterVersionsTableDataSource = new MatTableDataSource(data);
        this.MasterVersionsTableDataSource.paginator = this.MasterVersionsTablePaginator;
        this.MasterVersionsTableDataSource.sort = this.MasterVersionsTableSort;
        this.MasterVersionsTableDataSource.sort.active = 'createdAt';
        this.MasterVersionsTableDataSource.sort.direction = 'desc';
        this.MasterVersionsTableDataSource.filterPredicate = (data, filter) => {                         

            const normalizeString = (string) => string.toString().trim().toLowerCase();

            const filteredData = pick(data, this.getDisplayedColumns());

            const content = traverse(filteredData).reduce(function(content, node){
                
                try{
                    if(this.isLeaf && !(isNil(node) || isBoolean(node) || this.path.includes("FileIndex"))){
                        return content.concat(normalizeString(node));
                    } 
                } catch(error){
                    this.log('Content Traverse Error', error);
                }
                
                return content;

            }, "");

            return content.includes(filter);

        }
    }

    setMasterVersionsTableRowForms(data: Array<ReferenceVersion>) {
        this.zone.runOutsideAngular(() => {
            for (let referenceReferenceVersion of data) {

                if (!referenceReferenceVersion.Tags) { referenceReferenceVersion.Tags = []; }; 
                if (!referenceReferenceVersion.Subtags) { referenceReferenceVersion.Subtags = []; };

                let referenceversion: any = { ...referenceReferenceVersion };
                    referenceversion.TagsSelect = [];
                    referenceversion.SubtagsSelect = [];

                let row = this.formBuilder.group({
                    ...referenceversion
                });

                row.get('TagsSelect').setValue([...referenceversion.Tags.map(tag => tag.id)]);
                row.get('SubtagsSelect').setValue([...referenceversion.Subtags.map(subtag => subtag.id)]);

                row.get('UniqueID').disable();
                row.get('UniqueID').setValidators([Validators.required]);
                row.get('DisplayName').setValidators([Validators.required]);
                row.get('Title').setValidators([Validators.required]);
                row.get('VersionId').setValidators([Validators.required]);
                row.get('ShortCitation').setValidators([Validators.required]);

                this.MasterVersionsTableRowForms.controls[referenceReferenceVersion.id] = row;
                // this.log('Doc Table Forms', this.MasterVersionsTableForm);

            };
        });
    }

    setTableRowCellInputFocus(rowId, control) {
        setTimeout(() => {
            let index = this.tableRowReferenceMasterVersionInputs.toArray().findIndex(el => el.nativeElement.id == `${rowId}-${control}`);
            if (index !== -1) { this.tableRowReferenceMasterVersionInputs.toArray()[index].nativeElement.focus(); };
        }, 100); // DOM input initialization (ngIf)
    }

    applyFilter(filterValue: string) {
        this.MasterVersionsTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.MasterVersionsTableDataSource.filter = '';
    }

    resetMasterVersionsTableRowForm(data: ReferenceVersion) {
        if (!data.Tags) { data.Tags = []; };
        if (!data.Subtags) { data.Subtags = []; };

        let referenceversion: any = { ...data };
            referenceversion.TagsSelect = [];
            referenceversion.SubtagsSelect = [];

        let row = this.formBuilder.group({
            ...referenceversion
        });

        row.get('TagsSelect').setValue([...referenceversion.Tags.map(tag => tag.id)]);
        row.get('SubtagsSelect').setValue([...referenceversion.Subtags.map(subtag => subtag.id)]);

        row.get('UniqueID').disable();
        row.get('UniqueID').setValidators([Validators.required]);
        row.get('DisplayName').setValidators([Validators.required]);
        row.get('Title').setValidators([Validators.required]);
        row.get('VersionId').setValidators([Validators.required]);
        row.get('ShortCitation').setValidators([Validators.required]);

        this.MasterVersionsTableRowForms.controls[data.id] = row;
    }

    resetCreateForm(){
        let reset = { ...new ReferenceVersion };
        this.createForm.get('UniqueID').reset(reset.UniqueID);
        this.createForm.get('DisplayName').reset(reset.DisplayName);
        this.createForm.get('Title').reset(reset.Title);
        this.createForm.get('VersionId').reset(reset.VersionId);
        this.createForm.get('ShortCitation').reset(reset.ShortCitation);
        this.createForm.get('Citation').reset(reset.Citation);
        this.createForm.get('Tags').reset(reset.Tags);
        this.createForm.get('Subtags').reset(reset.Subtags);
        this.createForm.get('TagsSelect').reset([]);
        this.createForm.get('SubtagsSelect').reset([]);
        this.createForm.get('Annotated').reset(reset.Annotated);
        this.createForm.get('Disabled').reset(reset.Disabled);
        this.createForm.get('Cache').reset(reset.Cache);
        this.createForm.get('FilePath').reset(reset.FilePath);
        this.createForm.get('AnnotatedFilePath').reset(reset.AnnotatedFilePath);
        this.createForm.get('SourceFilePath').reset(reset.SourceFilePath);
        this.createForm.get('ThumbnailPath').reset(reset.ThumbnailPath);
        this.createForm.get('UniqueID').setValue(reset.UniqueID);
        this.createForm.get('DisplayName').setValue(reset.DisplayName);
        this.createForm.get('Title').setValue(reset.Title);
        this.createForm.get('VersionId').setValue(reset.VersionId);
        this.createForm.get('ShortCitation').setValue(reset.ShortCitation);
        this.createForm.get('Citation').setValue(reset.Citation);
        this.createForm.get('Tags').setValue(reset.Tags);
        this.createForm.get('Subtags').setValue(reset.Subtags);
        this.createForm.get('TagsSelect').setValue([]);
        this.createForm.get('SubtagsSelect').setValue([]);
        this.createForm.get('Annotated').setValue(reset.Annotated);
        this.createForm.get('Disabled').setValue(reset.Disabled);
        this.createForm.get('Cache').setValue(reset.Cache);
        this.createForm.get('FilePath').setValue(reset.FilePath);
        this.createForm.get('AnnotatedFilePath').setValue(reset.FilePath);
        this.createForm.get('SourceFilePath').setValue(reset.FilePath);
        this.createForm.get('ThumbnailPath').setValue(reset.ThumbnailPath);
        this.createForm.markAsPristine();
    }

    uniqueIDValidator(control: FormControl) {
        let uniqueId = control.value;
        let exist = this.references.some((reference: Reference) => reference.UniqueID == uniqueId );
        if(exist) {
            return {
                uniqueIdAlreadyExist: true
            }
        }
        return null; // PASSES 
    }

    //-------------------------------------------------------------------
    //  CRUD
    //-------------------------------------------------------------------

    createReference() {
        if(this.createForm.valid) {
            let referenceversion = this.createForm.value;
                referenceversion.Tags = referenceversion.TagsSelect || [];
                referenceversion.Subtags = referenceversion.SubtagsSelect || [];
                delete referenceversion.TagsSelect;
                delete referenceversion.SubtagsSelect;

            let s3FileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === referenceversion.FilePath.key) };
            let s3AnnotatedFileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === referenceversion.AnnotatedFilePath.key) };
            let s3SourceFileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === referenceversion.SourceFilePath.key) };
            let s3ThumbnailObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === referenceversion.ThumbnailPath.key) };

            referenceversion.Filename = s3FileObject.key || "";
            referenceversion.FilePath = s3FileObject.id || null;
            referenceversion.AnnotatedFilePath = s3AnnotatedFileObject.id || null;
            referenceversion.SourceFilePath = s3SourceFileObject.id || null;
            referenceversion.ThumbnailPath = s3ThumbnailObject.id || null;

            // this.log('Prepared ReferenceVersion', referenceversion);
            this.referenceService.createReference(referenceversion);
            this.resetCreateForm();
        } else {
            this.createForm.get('UniqueID').markAsTouched();
            this.createForm.get('DisplayName').markAsTouched();
            this.createForm.get('Title').markAsTouched();
            this.createForm.get('VersionId').markAsTouched();
            this.createForm.get('ShortCitation').markAsTouched();
        }
    }

    deleteReference(referenceversion) {
        let reference: Reference = { ...this.references.find(reference => reference.id === referenceversion.Reference || reference.id === referenceversion.Reference.id) };
        this.referenceService.deleteReference(reference);
    }

    updateReferenceVersion(referenceversionFormValue){
        let referenceversion = { ...referenceversionFormValue };
            referenceversion.Tags = referenceversion.TagsSelect || [];
            referenceversion.Subtags = referenceversion.SubtagsSelect || [];
            delete referenceversion.TagsSelect;
            delete referenceversion.SubtagsSelect;

            
            if(referenceversion.FilePath) { 
                referenceversion.Filename = referenceversion.FilePath.key || '';
                referenceversion.FilePath = referenceversion.FilePath.id || null 
            };
            if(referenceversion.AnnotatedFilePath) { referenceversion.AnnotatedFilePath = referenceversion.AnnotatedFilePath.id || null };
            if(referenceversion.SourceFilePath) { referenceversion.SourceFilePath = referenceversion.SourceFilePath.id || null };
            if(referenceversion.ThumbnailPath) { referenceversion.ThumbnailPath = referenceversion.ThumbnailPath.id || null };
            // this.log('UPDATEDV',referenceversion);
        this.referenceService.updateReferenceVersion(referenceversion);
    }

    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------

    openReferenceVersionDialog(referenceversionId: ReferenceVersion['id']): void {
        let reference: Reference = { ...this.references.find(reference => reference.MasterVersion === referenceversionId || reference.MasterVersion.id === referenceversionId) };
        let masterversion: ReferenceVersion = { ...this.masterversions.find(masterversion => masterversion.id === referenceversionId) };

        this.referenceVersionDialogRef = this.dialog.open(ReferenceVersionDialog, {
            width: '99%',
            data: {
                Reference: { ...reference },
                MasterVersion: { ...masterversion },
                ReferenceVersions: [ ...reference.Versions ],
            }
        });

        this.referenceVersionDialogRef.afterClosed().subscribe(form => { });
    }

    openHTMLEditorDialog(control: AbstractControl, options?: Object): void {
        this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
            width: '50%',
            data: {
                control,
                options
            }
        });

        this.htmlEditorDialogRef.afterClosed().subscribe(data => {
            if (data !== control.value) { control.markAsDirty(); }
            if (data && data !== '') { control.setValue(data); }
        });
    }

    openFileSelectDialog(control: AbstractControl): void {
        this.fileSelectDialogRef = this.dialog.open(FileSelectDialog, {
            width: '60%',
        });

        this.fileSelectDialogRef.afterClosed().subscribe(data => {
            if(!data) return;
            control.setValue(data);
            control.markAsDirty();
        });
    }

    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////
    
    destroyReferences() {
        this.referenceService.destroyReferences();
    }

}