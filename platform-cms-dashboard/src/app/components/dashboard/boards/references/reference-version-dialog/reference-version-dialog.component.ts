import { Component, OnInit, Inject, NgZone, ViewChild, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';

import { Reference } from 'app/models/reference.model';
import { ReferenceVersion } from 'app/models/referenceversion.model';

import { ReferenceService } from 'app/services/dashboard/reference.service';
import { TagService } from 'app/services/dashboard/tag.service';
import { S3ObjectService } from 'app/services/dashboard/s3object.service';
import { LoggerService } from 'app/services/logger/logger.service';

import { Tag } from 'app/models/tag.model';
import { Subtag } from 'app/models/subtag.model';
import { S3Object } from 'app/models/s3object.model';

import { TagInputComponent } from 'ngx-chips';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';
import { FileSelectDialog } from 'app/components/file-select-dialog/file-select-dialog.component';

import { environment } from 'environments/environment';

declare var $: any;

@Component({
    selector: 'reference-version-dialog',
    templateUrl: 'reference-version-dialog.component.html',
    styleUrls: ['reference-version-dialog.component.scss']
})
export class ReferenceVersionDialog implements OnInit {
    
    @ViewChild('referenceVersionsTable', { read: MatSort }) ReferenceVersionsTableSort: MatSort;
    @ViewChildren('tableRowReferenceVersionInputs') tableRowReferenceVersionInputs: QueryList<any>;
    
    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////
    
    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public references: Array<Reference> = [];
    public versions: Array<ReferenceVersion> = [];
    
    public tags: Array<Tag> = [];
    public subtags: Array<Subtag> = [];
    public s3objects: Array<S3Object> = [];

    //-------------------------------------------------------------------
    // Dialog Data {} Constructors
    //-------------------------------------------------------------------
    public reference: Reference;
    public masterversion: ReferenceVersion;
    public referenceversions: Array<ReferenceVersion>;

    //-------------------------------------------------------------------
    // Form Variables Subtag
    //-------------------------------------------------------------------
    public createForm: FormGroup;
    public createFormExpanded: boolean = false;

    //-------------------------------------------------------------------
    // Form Variables ReferenceVersion Table
    //-------------------------------------------------------------------
    public ReferenceVersionsTableForm: FormGroup;
    public ReferenceVersionsTableRowForms: FormArray = new FormArray([]);
    public ReferenceVersionsTableRowEditState: Array<boolean> = [];


    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public TableColumnDefinitions = [
        { def: 'Master', name: 'Master', displayed: true, hideable: false },
        { def: 'UniqueID', name: 'UID', displayed: true, hideable: false },
        { def: 'VersionId', name: 'Version', displayed: true, hideable: false },
        { def: 'DisplayName', name: 'Display Name', displayed: true, hideable: true },
        { def: 'Title', name: 'Title', displayed: false, hideable: true },
        { def: 'ShortCitation', name: 'Short Citation', displayed: true, hideable: true },
        { def: 'Citation', name: 'Citation', displayed: false, hideable: true },
        { def: 'FilePath', name: 'File', displayed: true, hideable: true },
        { def: 'AnnotatedFilePath', name: 'Annotated File', displayed: false, hideable: true },
        { def: 'SourceFilePath', name: 'Source File', displayed: false, hideable: true },
        { def: 'Tags', name: 'Tags', displayed: false, hideable: true },
        { def: 'Subtags', name: 'Subtags', displayed: false, hideable: true },
        { def: 'Annotated', name: 'Annotated', displayed: false, hideable: true },
        { def: 'Disabled', name: 'Hidden', displayed: false, hideable: true },
        { def: 'Cache', name: 'Cache', displayed: false, hideable: true },
        { def: 'createdAt', name: 'Created', displayed: true, hideable: false },
        { def: 'updatedAt', name: 'Last Updated', displayed: false, hideable: true },
        { def: 'Actions', name: 'Actions', displayed: true, hideable: false }
    ];
    public MasterVersionsTableDisplayedColumns: string[] = [
        'Master',
        'UniqueID',
        'VersionId',
        'DisplayName',
        'Title',
        'ShortCitation',
        'Citation',
        'FilePath',
        'AnnotatedFilePath',
        'SourceFilePath',
        'Tags',
        'Subtags',
        'Annotated',
        'Disabled',
        'Cache',
        'createdAt',
        'updatedAt',
        'Actions',
    ];
    public ReferenceVersionsTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private htmlEditorDialogRef;
    private fileSelectDialogRef;

    //-------------------------------------------------------------------
    // Froala Editor Options
    //-------------------------------------------------------------------
    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: [],
        multiLine: false,
        charCounterCount: false,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        zIndex: 999,
    };

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private referenceService: ReferenceService,
        private tagService: TagService,
        private s3ObjectService: S3ObjectService,

        private zone: NgZone,
        private formBuilder: FormBuilder,
        private dialog: MatDialog,
        public dialogRef: MatDialogRef<ReferenceVersionDialog>,

        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("ReferenceVersionDialogComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("ReferenceVersionDialogComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Life-cycle Methods
    /////////////////////////////////////////////////////////////////////////

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit() {

        //-------------------------------------------------------------------
        // Deconstruct data
        //-------------------------------------------------------------------
        
        this.reference = { ...this.data.Reference };
        this.masterversion = { ...this.data.MasterVersion };
        this.referenceversions = [ ...this.data.ReferenceVersions ];

        //-------------------------------------------------------------------
        // Subscribe to All References
        //-------------------------------------------------------------------
        // Extrapolate Reference MasterVersion[s] to referenceversions
        //-------------------------------------------------------------------

        this.subscriptions.references = this.referenceService.references$.subscribe(response => {
            this.zone.run(() => {
                this.references = [...response];
                this.reference = [...response].find(item => item.id == this.reference.id);
                this.referenceversions = [...this.versions].filter(version => version.Reference && version.Reference.id == this.reference.id);
                this.masterversion = [...this.referenceversions].find(item => item.id == this.reference.MasterVersion.id) || this.masterversion;
                if (this.versions.length !== 0) {
                    this.setReferenceVersionsTable([...this.referenceversions]);
                    this.setReferenceVersionsTableRowForms([...this.referenceversions]);
                }
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to All ReferenceVersions
        //-------------------------------------------------------------------

        this.subscriptions.referenceversions = this.referenceService.referenceversions$.subscribe(response => {
            this.zone.run(() => {
                this.versions = [...response].map(version => {
                    if(!version.FilePath) { version.FilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.AnnotatedFilePath) { version.AnnotatedFilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.SourceFilePath) { version.SourceFilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.ThumbnailPath) { version.ThumbnailPath = { key: null, eTag: null, lastModified: null, size: null } }
                    return version;
                });
                this.referenceversions = [...this.versions].filter(version => version.Reference && version.Reference.id == this.reference.id);
                this.log('REFVERS rv sub', this.referenceversions);
                this.masterversion = [...response].find(item => item.id == this.reference.MasterVersion.id) || this.masterversion;
                this.setReferenceVersionsTable([...this.referenceversions]);
                this.setReferenceVersionsTableRowForms([...this.referenceversions]);
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to Tags & Subtags
        //-------------------------------------------------------------------

        this.subscriptions.tags = this.tagService.tags$.subscribe(response => {
            this.zone.run(() => {
                this.tags = [...response];

                this.subtags = [];
                [...response].forEach(tag => tag.Subtags.forEach(subtag => this.subtags.push(subtag)));
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to S3Objects
        //-------------------------------------------------------------------

        this.subscriptions.s3objects = this.s3ObjectService.s3objects$.subscribe(response => {
            this.zone.run(() => {
                this.s3objects = [...response];
            })
        })


        //-------------------------------------------------------------------
        // Set Form Variables Reference
        //-------------------------------------------------------------------
        this.createForm = this.formBuilder.group({
            'TagsSelect': [],
            'SubtagsSelect': [],
            ...new ReferenceVersion,
            'UniqueID': this.reference.UniqueID
        });
        this.createForm.get('UniqueID').disable();
        this.createForm.get('UniqueID').setValidators([Validators.required]);
        this.createForm.get('DisplayName').setValidators([Validators.required]);
        this.createForm.get('Title').setValidators([Validators.required]);
        this.createForm.get('VersionId').setValidators([Validators.required]);
        this.createForm.get('ShortCitation').setValidators([Validators.required]);

        //-------------------------------------------------------------------
        // Set Form Variables Reference Table
        //-------------------------------------------------------------------
        this.ReferenceVersionsTableRowForms = this.formBuilder.array([]);
        this.ReferenceVersionsTableForm = this.formBuilder.group({ 'ReferenceVersionsTableRowForms': this.ReferenceVersionsTableRowForms });
        this.log('Doc Table Forms', this.ReferenceVersionsTableForm);
        this.setReferenceVersionsTableRowForms(this.referenceversions);

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setReferenceVersionsTable([...this.referenceversions]);
        });

    }

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.references.unsubscribe();
        this.subscriptions.referenceversions.unsubscribe();
        this.subscriptions.tags.unsubscribe();

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  ReferenceVersions
    //-------------------------------------------------------------------

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------
    getDisplayedColumns(): string[] {
        return this.TableColumnDefinitions
            .filter(cd => cd.displayed == true)
            .map(cd => cd.def);
    }

    toggleColumnDisplayed(event, column) {
        let index = this.TableColumnDefinitions.findIndex(item => item.def === column.def);
        if (index > -1) { this.TableColumnDefinitions[index].displayed = event.checked; }
    }

    setReferenceVersionsTable(data: Array<ReferenceVersion>) {
        this.ReferenceVersionsTableDataSource = new MatTableDataSource(data);
        this.ReferenceVersionsTableDataSource.sort = this.ReferenceVersionsTableSort;
        this.ReferenceVersionsTableDataSource.sort.active = 'createdAt';
        this.ReferenceVersionsTableDataSource.sort.direction = 'desc';
    }

    setReferenceVersionsTableRowForms(data: Array<ReferenceVersion>) {
        this.zone.runOutsideAngular(() => {
            for (let referenceReferenceVersion of data) {
                
                if (!referenceReferenceVersion.Tags) { referenceReferenceVersion.Tags = []; };
                if (!referenceReferenceVersion.Subtags) { referenceReferenceVersion.Subtags = []; };

                let referenceversion: any = { ...referenceReferenceVersion };
                referenceversion.TagsSelect = [];
                referenceversion.SubtagsSelect = [];

                let row = this.formBuilder.group({
                    ...referenceversion
                });

                row.get('TagsSelect').setValue([...referenceversion.Tags.map(tag => tag.id)]);
                row.get('SubtagsSelect').setValue([...referenceversion.Subtags.map(subtag => subtag.id)]);

                row.get('UniqueID').disable();
                row.get('UniqueID').setValidators([Validators.required]);
                row.get('DisplayName').setValidators([Validators.required]);
                row.get('Title').setValidators([Validators.required]);
                row.get('VersionId').setValidators([Validators.required]);
                row.get('ShortCitation').setValidators([Validators.required]);

                this.ReferenceVersionsTableRowForms.controls[referenceReferenceVersion.id] = row;
                this.log('Doc Table Forms', this.ReferenceVersionsTableForm);
            };
        });
    }

    setTableRowCellInputFocus(rowId, control) {
        this.log('RF: ', this.ReferenceVersionsTableRowForms.controls[rowId]);
        setTimeout(() => {
            let index = this.tableRowReferenceVersionInputs.toArray().findIndex(el => el.nativeElement.id == `${rowId}-${control}`);
            this.log('RF Index', [index, this.tableRowReferenceVersionInputs.toArray()[index]]);
            if (index !== -1) { this.tableRowReferenceVersionInputs.toArray()[index].nativeElement.focus(); };
        }, 100); // DOM input initialization (ngIf)
    }

    resetReferenceVersionsTableRowForm(data: ReferenceVersion) {
        if (!data.Tags) { data.Tags = []; };
        if (!data.Subtags) { data.Subtags = []; };

        let referenceversion: any = { ...data };
        referenceversion.TagsSelect = [];
        referenceversion.SubtagsSelect = [];

        let row = this.formBuilder.group({
            ...referenceversion
        });

        row.get('TagsSelect').setValue([...referenceversion.Tags.map(tag => tag.id)]);
        row.get('SubtagsSelect').setValue([...referenceversion.Subtags.map(subtag => subtag.id)]);

        row.get('UniqueID').disable();
        row.get('UniqueID').setValidators([Validators.required]);
        row.get('DisplayName').setValidators([Validators.required]);
        row.get('Title').setValidators([Validators.required]);
        row.get('VersionId').setValidators([Validators.required]);
        row.get('ShortCitation').setValidators([Validators.required]);

        this.ReferenceVersionsTableRowForms.controls[data.id] = row;
    }

    resetCreateForm() {
        let reset = { 
            ...new ReferenceVersion,
            'UniqueID': this.reference.UniqueID
        };
        this.createForm.get('UniqueID').reset(reset.UniqueID);
        this.createForm.get('DisplayName').reset(reset.DisplayName);
        this.createForm.get('Title').reset(reset.Title);
        this.createForm.get('VersionId').reset(reset.VersionId);
        this.createForm.get('ShortCitation').reset(reset.ShortCitation);
        this.createForm.get('Citation').reset(reset.Citation);
        this.createForm.get('Tags').reset(reset.Tags);
        this.createForm.get('Subtags').reset(reset.Subtags);
        this.createForm.get('TagsSelect').reset([]);
        this.createForm.get('SubtagsSelect').reset([]);
        this.createForm.get('Annotated').reset(reset.Annotated);
        this.createForm.get('Disabled').reset(reset.Disabled);
        this.createForm.get('Cache').reset(reset.Cache);
        this.createForm.get('FilePath').reset(reset.FilePath);
        this.createForm.get('AnnotatedFilePath').reset(reset.AnnotatedFilePath);
        this.createForm.get('SourceFilePath').reset(reset.SourceFilePath);
        this.createForm.get('ThumbnailPath').reset(reset.ThumbnailPath);
        this.createForm.get('UniqueID').setValue(reset.UniqueID);
        this.createForm.get('DisplayName').setValue(reset.DisplayName);
        this.createForm.get('Title').setValue(reset.Title);
        this.createForm.get('VersionId').setValue(reset.VersionId);
        this.createForm.get('ShortCitation').setValue(reset.ShortCitation);
        this.createForm.get('Citation').setValue(reset.Citation);
        this.createForm.get('Tags').setValue(reset.Tags);
        this.createForm.get('Subtags').setValue(reset.Subtags);
        this.createForm.get('TagsSelect').setValue([]);
        this.createForm.get('SubtagsSelect').setValue([]);
        this.createForm.get('Annotated').setValue(reset.Annotated);
        this.createForm.get('Disabled').setValue(reset.Disabled);
        this.createForm.get('Cache').setValue(reset.Cache);
        this.createForm.get('FilePath').setValue(reset.FilePath);
        this.createForm.get('AnnotatedFilePath').setValue(reset.FilePath);
        this.createForm.get('SourceFilePath').setValue(reset.FilePath);
        this.createForm.get('ThumbnailPath').setValue(reset.ThumbnailPath);
        this.createForm.markAsPristine();
    }

    populateCreateForm(referenceversion, referenceversionTableRowForm) {
        this.createForm.get('UniqueID').setValue(referenceversion.UniqueID);
        this.createForm.get('DisplayName').setValue(referenceversion.DisplayName);
        this.createForm.get('Title').setValue(referenceversion.Title);
        this.createForm.get('VersionId').setValue(referenceversion.VersionId);
        this.createForm.get('ShortCitation').setValue(referenceversion.ShortCitation);
        this.createForm.get('Citation').setValue(referenceversion.Citation);
        this.createForm.get('Tags').setValue(referenceversion.Tags);
        this.createForm.get('Subtags').setValue(referenceversion.Subtags);
        this.createForm.get('TagsSelect').setValue(referenceversionTableRowForm.get('TagsSelect').value);
        this.createForm.get('SubtagsSelect').setValue(referenceversionTableRowForm.get('SubtagsSelect').value);
        this.createForm.get('Annotated').setValue(referenceversion.Annotated);
        this.createForm.get('Disabled').setValue(referenceversion.Disabled);
        this.createForm.get('Cache').setValue(referenceversion.Cache);
        this.createForm.get('FilePath').setValue(referenceversion.FilePath);
        this.createForm.get('AnnotatedFilePath').setValue(referenceversion.FilePath);
        this.createForm.get('SourceFilePath').setValue(referenceversion.SourceFilePath);
        this.createForm.get('ThumbnailPath').setValue(referenceversion.ThumbnailPath);
        this.createForm.markAsDirty();
    }

    //-------------------------------------------------------------------
    //  CRUD
    //-------------------------------------------------------------------

    createReferenceVersion(master?: boolean) {

        if(this.createForm.valid) {
            let referenceversion = this.createForm.value;
                referenceversion.Tags = referenceversion.TagsSelect || [];
                referenceversion.Subtags = referenceversion.SubtagsSelect || [];
                delete referenceversion.TagsSelect;
                delete referenceversion.SubtagsSelect;

            let s3FileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === referenceversion.FilePath.key) };
            let s3AnnotatedFileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === referenceversion.AnnotatedFilePath.key) };
            let s3SourceFileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === referenceversion.SourceFilePath.key) };
            let s3ThumbnailObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === referenceversion.ThumbnailPath.key) };

                referenceversion.Filename = s3FileObject.key || "";
                referenceversion.FilePath = s3FileObject.id || null;
                referenceversion.AnnotatedFilePath = s3AnnotatedFileObject.id || null;
                referenceversion.SourceFilePath = s3SourceFileObject.id || null;
                referenceversion.ThumbnailPath = s3ThumbnailObject.id || null;
    
            // this.log('Prepared ReferenceVersion in Dialog', referenceversion);
            this.referenceService.createReferenceVersion(referenceversion, this.reference, master);

            this.resetCreateForm();
        } else {
            this.createForm.get('UniqueID').markAsTouched();
            this.createForm.get('DisplayName').markAsTouched();
            this.createForm.get('Title').markAsTouched();
            this.createForm.get('VersionId').markAsTouched();
            this.createForm.get('ShortCitation').markAsTouched();
        }
    }

    setAsMasterVersion(referenceversion) {
        let reference = { ...this.reference };
        reference.MasterVersion = referenceversion.id;
        this.referenceService.updateReference(reference);
    }

    updateReferenceVersion(referenceversionFormValue) {
        let referenceversion = { ...referenceversionFormValue };
            referenceversion.Tags = referenceversion.TagsSelect || [];
            referenceversion.Subtags = referenceversion.SubtagsSelect || [];
            delete referenceversion.TagsSelect;
            delete referenceversion.SubtagsSelect;

            if(referenceversion.FilePath) { 
                referenceversion.Filename = referenceversion.FilePath.key || '';
                referenceversion.FilePath = referenceversion.FilePath.id || null 
            };
            if(referenceversion.AnnotatedFilePath) { referenceversion.AnnotatedFilePath = referenceversion.AnnotatedFilePath.id || null };
            if(referenceversion.SourceFilePath) { referenceversion.SourceFilePath = referenceversion.SourceFilePath.id || null };
            if(referenceversion.ThumbnailPath) { referenceversion.ThumbnailPath = referenceversion.ThumbnailPath.id || null };

            // this.log('UPDATEDV', {...referenceversion});
        this.referenceService.updateReferenceVersion(referenceversion);
    }

    deleteReferenceVersion(referenceversion) {
        this.referenceService.deleteReferenceVersion(referenceversion);
    }

    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------

    openHTMLEditorDialog(control: AbstractControl, options?: Object): void {
        this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
            width: '50%',
            data: {
                control,
                options,
            }
        });

        this.htmlEditorDialogRef.afterClosed().subscribe(data => {
            if (data !== control.value) { control.markAsDirty(); }
            if (data && data !== '') { control.setValue(data); }
        });
    }

    openFileSelectDialog(control: AbstractControl): void {
        this.fileSelectDialogRef = this.dialog.open(FileSelectDialog, {
            width: '60%',
        });

        this.fileSelectDialogRef.afterClosed().subscribe(data => {
            if (!data) return;
            control.setValue(data);
            control.markAsDirty();
        });
    }

    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////

}