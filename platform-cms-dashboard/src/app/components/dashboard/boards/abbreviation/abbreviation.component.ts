import { Component, OnInit, OnDestroy, ViewChild, ViewChildren, QueryList, NgZone } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';

import { FormBuilder, FormArray, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { AbbreviationService } from 'app/services/dashboard/abbreviation.service';

import { Abbreviation } from 'app/models/abbreviation.model';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';
import { environment } from 'environments/environment';

declare var $: any;
declare var window: any;

@Component({
    selector: 'dashboard-abbreviation',
    templateUrl: './abbreviation.component.html',
    styleUrls: ['./abbreviation.component.scss']
})
export class AbbreviationComponent implements OnInit, OnDestroy {

    @ViewChild('abbreviationsTable', { read: MatSort }) AbbreviationsTableSort: MatSort;
    @ViewChild('tablePaginator') AbbreviationsTablePaginator: MatPaginator;
    @ViewChildren('tableRowAbbreviationInputs') tableRowAbbreviationInputs: QueryList<any>;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public abbreviations: Array<Abbreviation> = [];

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';

    //-------------------------------------------------------------------
    // Form Variables Abbreviation
    //-------------------------------------------------------------------
    public createForm: FormGroup;

    //-------------------------------------------------------------------
    // Form Variables Abbreviation Table
    //-------------------------------------------------------------------
    public AbbreviationsTableForm: FormGroup;
    public AbbreviationsTableRowForms: FormArray = new FormArray([]);
    public AbbreviationsTableRowEditState: Array<boolean> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public AbbreviationsTableDisplayedColumns: string[] = [
        'Abbreviation',
        'Definition',
        'Disabled',
        'createdAt',
        'updatedAt',
        // 'id',
        'Actions',
    ];
    public AbbreviationsTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private htmlEditorDialogRef;

    //-------------------------------------------------------------------
    // Froala Editor Options
    //-------------------------------------------------------------------
    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', '|', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: ['reference', 'codeBeautifier', 'codeView'],
        multiLine: false,
        charCounterCount: false,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        theme: 'dark',
        codeMirror: window.CodeMirror,
        codeMirrorOptions: {
            indentWithTabs: true,
            lineNumbers: true,
            lineWrapping: true,
            mode: 'text/html',
            // theme: 'monokai',
            tabMode: 'indent',
            tabSize: 4
        }
    };

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private modelService: ModelService,
        private abbreviationService: AbbreviationService,

        private formBuilder: FormBuilder,
        private zone: NgZone,
        public dialog: MatDialog,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("AbbreviationComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("AbbreviationComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------
        this.applyFilter(this.filterValue);

        //-------------------------------------------------------------------
		// Subscribe to Subscriptions
		//-------------------------------------------------------------------

		// this.abbreviationService.subscribeToAbbreviations();
        
        //-------------------------------------------------------------------
        // Set Form Variables Abbreviation Table
        //-------------------------------------------------------------------
        this.AbbreviationsTableRowForms = this.formBuilder.array([]);
        this.AbbreviationsTableForm = this.formBuilder.group({ 'AbbreviationsTableRowForms': this.AbbreviationsTableRowForms });

        //-------------------------------------------------------------------
        // Subscribe to All Abbreviations
        //-------------------------------------------------------------------

        this.subscriptions.abbreviations = this.abbreviationService.abbreviations$.subscribe(response => {
            this.zone.run(() => {
                this.abbreviations = [...response];
                this.setAbbreviationsTable([...response]);
                this.setAbbreviationsTableRowForms([...response]);
            });
        });

        //-------------------------------------------------------------------
        // Set Form Variables Abbreviation
        //-------------------------------------------------------------------
        this.createForm = this.formBuilder.group({
            ...new Abbreviation,
        });
        this.createForm.get('Abbreviation').setValidators([Validators.required]);

    }
    

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.abbreviations.unsubscribe();

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        this.applyFilter(this.filterValue);

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setAbbreviationsTable([...this.abbreviations]);
        });

        //-------------------------------------------------------------------
        // Subscribe to children
        //-------------------------------------------------------------------
        this.tableRowAbbreviationInputs.changes.subscribe(change => this.tableRowAbbreviationInputs = change);

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------

    setAbbreviationsTable(data: Array<Abbreviation>) {
        this.AbbreviationsTableDataSource = new MatTableDataSource(data);
        this.AbbreviationsTableDataSource.paginator = this.AbbreviationsTablePaginator;
        this.AbbreviationsTableDataSource.sort = this.AbbreviationsTableSort;
        this.AbbreviationsTableDataSource.sort.active = 'createdAt';
        this.AbbreviationsTableDataSource.sort.direction = 'desc';
    }

    setAbbreviationsTableRowForms(data: Array<Abbreviation>) {
        this.zone.runOutsideAngular(() => {
            for (let abbreviation of data) {
                let row = this.formBuilder.group({ ...abbreviation });
                row.get('Abbreviation').setValidators([Validators.required]);
                this.AbbreviationsTableRowForms.controls[abbreviation.id] = row;
            };
        });
    }

    setTableRowCellInputFocus(rowId, control) {
        setTimeout(() => {
            let index = this.tableRowAbbreviationInputs.toArray().findIndex(el => el.nativeElement.id == `${rowId}-${control}`);
            if (index !== -1) { this.tableRowAbbreviationInputs.toArray()[index].nativeElement.focus(); };
        }, 100); // DOM input initialization (ngIf)
    }

    applyFilter(filterValue: string) {
        this.AbbreviationsTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.AbbreviationsTableDataSource.filter = '';
    }

    resetAbbreviationsTableRowForm(data: Abbreviation) {
        let row = this.formBuilder.group({ ...data });
        row.get('Abbreviation').setValidators([Validators.required]);
        this.AbbreviationsTableRowForms.controls[data.id] = row;
    }

    resetCreateForm() {
        let reset = { ...new Abbreviation };
        this.createForm.get('Abbreviation').reset();
        this.createForm.get('Definition').reset();
        this.createForm.get('Disabled').reset();
        this.createForm.get('Abbreviation').setValue(reset.Abbreviation);
        this.createForm.get('Definition').setValue(reset.Definition);
        this.createForm.get('Disabled').setValue(reset.Disabled);
        this.createForm.markAsPristine();
    }

    //-------------------------------------------------------------------
    //  CRUD
    //-------------------------------------------------------------------

    createAbbreviation() {
        if (this.createForm.valid) {
            let abbreviation = this.createForm.value;
            this.abbreviationService.createAbbreviation(abbreviation);
            this.resetCreateForm();
        } else {
            this.createForm.get('Abbreviation').markAsTouched();
        }
    }

    updateAbbreviation(abbreviation) {
        this.abbreviationService.updateAbbreviation(abbreviation);
    }

    deleteAbbreviation(abbreviationElement) {
        let abbreviation = { ...abbreviationElement };
        this.abbreviationService.deleteAbbreviation(abbreviation);
    }

    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////
    
    destroyAbbreviations() {
        this.abbreviationService.destroyAbbreviations();
    }

    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------

    openHTMLEditorDialog(control: AbstractControl, options?: Object): void {
        this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
            width: '50%',
            data: {
                control,
                options
            }
        });

        this.htmlEditorDialogRef.afterClosed().subscribe(data => {
            if (data !== control.value) { control.markAsDirty(); }
            if (data && data !== '') { control.setValue(data); }
        });
    }

}