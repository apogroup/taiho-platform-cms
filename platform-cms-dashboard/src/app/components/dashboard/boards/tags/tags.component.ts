import { Component, OnInit, OnDestroy, ViewChild, ViewChildren, QueryList, NgZone } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';

import { FormBuilder, FormArray, FormGroup, FormControl, Validators } from '@angular/forms';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { TagService } from 'app/services/dashboard/tag.service';

import { Tag } from 'app/models/tag.model';
import { Subtag } from 'app/models/subtag.model';
import { TagEditDialog } from 'app/components/dashboard/boards/tags/tag-edit-dialog/tag-edit-dialog.component';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';

import { environment } from 'environments/environment';

declare var $: any;

@Component({
    selector: 'dashboard-tags',
    templateUrl: './tags.component.html',
    styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit, OnDestroy {

    @ViewChild('tagsTable', { read: MatSort }) TagsTableSort: MatSort;
    @ViewChild('tablePaginator') TagsTablePaginator: MatPaginator;
    @ViewChildren('tableRowTagInputs') tableRowTagInputs: QueryList<any>;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public tags: Array<Tag> = [];

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';


    //-------------------------------------------------------------------
    // Form Variables Tag
    //-------------------------------------------------------------------
    public tagCreateForm: FormGroup;
    public tagCreateInput: Array<any> = [];

    //-------------------------------------------------------------------
    // Form Variables Tag Table
    //-------------------------------------------------------------------
    public tagTableForm: FormGroup;
    public tagRowForms: FormArray = new FormArray([]);
    public tagRowEditState: Array<boolean> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public TagsTableDisplayedColumns: string[] = [
        'Tag',
        'Subtags',
        'createdAt',
        'updatedAt',
        // 'id',
        // 'Disabled',
        'Actions',
    ];
    public TagsTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private tagEditDialogRef;
    private htmlEditorDialogRef;

    //-------------------------------------------------------------------
    // Froala Editor Options
    //-------------------------------------------------------------------
    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: [],
        multiLine: false,
        charCounterCount: false,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        theme: 'dark',
    };

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private modelService: ModelService,
        private tagService: TagService,

        private formBuilder: FormBuilder,
        private zone: NgZone,
        public dialog: MatDialog,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("TagsComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("TagsComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Subscribe to Subscriptions
        //-------------------------------------------------------------------

        // this.tagService.subscribeToTags();
        // this.tagService.subscribeToSubtags();

        //-------------------------------------------------------------------
        // Set Form Variables Tag Table
        //-------------------------------------------------------------------
        
        this.tagRowForms = this.formBuilder.array([]);
        this.tagTableForm = this.formBuilder.group({ 'tagRowForms': this.tagRowForms });

        //-------------------------------------------------------------------
        // Set Form Variables Tag
        //-------------------------------------------------------------------
        this.tagCreateInput = [];
        this.tagCreateForm = this.formBuilder.group({ placeholder: ' ' });


        //-------------------------------------------------------------------
        // Subscribe to All Tags
        //-------------------------------------------------------------------

        this.subscriptions.tags = this.tagService.tags$.subscribe(response => {
            this.zone.run(() => {
                this.tags = [...response];
                this.setTagsTable([...response]);
                this.setTagsTableRowForms([...response]);
            });
        });

    }
    

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.tags.unsubscribe();

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setTagsTable([...this.tags]);
        });

        //-------------------------------------------------------------------
        // Subscribe to children
        //-------------------------------------------------------------------
        this.tableRowTagInputs.changes.subscribe(change => this.tableRowTagInputs = change);

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------

    setTagsTable(data: Array<Tag>) {
        this.TagsTableDataSource = new MatTableDataSource(data);
        this.TagsTableDataSource.paginator = this.TagsTablePaginator;
        this.TagsTableDataSource.sort = this.TagsTableSort;
        this.TagsTableDataSource.sort.active = 'createdAt';
        this.TagsTableDataSource.sort.direction = 'desc';
    }

    setTagsTableRowForms(data: Array<Tag>) {
        this.zone.runOutsideAngular(() => {
            for (let tag of data) {
                let row = this.formBuilder.group({ ...tag });
                row.get('Tag').setValidators([Validators.required]);
                this.tagRowForms.controls[tag.id] = row;
            };
        });
    }

    applyFilter(filterValue: string) {
        this.TagsTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.TagsTableDataSource.filter = '';
    }

    setTableRowTagInputFocus(rowId){
        setTimeout(() => {
            let index = this.tableRowTagInputs.toArray().findIndex(el => el.nativeElement.id == rowId);
            if (index !== -1) { this.tableRowTagInputs.toArray()[index].nativeElement.focus(); };
        }, 100); // DOM input initialization (ngIf)
    }

    resetTagsTableRowForm(data: Tag) {
        let row = this.formBuilder.group({ ...data });
        row.get('Tag').setValidators([Validators.required]);
        row.get('Tag').markAsPristine();
        this.tagRowForms.controls[data.id] = row;
    }

    //-------------------------------------------------------------------
    //  CRUD
    //-------------------------------------------------------------------

    createTags() {
        let tags = [ ...this.tagCreateInput ];

        for(let tag of tags) {
            this.tagService.createTag(tag.value); // tag: { display: string, value: string }
        }

        this.tagCreateInput = [];
    }

    updateTag(tagElement, tagValue) {
        if(tagElement.Tag == tagValue) { return; };
        let tag = { ...tagElement };
            tag.Tag = tagValue;
        this.tagService.updateTag(tag);
    }

    deleteTag(tagElement) {
        let tag = { ...tagElement };
        this.tagService.deleteTag(tag);
    }

    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------

    openSubtagEditDialog(tagId: Tag['id'], subtagId: Subtag['id']): void {
        let tag: Tag = { ...this.tags.find(tag => tag.id === tagId) };
        let subtag: Subtag = { ...tag.Subtags.find(subtag => subtag.id === tagId) };

        this.tagEditDialogRef = this.dialog.open(TagEditDialog, {
            width: '90%',
            data: {
                tag: tag,
                targetSubtagId: subtagId
            }
        });

        this.tagEditDialogRef.afterClosed().subscribe(form => {});
    }

    openHTMLEditorDialog(control: FormControl, options?: Object): void {
        this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
            width: '50%',
            data: {
                control,
                options
            }
        });

        this.htmlEditorDialogRef.afterClosed().subscribe(data => {
            if (data !== control.value) { control.markAsDirty(); }
            if (data && data !== '') { control.setValue(data); }
        });
    }

    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////
    
    destroyTags() {
        this.tagService.destroyTags();
    }

}