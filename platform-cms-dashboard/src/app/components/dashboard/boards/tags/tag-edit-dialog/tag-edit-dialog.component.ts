import { Component, OnInit, Inject, NgZone, ViewChild, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';

import { Tag } from 'app/models/tag.model';
import { Subtag } from 'app/models/subtag.model';

import { TagService } from 'app/services/dashboard/tag.service';
import { LoggerService } from 'app/services/logger/logger.service';

import { TagInputComponent } from 'ngx-chips';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';

import { environment } from 'environments/environment';

declare var $: any;

@Component({
    selector: 'tag-edit-dialog',
    templateUrl: 'tag-edit-dialog.component.html',
    styleUrls: ['tag-edit-dialog.component.scss']
})
export class TagEditDialog implements OnInit {

    @ViewChild('subtagsCreateFormField') subtagsCreateFormField: TagInputComponent;
    @ViewChild('subtagsTable', { read: MatSort }) SubtagsTableSort: MatSort;
    @ViewChildren('tableRowSubtagInputs') tableRowSubtagInputs: QueryList<any>;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public subtags: Array<Subtag> = [];

    //-------------------------------------------------------------------
    // Form Variables Subtag
    //-------------------------------------------------------------------
    public subtagCreateForm: FormGroup;
    public subtagCreateInput: Array<any> = [];

    //-------------------------------------------------------------------
    // Form Variables Subtag Table
    //-------------------------------------------------------------------
    public subtagTableForm: FormGroup;
    public subtagRowForms: FormArray = new FormArray([]);
    public subtagRowEditState: Array<boolean> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public SubtagsTableDisplayedColumns: string[] = [
        'Subtag',
        'createdAt',
        'updatedAt',
        'Actions',
    ];
    public SubtagsTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Data {} Constructors
    //-------------------------------------------------------------------
    public tag: Tag;
    public targetSubtagId: string;
    public targetFocusSet: boolean = false;

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private htmlEditorDialogRef;

    //-------------------------------------------------------------------
    // Froala Editor Options
    //-------------------------------------------------------------------
    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: [],
        multiLine: false,
        charCounterCount: false,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        theme: 'dark',
    };
    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private tagService: TagService,

        private zone: NgZone,
        private formBuilder: FormBuilder,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<TagEditDialog>,

        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("TagEditDialogComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("TagEditDialogComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Life-cycle Methods
    /////////////////////////////////////////////////////////////////////////

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit() {

        //-------------------------------------------------------------------
        // Deconstruct data
        //-------------------------------------------------------------------
        
        this.tag = { ...this.data.tag };
        this.targetSubtagId = this.data.targetSubtagId;

        //-------------------------------------------------------------------
        // Subscribe to All Tags
        //-------------------------------------------------------------------

        this.subscriptions.subtags = this.tagService.subtags$.subscribe(response => {
            this.zone.run(() => {
                this.subtags = [...response].filter((subtag) => subtag.Tag && subtag.Tag.id == this.tag.id);
                this.setSubtagsTableRowForms([...response].filter((subtag) => subtag.Tag && subtag.Tag.id == this.tag.id));
                this.setSubtagsTable([...response].filter((subtag) => subtag.Tag && subtag.Tag.id == this.tag.id));
                this.setTargetFormField();
            });
        });

        //-------------------------------------------------------------------
        // Set Form Variables Tag
        //-------------------------------------------------------------------
        this.subtagCreateInput = [];
        this.subtagCreateForm = this.formBuilder.group({ placeholder: ' ' });

        //-------------------------------------------------------------------
        // Set Form Variables Tag Table
        //-------------------------------------------------------------------
        this.subtagRowForms = this.formBuilder.array([]);
        this.subtagTableForm = this.formBuilder.group({ 'subtagRowForms': this.subtagRowForms });

        //-------------------------------------------------------------------
        // Preset Table Row Forms from data
        //-------------------------------------------------------------------
        this.setSubtagsTableRowForms(this.tag.Subtags);

        //-------------------------------------------------------------------
        // Set Focus from Input
        //-------------------------------------------------------------------
        if (!this.targetSubtagId) { 
            setTimeout(() => {
                this.subtagsCreateFormField.focus(true);
            }, 500);
        }

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setSubtagsTable([...this.subtags]);
        });

        //-------------------------------------------------------------------
        // Subscribe to children
        //-------------------------------------------------------------------
        this.tableRowSubtagInputs.changes.subscribe(change => this.tableRowSubtagInputs = change);

    }

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.subtags.unsubscribe();

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Subtags
    //-------------------------------------------------------------------

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------

    setSubtagsTable(data: Array<Subtag>) {
        this.SubtagsTableDataSource = new MatTableDataSource(data);
        this.SubtagsTableDataSource.sort = this.SubtagsTableSort;
        this.SubtagsTableDataSource.sort.active = 'createdAt';
        this.SubtagsTableDataSource.sort.direction = 'desc';
    }

    setSubtagsTableRowForms(data: Array<Subtag>) {
        this.zone.runOutsideAngular(() => {
            for (let subtag of data) {
                let row = this.formBuilder.group({ ...subtag });
                row.get('Subtag').setValidators([Validators.required]);
                this.subtagRowForms.controls[subtag.id] = row;
            };
        });
    }

    setTargetFormField() {
        setTimeout(() => {
            if (!this.targetFocusSet && this.targetSubtagId) {
                this.subtagRowEditState[this.targetSubtagId] = true;
                this.setTableRowSubtagInputFocus(this.targetSubtagId);
                this.targetFocusSet = true;
            }
        }, 500);
    }

    setTableRowSubtagInputFocus(rowId) {
        setTimeout(() => {
            let index = this.tableRowSubtagInputs.toArray().findIndex(el => el.nativeElement.id == rowId);
            if (index !== -1) { this.tableRowSubtagInputs.toArray()[index].nativeElement.focus(); };
        }, 100); // DOM input initialization (ngIf)
    }

    resetSubtagsTableRowForm(data: Subtag) {
        let row = this.formBuilder.group({ ...data });
        row.get('Subtag').setValidators([Validators.required]);
        row.get('Subtag').markAsPristine();
        this.subtagRowForms.controls[data.id] = row;
    }

    //-------------------------------------------------------------------
    //  CRUD
    //-------------------------------------------------------------------

    createSubtags() {
        let subtags = [...this.subtagCreateInput];

        for (let subtag of subtags) {
            this.tagService.createSubtag(this.tag.id, subtag.value); // subtag: { display: string, value: string }
        }

        this.subtagCreateInput = [];
    }

    updateSubtag(subtagElement, subtagValue) {
        if (subtagElement.Subtag == subtagValue) { return; };
        let subtag = { ...subtagElement };
        subtag.Subtag = subtagValue;
        this.tagService.updateSubtag(subtag);
    }

    deleteSubtag(subtagElement) {
        let subtag = { ...subtagElement };
        this.tagService.deleteSubtag(subtag);
    }

    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------

    openHTMLEditorDialog(control: FormControl, options?: Object): void {
        this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
            width: '50%',
            data: {
                control,
                options
            }
        });

        this.htmlEditorDialogRef.afterClosed().subscribe(data => {
            if (data !== control.value) { control.markAsDirty(); }
            if (data && data !== '') { control.setValue(data); }
        });
    }

    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////

}