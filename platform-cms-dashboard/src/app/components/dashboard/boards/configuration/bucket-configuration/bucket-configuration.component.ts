import { get, pick } from 'lodash';

import { Bucket } from 'app/models/bucket.model';
import { S3Object } from 'app/models/s3object.model';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';

import { BucketService } from 'app/services/dashboard/bucket.service';

import { MatDialog } from '@angular/material';
import { FileSelectDialog } from 'app/components/file-select-dialog/file-select-dialog.component';

import { Component, OnInit, Input } from '@angular/core';

import { environment } from 'environments/environment';

declare const window: any;

const editableFields: string[] = ['Title', 'Description', 'ExportTitle'];

enum Status {
  Browsing,
  Editing
}

@Component({
  selector: 'bucket-configuration',
  templateUrl: './bucket-configuration.component.html',
  styleUrls: ['./bucket-configuration.component.scss']
})
export class BucketConfigurationComponent implements OnInit {

  @Input() bucket: Bucket;

  Status = Status;
  public status: Status = Status.Browsing;
  public hasExport: boolean = false;
  private editBucketForm: FormGroup;

  //-------------------------------------------------------------------
  // Froala Editor Options
  //-------------------------------------------------------------------
  public editorOptions: object = {
    key: environment.froala.key,
    toolbarButtons: ['bold', 'italic', 'underline', '|', 'referenceSelect', '|', 'superscript', 'subscript', 'outdent', 'indent', 'formatOL', 'formatUL', '|', 'undo', 'redo', '|', 'html', '|', 'insertTable'],
    pluginsEnabled: ['reference', 'lists', 'paragraphFormat ', 'charCounter', 'codeBeautifier', 'codeView', 'table'],
    multiLine: true,
    charCounterCount: true,
    pastePlain: true,
    pasteDeniedTags: ['table', 'th', 'tr', 'td'],
    zIndex: 999,
    codeMirror: window.CodeMirror,
    codeMirrorOptions: {
      indentWithTabs: true,
      lineNumbers: true,
      lineWrapping: true,
      mode: 'text/html',
      tabMode: 'indent',
      tabSize: 4
    },
    tableStyles: {
      'fr-alternate-rows': 'Alternate Rows'
    },
    tableCellStyles: {
      'cell-primary': 'Header Style',
      'cell-secondary': 'Subheader Style',
      'cell-muted': 'Muted Color'
    },
    tableEditButtons: ['tableHeader', 'tableRemove', '|', 'tableRows', 'tableColumns', 'tableStyle', '-', 'tableCells', 'tableCellVerticalAlign', 'tableCellHorizontalAlign', 'tableCellStyle'],
    events: {
      'froalaEditor.table.inserted': function (e, editor, table) {
        // table.classList.add('apop-table');
      }
    }
  };

  //-------------------------------------------------------------------
  // Dialog Components
  //-------------------------------------------------------------------
  private fileSelectDialogRef;

  constructor(
    private formBuilder: FormBuilder,
    private bucketService: BucketService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {

    this.bucket = {...new Bucket, ...this.bucket};

    const fields = pick(this.bucket, editableFields);
    this.editBucketForm = this.formBuilder.group(fields);

    this.hasExport = this.bucket.Key === 'core-scientific-statements';
    if (this.hasExport) {
      // const Export = get(this.bucket, 'Export', new S3Object);
      const Export = this.bucket.Export || new S3Object;
      const ExportControl: AbstractControl = this.formBuilder.control(Export);
      this.editBucketForm.addControl('Export', ExportControl);
    }
  }

  /////////////////////////////////////////////////////////////////////////
  // Methods
  /////////////////////////////////////////////////////////////////////////

  editBucket(): void {
    this.status = Status.Editing;
  }

  restoreBucketForm(): void {
    try {
      this.editBucketForm.reset(this.bucket);
      this.status = Status.Browsing;
    } catch (error) {
      console.error(error);
    }
  }

  openFileSelectDialog(control: AbstractControl): void {
    this.fileSelectDialogRef = this.dialog.open(FileSelectDialog, {
      width: '60%',
    });

    this.fileSelectDialogRef.afterClosed().subscribe(data => {
      if (!data) return;
      control.setValue(data);
      control.markAsDirty();
    });
  }

  //-------------------------------------------------------------------
  //  CRUD
  //-------------------------------------------------------------------

  updateBucket() {
    try {

      const { value: updatedFields } = this.editBucketForm;

      const previousVersion = this.bucket;

      try {
        const { id } = updatedFields.Export;
        updatedFields.Export = id;
      } catch (error) {
      }

      const bucket: Bucket = {
        ...previousVersion,
        ...updatedFields
      };

      this.bucketService.updateBucket(bucket);

      this.status = Status.Browsing;

    } catch (error) {
      console.error(error);
      this.restoreBucketForm();
    }
  }
}
