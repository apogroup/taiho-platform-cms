import { Subscription } from 'rxjs';

import {
    Component, OnInit, OnDestroy, NgZone
} from '@angular/core';

import { LoggerService } from 'app/services/logger/logger.service';
import { BucketService } from 'app/services/dashboard/bucket.service';
import { SettingService } from 'app/services/dashboard/setting.service';

import { Bucket } from 'app/models/bucket.model';
import { Setting } from 'app/models/setting.model';

@Component({
    selector: 'dashboard-configuration',
    templateUrl: './configuration.component.html',
    styleUrls: ['./configuration.component.scss'],
})
export class ConfigurationComponent implements OnInit, OnDestroy {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: Subscription = new Subscription;
    public buckets: Bucket[] = [];
    public settings: Setting[] = [];

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private bucketService: BucketService,
        private settingService: SettingService,
        private zone: NgZone,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("ConfigurationComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("ConfigurationComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Subscribe to All Buckets
        //-------------------------------------------------------------------
        const buckets$ = this.bucketService.buckets$.subscribe((buckets: Bucket[]) => {
            this.zone.run(() => {
                this.buckets = buckets;
            });
        });
        this.subscriptions.add(buckets$);

        //-------------------------------------------------------------------
        // Subscribe to All Settings
        //-------------------------------------------------------------------
        const settings$ = this.settingService.settings$.subscribe((settings: Setting[]) => {
            this.zone.run(() => {
                this.settings = settings;
            });
        });
        this.subscriptions.add(settings$);

    }

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.unsubscribe();

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    trackBucketById(index, bucket: Bucket) {
        return bucket ? bucket.id : undefined;
    }

}