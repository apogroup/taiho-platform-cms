import reduce from 'lodash/reduce';
import find from 'lodash/find';

import { Setting } from 'app/models/setting.model';
import { S3Object } from 'app/models/s3object.model';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';

import { SettingService } from 'app/services/dashboard/setting.service';
import { GatewayS3Service } from 'app/services/gateway/gateway-s3.service';

import { MatDialog } from '@angular/material';
import { FileSelectDialog } from 'app/components/file-select-dialog/file-select-dialog.component';

import { Component, OnInit, Input, NgZone } from '@angular/core';

import { environment } from 'environments/environment';

declare const window: any;

enum Status {
  Browsing,
  Editing
}

@Component({
  selector: 'settings-configuration',
  templateUrl: './settings-configuration.component.html',
  styleUrls: ['./settings-configuration.component.scss']
})
export class SettingsConfigurationComponent implements OnInit {

  @Input() settings: Setting[];

  Status = Status;
  public status: Status = Status.Browsing;
  public editSettingsForm: FormGroup;

  //-------------------------------------------------------------------
  // Dialog Components
  //-------------------------------------------------------------------
  private fileSelectDialogRef;

  constructor(
    private formBuilder: FormBuilder,
    private settingService: SettingService,
    private gatewayS3Service: GatewayS3Service,
    private zone: NgZone,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {

    const controlsConfig = this.createSettingsControlsConfig(this.settings);
    this.editSettingsForm = this.formBuilder.group(controlsConfig);

  }

  /////////////////////////////////////////////////////////////////////////
  // Methods
  /////////////////////////////////////////////////////////////////////////

  createSettingsControlsConfig(settings) {
    const collectSettingsAsFields = (controlsConfig: any, setting: Setting) => {
      return {
        ...controlsConfig,
        [setting.Key]: setting.Value
      };
    };

    return this.settings.reduce(collectSettingsAsFields, {});
  }

  editSettings(): void {
    this.status = Status.Editing;
  }

  restoreSettingsForm(): void {
    try {
      const pristineControlsConfig = this.createSettingsControlsConfig(this.settings);
      this.editSettingsForm.reset(pristineControlsConfig);
      this.status = Status.Browsing;
    } catch (error) {
      console.error(error);
    }
  }

  openFileSelectDialog(control: AbstractControl): void {
    this.fileSelectDialogRef = this.dialog.open(FileSelectDialog, {
      width: '60%',
    });

    this.fileSelectDialogRef.afterClosed().subscribe(data => {
      if (!data) return;
      control.setValue(data);
      control.markAsDirty();
    });
  }

  //-------------------------------------------------------------------
  //  CRUD
  //-------------------------------------------------------------------

  updateSettings() {
    try {

      const dirtySettings = reduce(this.editSettingsForm.controls, (settings: Setting[], control: AbstractControl, key: string) => {
        try {
          const settingToUpdate = find(this.settings, { Key: key });
          settingToUpdate.Value = control.value;
          settings.push(settingToUpdate);
        } catch (error) {
          console.error(error);
        }

        return settings;
      }, []);
      
      dirtySettings.forEach(setting => {
        this.settingService.updateSetting(setting);
      });

      this.status = Status.Browsing;

    } catch (error) {
      console.error(error);
      this.restoreSettingsForm();
    }
  }

  viewFile(key: string): void {
    this.gatewayS3Service.get$(key).subscribe((response) => {
      this.zone.run(() => {

        if (response.error) {
          console.error(response.error);
        } else {
          console.log(response);
          window.open(response);
        }

      }, error => {

        // this.log('ERROR: (getS3BucketFile get$.subscribe', error);

      });
    });

  }

}
