import { Component, OnInit, OnDestroy, QueryList, ViewChildren, ViewChild, NgZone } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource, MatDialog, MatDatepicker } from '@angular/material';
import { Congress } from 'app/models/congress.model';
import { S3Object } from 'app/models/s3object.model';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { environment } from 'environments/environment';
import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { CongressService } from 'app/services/dashboard/congress.service';
import { S3ObjectService } from 'app/services/dashboard/s3object.service';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';
import { FileSelectDialog } from 'app/components/file-select-dialog/file-select-dialog.component';
import { DocumentMultiSelectDialog } from 'app/components/document-multiselect-dialog/document-multiselect-dialog.component';
import { DocumentService } from 'app/services/dashboard/document.service';
import { LinkDialog } from 'app/components/link-dialog/link-dialog.component';
import { Link } from 'app/models/link.model';

declare var $: any;

@Component({
  selector: 'dashboard-congresses',
  templateUrl: './congresses.component.html',
  styleUrls: ['./congresses.component.scss']
})
export class CongressesComponent implements OnInit, OnDestroy {

  @ViewChild('congressesTable', { read: MatSort }) CongressesTableSort: MatSort;
  @ViewChild('tablePaginator') CongressesTablePaginator: MatPaginator;
  @ViewChildren('tableRowCongressInputs') tableRowCongressInputs: QueryList<any>;

  /////////////////////////////////////////////////////////////////////////
  // Variables
  /////////////////////////////////////////////////////////////////////////

  //-------------------------------------------------------------------
  // Data Subscriptions
  //-------------------------------------------------------------------
  private subscriptions: any = {};
  public congresses: Array<Congress> = [];

  public s3objects: Array<S3Object> = [];
  public documents: Array<Document> = [];
  public documentsForAutoComplete: Array<any> = [];
  
  public links: Array<Link> = [];
  public linksForAutoComplete: Array<any> = [];


  //-------------------------------------------------------------------
  // Filter Input Binders
  //-------------------------------------------------------------------
  public filterValue: string = '';

  //-------------------------------------------------------------------
  // Form Variables Congress
  //-------------------------------------------------------------------
  public createForm: FormGroup;
  public congressTableRowForms: FormArray = new FormArray([]);
  public congressesDisplayFormControlValue: string = '';
  public startDatePicker: MatDatepicker<any>;
  public endDatePicker: MatDatepicker<any>;
  public invalidDateRange: Boolean = false;

  public minDate: Date;
  public maxDate: Date;

  //-------------------------------------------------------------------
  // Form Variables Congress Table
  //-------------------------------------------------------------------
  public CongressesTableForm: FormGroup;
  public CongressesTableRowForms: FormArray = new FormArray([]);
  public CongressesTableRowEditState: Array<boolean> = [];

  //-------------------------------------------------------------------
  // Material Component Variable
  //-------------------------------------------------------------------
  public CongressesTableDisplayedColumns: string[] = [
    // 'Order',
    'Event',
    'Location',
    'Dates',
    'StartDate',
    'EndDate',
    'Specialty',
    'Attendance',
    'Attendees',
    'Image',
    'Links',
    // 'id',
    // 'Disabled',
    'Actions',
  ];
  public CongressesTableDataSource = new MatTableDataSource([]);

  //-------------------------------------------------------------------
  // Dialog Components
  //-------------------------------------------------------------------
  private htmlEditorDialogRef;
  private fileSelectDialogRef;
  private documentMultiSelectDialogRef;
  private linkDialogRef;
  
  //-------------------------------------------------------------------
  // Froala Editor Options
  //-------------------------------------------------------------------
  public inlineEditorOptions: Object = {
    key: environment.froala.key,
    toolbarInline: true,
    toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'undo', 'redo'],
    showNextToCursor: true,
    pluginsEnabled: [],
    multiLine: false,
    charCounterCount: false, 
    placeholderText: '...',
    pastePlain: true,
    pasteDeniedTags: ['table', 'th', 'tr', 'td'],
    enter: $.FroalaEditor.ENTER_BR,
    theme: 'dark',
  };


  /////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////
  constructor(
    private logger: LoggerService,
    private congressService: CongressService,
    private s3ObjectService: S3ObjectService,
    private documentService: DocumentService,

    private formBuilder: FormBuilder,
    private zone: NgZone,
    public dialog: MatDialog,
  ) {

      //-------------------------------------------------------------------
      // Configure Logger
      //-------------------------------------------------------------------

      this.logger.config("CongressesComponent",
        {
            label: 'background: #03a9f4; color: #333',
            text: ''
        }
      );
   }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
      this.logger.log("CongressesComponent", text, ...args);
    }   

  /////////////////////////////////////////////////////////////////////////
  // Component Life-Cycle Methods
  /////////////////////////////////////////////////////////////////////////

  ngOnInit() {

      //-------------------------------------------------------------------
      // Subscribe to Subscriptions
      //-------------------------------------------------------------------


      //-------------------------------------------------------------------
      // Set Form Variables Congress Table
      //-------------------------------------------------------------------
      this.CongressesTableRowForms = this.formBuilder.array([]);
      this.CongressesTableForm = this.formBuilder.group({ 'CongressesTableRowForms': this.CongressesTableRowForms });


      //-------------------------------------------------------------------
      // Subscribe to Congresses
      //-------------------------------------------------------------------

      this.subscriptions.congresses = this.congressService.congresses$.subscribe(response => {
        this.zone.run(() => {
          this.congresses = [...response];
          this.setCongressesTable([...response]);
          this.setCongressesTableRowForms([...response]);
        });
      });

      //-------------------------------------------------------------------
      // Subscribe to S3Objects
      //-------------------------------------------------------------------

      this.subscriptions.s3objects = this.s3ObjectService.s3objects$.subscribe(response => {
        this.zone.run(() => {
            this.s3objects = [...response];
        });
      });

      //-------------------------------------------------------------------
      // Subscribe to Documents 
      //-------------------------------------------------------------------

      this.subscriptions.documents = this.documentService.documents$.subscribe(response => {
        this.zone.run(() => {
            this.documents = [...response];
            this.documentsForAutoComplete = [...response].map((document) => {
                const value = document.id;
                const display = document.MasterVersion.Title;
                return { value, display };
            });
        });
      });

      //-------------------------------------------------------------------
      // Set Form Variables Congress
      //-------------------------------------------------------------------
      this.createForm = this.formBuilder.group({
        ...new Congress,
      });
      this.createForm.get('Event').setValidators([Validators.required]);
      // this.log('CF', this.createForm);
  }

  ngOnDestroy() {
    this.subscriptions.congresses.unsubscribe();
    this.subscriptions.s3objects.unsubscribe();
  }

  ngAfterViewInit(){
  
      //-------------------------------------------------------------------
      // Material Components Initializers
      //-------------------------------------------------------------------

      this.zone.run(() => {
        this.setCongressesTable([...this.congresses]);
      });

      //-------------------------------------------------------------------
      // Subscribe to children
      //-------------------------------------------------------------------
      this.tableRowCongressInputs.changes.subscribe(change => this.tableRowCongressInputs = change);

      //-------------------------------------------------------------------
      // Selected Text Filter
      //-------------------------------------------------------------------

      this.applyFilter(this.filterValue);

  }

  /////////////////////////////////////////////////////////////////////////
  // Methods
  /////////////////////////////////////////////////////////////////////////

  //-------------------------------------------------------------------
  //  Setters
  //-------------------------------------------------------------------

  setMinMaxDates($event) {
    $event.targetElement.id === "mat-input-2" ? this.maxDate = $event.target.value : this.minDate = $event.target.value;
  }
  
  setCongressesTable(data: Array<Congress>) {
    this.CongressesTableDataSource = new MatTableDataSource(data);
    this.CongressesTableDataSource.paginator = this.CongressesTablePaginator;
    this.CongressesTableDataSource.sort = this.CongressesTableSort;
    this.CongressesTableDataSource.sort.active = 'createdAt';
    this.CongressesTableDataSource.sort.direction = 'desc';
  }

  setCongressesTableRowForms(data: Array<Congress>) {
    this.zone.runOutsideAngular(() => {
      for (let congress of data) {
        let row = this.formBuilder.group({...congress});
        row.get('Event').setValidators([Validators.required]);
        this.CongressesTableRowForms.controls[congress.id] = row;
      };
    });
  }

  setTableRowCellInputFocus(rowId, control) {
    setTimeout(() => {
        let index = this.tableRowCongressInputs.toArray().findIndex(el => el.nativeElement.id == `${rowId}-${control}`);
        if (index !== -1) { this.tableRowCongressInputs.toArray()[index].nativeElement.focus(); };
    }, 100); // DOM input initialization (ngIf)
  }

  applyFilter(filterValue: string) {
      this.CongressesTableDataSource.filter = filterValue.trim().toLowerCase();
  }

  clearFilter() {
      this.filterValue = '';
      this.CongressesTableDataSource.filter = '';
  }

  resetCongressesTableRowForm(data: Congress) {
      let row = this.formBuilder.group({ ...data });
      row.get('Event').setValidators([Validators.required]);
      this.CongressesTableRowForms.controls[data.id] = row;
  }

  resetCreateForm() {
      let reset = { ...new Congress };
      this.createForm.get('Event').reset();
      this.createForm.get('Location').reset();
      this.createForm.get('Dates').reset();
      this.createForm.get('StartDate').reset();
      this.createForm.get('EndDate').reset();
      this.createForm.get('Specialty').reset();
      this.createForm.get('Image').reset();
      this.createForm.get('Attendance').reset();
      this.createForm.get('Attendees').reset();
      this.createForm.get('Links').reset();
      this.createForm.markAsPristine();
  }  

  //-------------------------------------------------------------------
  //  CRUD
  //-------------------------------------------------------------------

  createCongress() {
    if (this.createForm.valid) {
      let congress = this.createForm.value;
      console.log(congress);

      const s3Image: S3Object = { ...this.s3objects.find(s3object => s3object.key === congress.Image.key) };
      congress.Image = s3Image.id || null;

      // assemble date range string

      const startDateMonth = congress.StartDate.toLocaleString('default', { month: 'long' });
      const startDateDay =  congress.StartDate.getDate();
      const startDateYear = congress.StartDate.getFullYear();

      const endDateMonth = congress.EndDate.toLocaleString('default', { month: 'long' });
      const endDateDay =  congress.EndDate.getDate();
      const endDateYear = congress.EndDate.getFullYear();

      const startDatePart = endDateYear > startDateYear ? `${startDateMonth} ${startDateDay}, ${startDateYear}` : `${startDateMonth} ${startDateDay}`; 

      const endDatePart = endDateMonth > startDateMonth ? `${endDateMonth} ${endDateDay}, ${endDateYear}` : `${endDateDay}, ${endDateYear}`;

      congress.Dates = `${startDatePart}–${endDatePart}`;

      this.congressService.createCongress(congress);
      this.resetCreateForm();
    } else {
      this.createForm.get('Event').markAsTouched();
    }
  }

  updateCongress(congress) {
    if(congress.Image) { congress.Image = congress.Image.id || null };

    this.congressService.updateCongress(congress);
  }
  
  deleteCongress(congressElement) {
    let congress = { ...congressElement };
    this.congressService.deleteCongress(congress);
  }

  /////////////////////////////////////////////////////////////////////////
  // Bulk
  /////////////////////////////////////////////////////////////////////////
  destroyCongresses() {
    this.congressService.destroyCongresses();
  }
  
  //-------------------------------------------------------------------
  //  Dialog Events
  //-------------------------------------------------------------------
  openHTMLEditorDialog(control: AbstractControl, options?: Object): void {
    this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
        width: '50%',
        data: {
            control,
            options
        }
    });

    this.htmlEditorDialogRef.afterClosed().subscribe(data => {
        if (data !== control.value) { control.markAsDirty(); }
        if (data && data !== '') { control.setValue(data); }
    });
}

openFileSelectDialog(control: AbstractControl): void {
  this.fileSelectDialogRef = this.dialog.open(FileSelectDialog, {
      width: '60%',
  });

  this.fileSelectDialogRef.afterClosed().subscribe(data => {
      if(!data) return;
      control.setValue(data);
      control.markAsDirty();
  });
}

openLinkEditorDialog(control: AbstractControl): void {
  this.linkDialogRef = this.dialog.open(LinkDialog, {
    width: '50%'
  });

  this.linkDialogRef.afterClosed().subscribe(data => {
    if (!data) return;
    
    let selection = data.map(link => {
        return link.id;
    });

    control.setValue(selection);
    control.markAsDirty();

});

  
}

openEditLinkMultiSelectDialog(control: AbstractControl, row: any): void {

  let linkIds = [];
      linkIds = control.value;

  // if (row && row.Links) {
  //     linkIds = this.transformLinks(row.Links);
  // }

  this.linkDialogRef = this.dialog.open(LinkDialog, {
      width: '60%',
      data: {
          linkIds
      }
  });

  this.linkDialogRef.afterClosed().subscribe(data => {
      if (!data) return;

      let selection = data.map(link => {
          return link.id;
      });

      if (control) {
          control.setValue(selection);
          control.markAsDirty();
      }

      if (row) {
          this.CongressesTableRowForms.get(row.id).get('Links').setValue(selection);
          this.CongressesTableRowForms.get(row.id).markAsDirty();
      }

  });
}


// openDocumentMultiSelectDialog(control: AbstractControl): void {

//   let documentIds = [];
//       documentIds = control.value;
//       documentIds = documentIds.map(document => document.value);

//   this.documentMultiSelectDialogRef = this.dialog.open(DocumentMultiSelectDialog, {
//       width: '60%',
//       data: {
//           documentIds
//       }
//   });

//   this.documentMultiSelectDialogRef.afterClosed().subscribe(data => {
//       if (!data) return;
      
//       let selection = data.map(masterversion => {
//           // return masterversion.Document.id;
//           return {
//               value: masterversion.Document.id,
//               display: masterversion.Title
//           }
//       });

//       selection.sort((a, b) => {
//           return documentIds.indexOf(a.value) - documentIds.indexOf(b.value);
//       });

//       control.setValue(selection);
//       control.markAsDirty();

//   });
// }


}
