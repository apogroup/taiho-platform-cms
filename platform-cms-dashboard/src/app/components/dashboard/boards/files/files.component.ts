import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';

import { LoggerService } from 'app/services/logger/logger.service';


@Component({
    selector: 'dashboard-files',
    templateUrl: './files.component.html',
    styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit, OnDestroy {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private zone: NgZone,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("FilesComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("FilesComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Subscribe to Subscriptions
        //-------------------------------------------------------------------

        // this.s3ObjectService.subscribeToS3Objects();

    }
    
    ngOnDestroy() {}

    ngAfterViewInit() {}

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

}