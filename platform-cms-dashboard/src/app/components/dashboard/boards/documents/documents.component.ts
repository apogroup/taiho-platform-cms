import { Component, OnInit, OnDestroy, ViewChild, ViewChildren, QueryList, NgZone } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource, MatSortable } from '@angular/material';
import { MatDialog, MatSelectChange, MatOption } from '@angular/material';

import { FormBuilder, FormArray, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { DocumentService } from 'app/services/dashboard/document.service';
import { S3ObjectService } from 'app/services/dashboard/s3object.service';
import { ReferenceService } from 'app/services/dashboard/reference.service';

import { Document } from 'app/models/document.model';
import { DocumentVersion } from 'app/models/documentversion.model';
import { Reference } from 'app/models/reference.model';

import { S3Object } from 'app/models/s3object.model';

import { DocumentVersionDialog } from 'app/components/dashboard/boards/documents/document-version-dialog/document-version-dialog.component';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';
import { FileSelectDialog } from 'app/components/file-select-dialog/file-select-dialog.component';
import { ReferenceMultiSelectDialog } from 'app/components/reference-multiselect-dialog/reference-multiselect-dialog.component';

import { environment } from 'environments/environment';

declare var $: any;

@Component({
    selector: 'dashboard-documents',
    templateUrl: './documents.component.html',
    styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit, OnDestroy {

    @ViewChild('masterverisonsTable', { read: MatSort }) MasterVersionsTableSort: MatSort;
    @ViewChild('tablePaginator') MasterVersionsTablePaginator: MatPaginator;
    @ViewChildren('tableRowDocumentMasterVersionInputs') tableRowDocumentMasterVersionInputs: QueryList<any>;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public documents: Array<Document> = [];
    public versions: Array<DocumentVersion> = [];
    public masterversions: Array<DocumentVersion> = [];
    public references: Array<Reference> = [];

    public s3objects: Array<S3Object> = [];

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';

    //-------------------------------------------------------------------
    // Form Variables Document 
    //-------------------------------------------------------------------
    public createForm: FormGroup;
    public referencesDisplayFormControlValue: string = '';

    //-------------------------------------------------------------------
    // Form Variables Document Table
    //-------------------------------------------------------------------
    public MasterVersionsTableForm: FormGroup;
    public MasterVersionsTableRowForms: FormArray = new FormArray([]);
    public MasterVersionsTableRowEditState: Array<boolean> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public MasterVersionsTableDisplayedColumns: string[] = [
        'VersionId',
        'Title',
        'FilePath',
        'SourceFilePath',
        'References',
        // 'ThumbnailPath',
        'Disabled',
        'Cache',
        'createdAt',
        'updatedAt',
        // 'id',
        // 'Disabled',
        'Actions',
    ];
    public MasterVersionsTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private htmlEditorDialogRef;
    private documentVersionDialogRef;
    private fileSelectDialogRef;
    private referenceMultiSelectDialogRef;

    //-------------------------------------------------------------------
    // Froala Editor Options
    //-------------------------------------------------------------------
    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: [],
        multiLine: false,
        charCounterCount: false, 
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        theme: 'dark',
    };

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private modelService: ModelService,
        private documentService: DocumentService,
        private s3ObjectService: S3ObjectService,
        private referenceService: ReferenceService,

        private formBuilder: FormBuilder,
        private zone: NgZone,
        public dialog: MatDialog,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("DocumentsComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("DocumentsComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Subscribe to Subscriptions
        //-------------------------------------------------------------------
        
        // this.documentService.subscribeToDocuments();
        // this.documentService.subscribeToDocumentVersions();

        //-------------------------------------------------------------------
        // Set Form Variables Document Table
        //-------------------------------------------------------------------
        
        this.MasterVersionsTableRowForms = this.formBuilder.array([]);
        this.MasterVersionsTableForm = this.formBuilder.group({ 'MasterVersionsTableRowForms': this.MasterVersionsTableRowForms });
        // this.log('Doc Table Forms', this.MasterVersionsTableForm);

        //-------------------------------------------------------------------
        // Subscribe to References
        //-------------------------------------------------------------------

        this.subscriptions.references = this.referenceService.references$.subscribe(response => {
            this.zone.run(() => {
                this.references = [...response];
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to All Documents
        //-------------------------------------------------------------------
        // Extrapolate Document MasterVersion[s] to masterversions
        //-------------------------------------------------------------------

        this.subscriptions.documents = this.documentService.documents$.subscribe(response => {
            this.zone.run(() => {
                this.documents = [...response];
                // this.masterversions = [...response].filter(item => item.MasterVersion).map(item => item.MasterVersion);
                // this.setMasterVersionsTable([...response].filter(item => item.MasterVersion).map(item => item.MasterVersion));
                // this.setMasterVersionsTableRowForms([...response].filter(item => item.MasterVersion).map(item => item.MasterVersion));
                this.masterversions = [...this.documents].filter(item => item.MasterVersion).map(item => {
                    let index = [...this.versions].findIndex(version => item.MasterVersion.id == version.id);
                    if (index > -1) return this.versions[index];
                });
                if(this.versions.length !== 0) {
                    this.setMasterVersionsTable([...this.masterversions]);
                    this.setMasterVersionsTableRowForms([...this.masterversions]);
                }
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to All DocumentVersions
        //-------------------------------------------------------------------

        this.subscriptions.documentversions = this.documentService.documentversions$.subscribe(response => {
            this.zone.run(() => {
                this.versions = [...response].map(version => {
                    if(!version.FilePath) { version.FilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.SourceFilePath) { version.SourceFilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.ThumbnailPath) { version.ThumbnailPath = { key: null, eTag: null, lastModified: null, size: null } }
                    return version;
                });
                this.masterversions = [...this.documents].filter(document => document.MasterVersion).map(document => {
                    let index = [...this.versions].findIndex(version => document.MasterVersion.id == version.id);
                    if (index > -1) return this.versions[index];
                    return;
                });
                this.setMasterVersionsTable([...this.masterversions]);
                this.setMasterVersionsTableRowForms([...this.masterversions]);
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to S3Objects
        //-------------------------------------------------------------------

        this.subscriptions.s3objects = this.s3ObjectService.s3objects$.subscribe(response => {
            this.zone.run(() => {
                this.s3objects = [...response];
            })
        })

        //-------------------------------------------------------------------
        // Set Form Variables Document
        //-------------------------------------------------------------------
        this.createForm = this.formBuilder.group({
            ...new DocumentVersion,
        });
        this.createForm.get('Title').setValidators([Validators.required]);
        this.createForm.get('VersionId').setValidators([Validators.required]);
        // this.log('CF', this.createForm);

    }
    

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.documents.unsubscribe();
        this.subscriptions.documentversions.unsubscribe();
        this.subscriptions.s3objects.unsubscribe();

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setMasterVersionsTable([...this.masterversions]);
        });

        //-------------------------------------------------------------------
        // Subscribe to children
        //-------------------------------------------------------------------
        // this.tableRowTagInputs.changes.subscribe(change => this.tableRowTagInputs = change);

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------

    setMasterVersionsTable(data: Array<DocumentVersion>) {
        this.MasterVersionsTableDataSource = new MatTableDataSource(data);
        this.MasterVersionsTableDataSource.paginator = this.MasterVersionsTablePaginator;
        this.MasterVersionsTableDataSource.sort = this.MasterVersionsTableSort;
        this.MasterVersionsTableDataSource.sort.active = 'createdAt';
        this.MasterVersionsTableDataSource.sort.direction = 'desc';
    }

    setMasterVersionsTableRowForms(data: Array<DocumentVersion>) {
        this.zone.runOutsideAngular(() => {
            for (let documentDocumentVersion of data) {
                let row = this.formBuilder.group({ ...documentDocumentVersion });
                    row.get('Title').setValidators([Validators.required]);

                    if (documentDocumentVersion.References) {
                        row.get('References').setValue(this.transformReferences(documentDocumentVersion.References))
                    } else {
                        row.get('References').setValue([]);
                    }

                this.MasterVersionsTableRowForms.controls[documentDocumentVersion.id] = row;
                // this.log('Doc Table Forms', this.MasterVersionsTableForm);
            };
        });
    }

    setTableRowCellInputFocus(rowId, control) {
        setTimeout(() => {
            let index = this.tableRowDocumentMasterVersionInputs.toArray().findIndex(el => el.nativeElement.id == `${rowId}-${control}`);
            if (index !== -1) { this.tableRowDocumentMasterVersionInputs.toArray()[index].nativeElement.focus(); };
        }, 100); // DOM input initialization (ngIf)
    }

    applyFilter(filterValue: string) {
        this.MasterVersionsTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.MasterVersionsTableDataSource.filter = '';
    }

    resetMasterVersionsTableRowForm(data: DocumentVersion) {
        let row = this.formBuilder.group({ ...data });
        row.get('Title').setValidators([Validators.required]);
        this.MasterVersionsTableRowForms.controls[data.id] = row;
    }

    resetCreateForm(){
        let reset = { ...new DocumentVersion };
        this.createForm.get('Title').reset(reset.Title);
        this.createForm.get('VersionId').reset(reset.VersionId);
        this.createForm.get('Disabled').reset(reset.Disabled);
        this.createForm.get('Cache').reset(reset.Cache);
        this.createForm.get('FilePath').reset(reset.FilePath);
        this.createForm.get('SourceFilePath').reset(reset.SourceFilePath);
        this.createForm.get('ThumbnailPath').reset(reset.ThumbnailPath);
        this.createForm.get('References').reset(reset.References);
        this.createForm.get('Title').setValue(reset.Title);
        this.createForm.get('VersionId').setValue(reset.VersionId);
        this.createForm.get('Disabled').setValue(reset.Disabled);
        this.createForm.get('Cache').setValue(reset.Cache);
        this.createForm.get('FilePath').setValue(reset.FilePath);
        this.createForm.get('SourceFilePath').setValue(reset.SourceFilePath);
        this.createForm.get('ThumbnailPath').setValue(reset.ThumbnailPath);
        this.createForm.get('References').setValue(reset.References);
        this.createForm.markAsPristine();
    }

    //-------------------------------------------------------------------
    //  CRUD
    //-------------------------------------------------------------------

    createDocument() {
        if(this.createForm.valid) {
            let documentversion = this.createForm.value;
                // this.log('Prepared DocumentVersion', documentversion);

            let s3FileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === documentversion.FilePath.key) };
            let s3SourceFileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === documentversion.SourceFilePath.key) };
            let s3ThumbnailObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === documentversion.ThumbnailPath.key) };

            documentversion.Filename = s3FileObject.key || "";
            documentversion.FilePath = s3FileObject.id || null;
            documentversion.SourceFilePath = s3SourceFileObject.id || null;
            documentversion.ThumbnailPath = s3ThumbnailObject.id || null;
            documentversion.References = documentversion.References || [];

            this.documentService.createDocument(documentversion);
            this.resetCreateForm();
        } else {
            this.createForm.get('Title').markAsTouched();
            this.createForm.get('VersionId').markAsTouched();
        }
    }

    deleteDocument(documentversion) {
        let document: Document = { ...this.documents.find(document => document.id === documentversion.Document || document.id === documentversion.Document.id) };
        this.documentService.deleteDocument(document);
    }

    updateDocumentVersion(documentversion){

        if(documentversion.FilePath) { 
            documentversion.Filename = documentversion.FilePath.key || '';
            documentversion.FilePath = documentversion.FilePath.id || null;
        };
        if(documentversion.SourceFilePath) { documentversion.SourceFilePath = documentversion.SourceFilePath.id || null };
        if(documentversion.ThumbnailPath) { documentversion.ThumbnailPath = documentversion.ThumbnailPath.id || null };

        this.log('UPDATEDV', {...documentversion});
        this.documentService.updateDocumentVersion(documentversion);
    }

    //-------------------------------------------------------------------
    //  Data Methods
    //-------------------------------------------------------------------

    transformReferences(references){
        let refs = references.map(item => item.id);
        return refs;
    }
    
    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------

    openDocumentVersionDialog(documentversionId: DocumentVersion['id']): void {
        let document: Document = { ...this.documents.find(document => document.MasterVersion === documentversionId || document.MasterVersion.id === documentversionId) };
        let masterversion: DocumentVersion = { ...this.masterversions.find(masterversion => masterversion.id === documentversionId) };

        console.log('document', document);
        console.log('masterversion', masterversion);

        this.documentVersionDialogRef = this.dialog.open(DocumentVersionDialog, {
            width: '90%',
            data: {
                Document: { ...document },
                MasterVersion: { ...masterversion },
                DocumentVersions: [ ...document.Versions ],
            }
        });

        this.documentVersionDialogRef.afterClosed().subscribe(form => { });
    }

    openHTMLEditorDialog(control: AbstractControl, options?: Object): void {
        this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
            width: '50%',
            data: {
                control,
                options
            }
        });

        this.htmlEditorDialogRef.afterClosed().subscribe(data => {
            if (data !== control.value) { control.markAsDirty(); }
            if (data && data !== '') { control.setValue(data); }
        });
    }

    openFileSelectDialog(control: AbstractControl): void {
        this.fileSelectDialogRef = this.dialog.open(FileSelectDialog, {
            width: '60%',
        });

        this.fileSelectDialogRef.afterClosed().subscribe(data => {
            if(!data) return;
            control.setValue(data);
            control.markAsDirty();
        });
    }

    openReferenceMultiSelectDialog(control: AbstractControl): void {
        
        let referenceIds = [];
            referenceIds = control.value;

        this.referenceMultiSelectDialogRef = this.dialog.open(ReferenceMultiSelectDialog, {
            width: '60%',
            data: {
                referenceIds
            }
        });

        this.referenceMultiSelectDialogRef.afterClosed().subscribe(data => {
            if (!data) return;
            
            let selection = data.map(masterversion => {
                return masterversion.Reference.id;
            });

            control.setValue(selection);
            control.markAsDirty();

        });
    }

    openEditReferenceMultiSelectDialog(control: AbstractControl, row: any): void {

        let referenceIds = [];
            referenceIds = control.value;

        // if (row && row.References) {
        //     referenceIds = this.transformReferences(row.References);
        // }

        this.referenceMultiSelectDialogRef = this.dialog.open(ReferenceMultiSelectDialog, {
            width: '60%',
            data: {
                referenceIds
            }
        });

        this.referenceMultiSelectDialogRef.afterClosed().subscribe(data => {
            if (!data) return;

            let selection = data.map(masterversion => {
                return masterversion.Reference.id;
            });

            if (control) {
                control.setValue(selection);
                control.markAsDirty();
            }

            if (row) {
                this.MasterVersionsTableRowForms.get(row.id).get('References').setValue(selection);
                this.MasterVersionsTableRowForms.get(row.id).markAsDirty();
            }

        });
    }

    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////
    
    destroyDocuments() {
        this.documentService.destroyDocuments();
    }

}