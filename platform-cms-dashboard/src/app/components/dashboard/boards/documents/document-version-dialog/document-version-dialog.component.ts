import { Component, OnInit, Inject, NgZone, ViewChild, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';

import { Reference } from 'app/models/reference.model';
import { Document } from 'app/models/document.model';
import { DocumentVersion } from 'app/models/documentversion.model';
import { S3Object } from 'app/models/s3object.model';

import { ReferenceService } from 'app/services/dashboard/reference.service';
import { DocumentService } from 'app/services/dashboard/document.service';
import { S3ObjectService } from 'app/services/dashboard/s3object.service';
import { LoggerService } from 'app/services/logger/logger.service';

import { TagInputComponent } from 'ngx-chips';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';
import { FileSelectDialog } from 'app/components/file-select-dialog/file-select-dialog.component';
import { ReferenceMultiSelectDialog } from 'app/components/reference-multiselect-dialog/reference-multiselect-dialog.component';

import { environment } from 'environments/environment';

declare var $: any;

@Component({
    selector: 'document-version-dialog',
    templateUrl: 'document-version-dialog.component.html',
    styleUrls: ['document-version-dialog.component.scss']
})
export class DocumentVersionDialog implements OnInit {

    @ViewChild('documentVersionsTable', { read: MatSort }) DocumentVersionsTableSort: MatSort;
    @ViewChildren('tableRowDocumentVersionInputs') tableRowDocumentVersionInputs: QueryList<any>;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public references: Array<Reference> = [];
    public documents: Array<Document> = [];
    public versions: Array<DocumentVersion> = [];
    public s3objects: Array<S3Object> = [];
    //-------------------------------------------------------------------
    // Dialog Data {} Constructors
    //-------------------------------------------------------------------
    public document: Document;
    public masterversion: DocumentVersion;
    public documentversions: Array<DocumentVersion>;

    //-------------------------------------------------------------------
    // Form Variables Subtag
    //-------------------------------------------------------------------
    public createForm: FormGroup;
    public createFormExpanded: boolean = false;

    //-------------------------------------------------------------------
    // Form Variables DocumentVersion Table
    //-------------------------------------------------------------------
    public DocumentVersionsTableForm: FormGroup;
    public DocumentVersionsTableRowForms: FormArray = new FormArray([]);
    public DocumentVersionsTableRowEditState: Array<boolean> = [];


    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public DocumentVersionsTableDisplayedColumns: string[] = [
        'Master',
        'VersionId',
        'Title',
        'FilePath',
        'SourceFilePath',
        'References',
        // 'ThumbnailPath',
        'Disabled',
        'Cache',
        'createdAt',
        'updatedAt',
        // 'id',
        // 'Disabled',
        'Actions',
    ];
    public DocumentVersionsTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private htmlEditorDialogRef;
    private fileSelectDialogRef;
    private referenceMultiSelectDialogRef;

    //-------------------------------------------------------------------
    // Froala Editor Options
    //-------------------------------------------------------------------
    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: [],
        multiLine: false,
        charCounterCount: false,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        zIndex: 999,
    };

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private documentService: DocumentService,
        private s3ObjectService: S3ObjectService,
        private referenceService: ReferenceService,

        private zone: NgZone,
        private formBuilder: FormBuilder,
        private dialog: MatDialog,
        public dialogRef: MatDialogRef<DocumentVersionDialog>,

        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("DocumentVersionDialogComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("DocumentVersionDialogComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Life-cycle Methods
    /////////////////////////////////////////////////////////////////////////

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit() {

        //-------------------------------------------------------------------
        // Deconstruct data
        //-------------------------------------------------------------------
        
        this.document = { ...this.data.Document };
        this.masterversion = { ...this.data.MasterVersion };
        this.documentversions = [ ...this.data.DocumentVersions ];

        //-------------------------------------------------------------------
        // Subscribe to References
        //-------------------------------------------------------------------

        this.subscriptions.references = this.referenceService.references$.subscribe(response => {
            this.zone.run(() => {
                this.references = [...response];
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to All Documents
        //-------------------------------------------------------------------
        // Extrapolate Document MasterVersion[s] to masterversions
        //-------------------------------------------------------------------

        this.subscriptions.documents = this.documentService.documents$.subscribe(response => {
            this.zone.run(() => {
                this.documents = [...response];
                this.document = [...response].find(item => item.id == this.document.id);
                this.masterversion = [...this.documentversions].find(item => item.id == this.document.MasterVersion.id) || this.masterversion;
                this.setDocumentVersionsTable(this.documentversions);
                this.setDocumentVersionsTableRowForms(this.documentversions);
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to All DocumentVersions
        //-------------------------------------------------------------------

        this.subscriptions.documentversions = this.documentService.documentversions$.subscribe(response => {
            this.zone.run(() => {
                this.versions = [...response].map(version => {
                    if(!version.FilePath) { version.FilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.SourceFilePath) { version.SourceFilePath = { key: null, eTag: null, lastModified: null, size: null } }
                    if(!version.ThumbnailPath) { version.ThumbnailPath = { key: null, eTag: null, lastModified: null, size: null } }
                    return version;
                });
                this.documentversions = [...response].filter(item => item.Document && item.Document.id == this.document.id);
                this.masterversion = [...response].find(item => item.id == this.document.MasterVersion.id) || this.masterversion;
                this.setDocumentVersionsTable([...response].filter(item => item.Document && item.Document.id == this.document.id));
                this.setDocumentVersionsTableRowForms([...response].filter(item => item.Document && item.Document.id == this.document.id));
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to S3Objects
        //-------------------------------------------------------------------

        this.subscriptions.s3objects = this.s3ObjectService.s3objects$.subscribe(response => {
            this.zone.run(() => {
                this.s3objects = [...response];
            })
        })

        //-------------------------------------------------------------------
        // Set Form Variables Document
        //-------------------------------------------------------------------
        this.createForm = this.formBuilder.group({
            ...new DocumentVersion,
        });
        this.createForm.get('Title').setValidators([Validators.required]);
        this.createForm.get('VersionId').setValidators([Validators.required]);

        //-------------------------------------------------------------------
        // Set Form Variables Document Table
        //-------------------------------------------------------------------
        this.DocumentVersionsTableRowForms = this.formBuilder.array([]);
        this.DocumentVersionsTableForm = this.formBuilder.group({ 'DocumentVersionsTableRowForms': this.DocumentVersionsTableRowForms });
        this.log('Doc Table Forms', this.DocumentVersionsTableForm);
        this.setDocumentVersionsTableRowForms(this.documentversions);

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setDocumentVersionsTable([...this.documentversions]);
        });

    }

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.documents.unsubscribe();
        this.subscriptions.documentversions.unsubscribe();

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  DocumentVersions
    //-------------------------------------------------------------------

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------

    setDocumentVersionsTable(data: Array<DocumentVersion>) {
        this.DocumentVersionsTableDataSource = new MatTableDataSource(data);
        this.DocumentVersionsTableDataSource.sort = this.DocumentVersionsTableSort;
        this.DocumentVersionsTableDataSource.sort.active = 'createdAt';
        this.DocumentVersionsTableDataSource.sort.direction = 'desc';
    }

    setDocumentVersionsTableRowForms(data: Array<DocumentVersion>) {
        this.zone.runOutsideAngular(() => {
            for (let documentDocumentVersion of data) {
                let row = this.formBuilder.group({ ...documentDocumentVersion });
                row.get('Title').setValidators([Validators.required]);

                if (documentDocumentVersion.References) {
                    row.get('References').setValue(this.transformReferences(documentDocumentVersion.References))
                } else {
                    if(row.get('References')) {
                        row.get('References').setValue([]);
                    }
                }

                this.DocumentVersionsTableRowForms.controls[documentDocumentVersion.id] = row;
                this.log('Doc Table Forms', this.DocumentVersionsTableForm);
            };
        });
    }

    setTableRowCellInputFocus(rowId, control) {
        this.log('RF: ', this.DocumentVersionsTableRowForms.controls[rowId]);
        setTimeout(() => {
            let index = this.tableRowDocumentVersionInputs.toArray().findIndex(el => el.nativeElement.id == `${rowId}-${control}`);
            this.log('RF Index', [index, this.tableRowDocumentVersionInputs.toArray()[index]]);
            if (index !== -1) { this.tableRowDocumentVersionInputs.toArray()[index].nativeElement.focus(); };
        }, 100); // DOM input initialization (ngIf)
    }

    resetDocumentVersionsTableRowForm(data: DocumentVersion) {
        let row = this.formBuilder.group({ ...data });
        row.get('Title').setValidators([Validators.required]);
        this.DocumentVersionsTableRowForms.controls[data.id] = row;
    }

    resetCreateForm() {
        let reset = { ...new DocumentVersion };
        this.createForm.get('Title').reset(reset.Title);
        this.createForm.get('VersionId').reset(reset.VersionId);
        this.createForm.get('Disabled').reset(reset.Disabled);
        this.createForm.get('Cache').reset(reset.Cache);
        this.createForm.get('FilePath').reset(reset.FilePath);
        this.createForm.get('SourceFilePath').reset(reset.SourceFilePath);
        this.createForm.get('ThumbnailPath').reset(reset.ThumbnailPath);
        this.createForm.get('Title').setValue(reset.Title);
        this.createForm.get('VersionId').setValue(reset.VersionId);
        this.createForm.get('Disabled').setValue(reset.Disabled);
        this.createForm.get('Cache').setValue(reset.Cache);
        this.createForm.get('FilePath').setValue(reset.FilePath);
        this.createForm.get('SourceFilePath').setValue(reset.SourceFilePath);
        this.createForm.get('ThumbnailPath').setValue(reset.ThumbnailPath);
        this.createForm.markAsPristine();
    }

    populateCreateForm(documentversion) {
        this.createForm.get('Title').setValue(documentversion.Title);
        this.createForm.get('VersionId').setValue(documentversion.VersionId);
        this.createForm.get('Disabled').setValue(documentversion.Disabled);
        this.createForm.get('Cache').setValue(documentversion.Cache);
        this.createForm.get('FilePath').setValue(documentversion.FilePath);
        this.createForm.get('ThumbnailPath').setValue(documentversion.ThumbnailPath);
        this.createForm.markAsDirty();
    }

    //-------------------------------------------------------------------
    //  CRUD
    //-------------------------------------------------------------------

    createDocumentVersion(master?: boolean) {

        if(this.createForm.valid) {
            let documentversion = { ...this.createForm.value };

            let s3FileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === documentversion.FilePath.key) };
            let s3SourceFileObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === documentversion.SourceFilePath.key) };
            let s3ThumbnailObject: S3Object = { ...this.s3objects.find(s3object => s3object.key === documentversion.ThumbnailPath.key) };

            documentversion.Filename = s3FileObject.key || "";
            documentversion.FilePath = s3FileObject.id || null;
            documentversion.SourceFilePath = s3SourceFileObject.id || null;
            documentversion.ThumbnailPath = s3ThumbnailObject.id || null;

            this.documentService.createDocumentVersion(documentversion, this.document, master);
            this.resetCreateForm();
        } else {
            this.createForm.get('Title').markAsTouched();
            this.createForm.get('VersionId').markAsTouched();
        }
    }

    setAsMasterVersion(documentversion) {
        let document = { ...this.document };
        document.MasterVersion = documentversion.id;
        this.documentService.updateDocument(document);
    }

    updateDocumentVersion(documentversion) {

        if(documentversion.FilePath) { 
            documentversion.Filename = documentversion.FilePath.key || '';
            documentversion.FilePath = documentversion.FilePath.id || null;
        };
        if(documentversion.SourceFilePath) { documentversion.SourceFilePath = documentversion.SourceFilePath.id || null };
        if(documentversion.AnnotatedFilePath) { documentversion.AnnotatedFilePath = documentversion.AnnotatedFilePath.id || null };
        if(documentversion.ThumbnailPath) { documentversion.ThumbnailPath = documentversion.ThumbnailPath.id || null };

        // this.log('UPDATEDV', {...documentversion});
        this.documentService.updateDocumentVersion(documentversion);
    }
    
    deleteDocumentVersion(documentversion) {
        this.documentService.deleteDocumentVersion(documentversion);
    }

    //-------------------------------------------------------------------
    //  Data Methods
    //-------------------------------------------------------------------

    transformReferences(references) {
        let refs = references.map(item => item.id);
        return refs;
    }

    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------

    openHTMLEditorDialog(control: AbstractControl, options?: Object): void {
        this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
            width: '50%',
            data: {
                control,
                options,
            }
        });

        this.htmlEditorDialogRef.afterClosed().subscribe(data => {
            if (data !== control.value) { control.markAsDirty(); }
            if (data && data !== '') { control.setValue(data); }
        });
    }

    openFileSelectDialog(control: AbstractControl): void {
        this.fileSelectDialogRef = this.dialog.open(FileSelectDialog, {
            width: '60%',
        });

        this.fileSelectDialogRef.afterClosed().subscribe(data => {
            if (!data) return;
            control.setValue(data);
            control.markAsDirty();
        });
    }

    openReferenceMultiSelectDialog(control: AbstractControl): void {
        
        let referenceIds = [];
            referenceIds = control.value;

        this.referenceMultiSelectDialogRef = this.dialog.open(ReferenceMultiSelectDialog, {
            width: '60%',
            data: {
                referenceIds
            }
        });

        this.referenceMultiSelectDialogRef.afterClosed().subscribe(data => {
            if (!data) return;
            
            let selection = data.map(masterversion => {
                return masterversion.Reference.id;
            });

            control.setValue(selection);
            control.markAsDirty();

        });
    }

    openEditReferenceMultiSelectDialog(control: AbstractControl, row: any): void {

        let referenceIds = [];
            referenceIds = control.value;

        // if (row && row.References) {
        //     referenceIds = this.transformReferences(row.References);
        // }

        this.referenceMultiSelectDialogRef = this.dialog.open(ReferenceMultiSelectDialog, {
            width: '60%',
            data: {
                referenceIds
            }
        });

        this.referenceMultiSelectDialogRef.afterClosed().subscribe(data => {
            if (!data) return;

            let selection = data.map(masterversion => {
                return masterversion.Reference.id;
            });

            if (control) {
                control.setValue(selection);
                control.markAsDirty();
            }

            if (row) {
                this.DocumentVersionsTableRowForms.get(row.id).get('References').setValue(selection);
                this.DocumentVersionsTableRowForms.get(row.id).markAsDirty();
            }

        });
    }
    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////

}