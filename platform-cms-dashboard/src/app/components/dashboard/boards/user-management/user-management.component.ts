import { Component, OnInit, OnDestroy, ViewChild, NgZone } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { AbstractControl, FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { User } from 'app/models/user.model';

@Component({
    selector: 'dashboard-user-management',
    templateUrl: './user-management.component.html',
    styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit, OnDestroy {

    @ViewChild('usersTable', { read: MatSort }) UsersTableSort: MatSort;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public users: Array<User> = [];

    public rows: FormArray = this.formBuilder.array([]);
    public form: FormGroup = this.formBuilder.group({ 'users': this.rows });


    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public UsersTableDisplayedColumns: string[] = [
        'Connected',
        'id',
        'createdAt',
        'updatedAt',
        'Email',
        'Name',
        'Groups',
        // 'Disabled',
    ];
    // public UsersTableDataSource = new BehaviorSubject<AbstractControl[]>([]);
    public UsersTableDataSource = new MatTableDataSource([]);

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private modelService: ModelService,
        private userService: UserService,
        private formBuilder: FormBuilder,
        private zone: NgZone,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("UserManagementComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );
    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("UserManagementComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
		// Subscribe to Subscriptions
		//-------------------------------------------------------------------

        // this.userService.subscribeToAllUsers();
        // Already subscribed in dashboard
        
        //-------------------------------------------------------------------
        // Subscribe to All Users
        //-------------------------------------------------------------------

        this.subscriptions.users = this.userService.users$.subscribe(response => {
            this.zone.run(() => {
                this.users = [...response];
                this.setUsersTable([...response]);
            });
        });


    }

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.users.unsubscribe();

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setUsersTable([...this.users]);
        });

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Users
    //-------------------------------------------------------------------

    setUsersTable(data: Array<User>) {
        this.UsersTableDataSource = new MatTableDataSource(data);
        this.UsersTableDataSource.sort = this.UsersTableSort;
        this.UsersTableDataSource.sort.active = 'createdAt';
        this.UsersTableDataSource.sort.direction = 'desc';
    }

    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////
    refreshAllUsers() {
        this.subscriptions.refreshAllUsers = this.modelService.get$('refresh/users').subscribe((response: any) => {
            this.zone.run(() => {
                this.log("Bulk Operation: Refreshed Users", response);
            });
        });
    }

    setAllUsersPage(page: string) {
        let body = {
            page: page
        };
        
        this.subscriptions.setPage = this.modelService.create$('set/page', body).subscribe((response: any) => {
            this.zone.run(() => {
                this.log("Bulk Operation: Refreshed By User", response);
            });
        });
    }

}