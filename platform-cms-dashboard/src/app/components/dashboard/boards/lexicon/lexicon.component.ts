import { Component, OnInit, OnDestroy, ViewChild, ViewChildren, QueryList, NgZone } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';

import { FormBuilder, FormArray, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { LexiconService } from 'app/services/dashboard/lexicon.service';

import { LexiconTerm } from 'app/models/lexiconterm.model';
import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';

import { environment } from 'environments/environment';

declare var $: any;
declare var window: any;

@Component({
    selector: 'dashboard-lexicon',
    templateUrl: './lexicon.component.html',
    styleUrls: ['./lexicon.component.scss']
})
export class LexiconComponent implements OnInit, OnDestroy {

    @ViewChild('termsTable', { read: MatSort }) TermsTableSort: MatSort;
    @ViewChild('tablePaginator') TermsTablePaginator: MatPaginator;
    @ViewChildren('tableRowTermInputs') tableRowTermInputs: QueryList<any>;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public terms: Array<LexiconTerm> = [];

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';

    //-------------------------------------------------------------------
    // Form Variables Term
    //-------------------------------------------------------------------
    public createForm: FormGroup;

    //-------------------------------------------------------------------
    // Form Variables Term Table
    //-------------------------------------------------------------------
    public TermsTableForm: FormGroup;
    public TermsTableRowForms: FormArray = new FormArray([]);
    public TermsTableRowEditState: Array<boolean> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public TermsTableDisplayedColumns: string[] = [
        'Term',
        'Example',
        'Rationale',
        'Disabled',
        'createdAt',
        'updatedAt',
        // 'id',
        'Actions',
    ];
    public TermsTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private htmlEditorDialogRef;

    //-------------------------------------------------------------------
    // Froala Editor Options
    //-------------------------------------------------------------------
    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', '|', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: ['reference', 'codeBeautifier', 'codeView'],
        multiLine: false,
        charCounterCount: false,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        theme: 'dark',
        codeMirror: window.CodeMirror,
        codeMirrorOptions: {
            indentWithTabs: true,
            lineNumbers: true,
            lineWrapping: true,
            mode: 'text/html',
            // theme: 'monokai',
            tabMode: 'indent',
            tabSize: 4
        }
    };

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private modelService: ModelService,
        private lexiconService: LexiconService,

        private formBuilder: FormBuilder,
        private zone: NgZone,
        public dialog: MatDialog,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("LexiconComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("LexiconComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------
        this.applyFilter(this.filterValue);

        //-------------------------------------------------------------------
        // Subscribe to Subscriptions
        //-------------------------------------------------------------------

        // this.lexiconService.subscribeToTerms();

        //-------------------------------------------------------------------
        // Set Form Variables Term Table
        //-------------------------------------------------------------------
        
        this.TermsTableRowForms = this.formBuilder.array([]);
        this.TermsTableForm = this.formBuilder.group({ 'TermsTableRowForms': this.TermsTableRowForms });

        //-------------------------------------------------------------------
        // Subscribe to All LexiconTerms
        //-------------------------------------------------------------------

        this.subscriptions.terms = this.lexiconService.terms$.subscribe(response => {
            this.zone.run(() => {
                this.terms = [...response];
                this.setTermsTable([...response]);
                this.setTermsTableRowForms([...response]);
            });
        });

        //-------------------------------------------------------------------
        // Set Form Variables Term
        //-------------------------------------------------------------------
        this.createForm = this.formBuilder.group({
            ...new LexiconTerm,
        });
        this.createForm.get('Term').setValidators([Validators.required]);

    }
    

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.terms.unsubscribe();

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setTermsTable([...this.terms]);
        });

        //-------------------------------------------------------------------
        // Subscribe to children
        //-------------------------------------------------------------------
        this.tableRowTermInputs.changes.subscribe(change => this.tableRowTermInputs = change);

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        this.applyFilter(this.filterValue);


    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------

    setTermsTable(data: Array<LexiconTerm>) {
        this.TermsTableDataSource = new MatTableDataSource(data);
        this.TermsTableDataSource.paginator = this.TermsTablePaginator;
        this.TermsTableDataSource.sort = this.TermsTableSort;
        this.TermsTableDataSource.sort.active = 'createdAt';
        this.TermsTableDataSource.sort.direction = 'desc';
    }

    setTermsTableRowForms(data: Array<LexiconTerm>) {
        this.zone.runOutsideAngular(() => {
            for (let term of data) {
                let row = this.formBuilder.group({ ...term });
                row.get('Term').setValidators([Validators.required]);
                this.TermsTableRowForms.controls[term.id] = row;
            };
        });
    }

    setTableRowCellInputFocus(rowId, control) {
        setTimeout(() => {
            let index = this.tableRowTermInputs.toArray().findIndex(el => el.nativeElement.id == `${rowId}-${control}`);
            if (index !== -1) { this.tableRowTermInputs.toArray()[index].nativeElement.focus(); };
        }, 100); // DOM input initialization (ngIf)
    }

    applyFilter(filterValue: string) {
        this.TermsTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.TermsTableDataSource.filter = '';
    }

    resetTermsTableRowForm(data: LexiconTerm) {
        let row = this.formBuilder.group({ ...data });
        row.get('Term').setValidators([Validators.required]);
        this.TermsTableRowForms.controls[data.id] = row;
    }

    resetCreateForm() {
        let reset = { ...new LexiconTerm };
        this.createForm.get('Term').reset();
        this.createForm.get('Example').reset();
        this.createForm.get('Rationale').reset();
        this.createForm.get('Disabled').reset();
        this.createForm.get('Term').setValue(reset.Term);
        this.createForm.get('Example').setValue(reset.Example);
        this.createForm.get('Rationale').setValue(reset.Example);
        this.createForm.get('Disabled').setValue(reset.Disabled);
        this.createForm.markAsPristine();
    }

    //-------------------------------------------------------------------
    //  CRUD
    //-------------------------------------------------------------------

    createTerm() {
        if (this.createForm.valid) {
            let term = this.createForm.value;
            this.lexiconService.createTerm(term);
            this.resetCreateForm();
        } else {
            this.createForm.get('Term').markAsTouched();
        }
    }

    updateTerm(term) {
        this.lexiconService.updateTerm(term);
    }

    deleteTerm(termElement) {
        let term = { ...termElement };
        this.lexiconService.deleteTerm(term);
    }

    /////////////////////////////////////////////////////////////////////////
    // Bulk
    /////////////////////////////////////////////////////////////////////////
    
    destroyTerms() {
        this.lexiconService.destroyTerms();
    }

    //-------------------------------------------------------------------
    //  Methods
    //-------------------------------------------------------------------

    openHTMLEditorDialog(control: AbstractControl, options?: Object): void {
        this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
            width: '50%',
            data: {
                control,
                options
            }
        });

        this.htmlEditorDialogRef.afterClosed().subscribe(data => {
            if (data !== control.value) { control.markAsDirty(); }
            if (data && data !== '') { control.setValue(data); }
        });
    }
}