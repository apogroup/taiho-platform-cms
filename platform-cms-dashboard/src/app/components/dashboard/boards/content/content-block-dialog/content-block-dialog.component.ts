import { Component, OnInit, OnDestroy, Inject, NgZone, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatCheckbox } from '@angular/material';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

import { Content } from 'app/models/content.model';
import { ContentService } from 'app/services/dashboard/content.service';
import { DocumentService } from 'app/services/dashboard/document.service';
import { ReferenceService } from 'app/services/dashboard/reference.service';

import { Document } from 'app/models/document.model';
import { Reference } from 'app/models/reference.model';

// import { ReferenceSelectDialog } from 'app/components/reference-select-dialog/reference-select-dialog.component';
import { DocumentMultiSelectDialog } from 'app/components/document-multiselect-dialog/document-multiselect-dialog.component';

import { environment } from 'environments/environment';

import { chain, find, map, partialRight, property, isNil } from 'lodash';
import { Status } from 'app/types/status.type';

declare var $: any;
declare var window: any;

@Component({
    selector: 'content-block-dialog',
    templateUrl: 'content-block-dialog.component.html',
    styleUrls: ['content-block-dialog.component.scss']
})
export class ContentBlockDialog implements OnInit, OnDestroy {

    @ViewChild('ngxEditor') ngxEditor: any;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    // tslint:disable-next-line: comment-format
    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public documents: Array<Document> = [];
    public references: Array<Reference> = [];
    public documentsForAutoComplete: Array<any> = [];

    //-------------------------------------------------------------------
    // Model
    //-------------------------------------------------------------------
    public parent;

    //-------------------------------------------------------------------
    // Form
    //-------------------------------------------------------------------
    public formControls: any = {
        'Descriptor': { validation: [Validators.required] },
        'Type': { validation: [Validators.required] },
        'Route': { validation: [Validators.pattern('[A-Za-z0-9\-]*')] },
        'Label': {},
        'Title': {},
        'Body': {},
        'Documents': { lens: (name: string, content: Content) => map(content[name], 'id') },
        'References': { lens: (name: string, content: Content) => map(content[name], 'id') },
        'Footnotes': {},
        'Abbreviations': {},
        'Disabled': { lens: (name: string, content: Content) => content.Status === 'disabled' }
    };

    //-------------------------------------------------------------------
    // Editor Options
    //-------------------------------------------------------------------
    public editor: any;

    public inlineEditorOptions: Object = {
        key: environment.froala.key,
        toolbarInline: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'undo', 'redo'],
        showNextToCursor: true,
        pluginsEnabled: [],
        multiLine: true,
        charCounterCount: false,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        enter: $.FroalaEditor.ENTER_BR,
        theme: 'dark',
        zIndex: 999,
        immediateAngularModelUpdate: true
    };

    public editorOptions: Object = {
        key: environment.froala.key,
        toolbarButtons: ['bold', 'italic', 'underline', '|', 'superscript', 'subscript', '|', 'referenceSelect', '|', 'outdent', 'indent', 'formatOL', 'formatUL', '|', 'undo', 'redo', '|', 'html'],
        pluginsEnabled: ['reference', 'lists', 'paragraphFormat ', 'charCounter', 'codeBeautifier', 'codeView'],
        multiLine: true,
        charCounterCount: true,
        placeholderText: '...',
        pastePlain: true,
        pasteDeniedTags: ['table', 'th', 'tr', 'td'],
        // enter: $.FroalaEditor.ENTER_BR,
        zIndex: 999,
        codeMirror: window.CodeMirror,
        codeMirrorOptions: {
            indentWithTabs: true,
            lineNumbers: true,
            lineWrapping: true,
            mode: 'text/html',
            // theme: 'monokai',
            tabMode: 'indent',
            tabSize: 4
        },
        immediateAngularModelUpdate: true
    };
    public editorOptionsBody: Object = {
        ...this.editorOptions,
    };
    public editorOptionsTitle: Object = { ...this.editorOptions };

    //-------------------------------------------------------------------
    // Form Variables Reference 
    //-------------------------------------------------------------------
    public createForm: FormGroup;
    public createFormExpandedStates = {
        title: false,
        body: false,
        footnotes: false,
        abbreviations: false,
    }

    public contentStatus: Status = 'enabled';

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    // private referenceSelectDialog;
    private documentMultiSelectDialogRef;

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private contentService: ContentService,
        private documentService: DocumentService,
        private referenceService: ReferenceService,
        private formBuilder: FormBuilder,
        private zone: NgZone,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<ContentBlockDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) { }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods & Close
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Subscribe to Documents & References
        //-------------------------------------------------------------------

        this.subscriptions.documents = this.documentService.documents$.subscribe(response => {
            this.zone.run(() => {
                this.documents = [...response];
                this.documentsForAutoComplete = [...response].map((document) => {
                    const value = document.id;
                    const display = document.MasterVersion.Title;
                    return { value, display };
                });
            });
        });

        this.subscriptions.references = this.referenceService.references$.subscribe(response => {
            this.zone.run(() => {
                this.references = [...response];
            });
        });

        //-------------------------------------------------------------------
        // Initialize Editor Options
        //-------------------------------------------------------------------
        this.parent = this.data.parent;

        //-------------------------------------------------------------------
        // Froala: Initialize Editor Options
        //-------------------------------------------------------------------
        this.editorOptions = { ...this.editorOptions, ...this.data.options };
        this.editorOptionsBody = {
            ...this.editorOptions,
            heightMin: 400,
            toolbarButtons: ['bold', 'italic', 'underline', '|', 'superscript', 'subscript', '|', 'referenceSelect', 'figureSelect', '|', 'outdent', 'indent', 'formatOL', 'formatUL', '|', 'undo', 'redo', '|', 'html'],
            pluginsEnabled: ['reference', 'figure', 'lists', 'paragraphFormat ', 'charCounter', 'codeBeautifier', 'codeView']
        };
        this.editorOptionsTitle = {
            ...this.editorOptions,
            enter: $.FroalaEditor.ENTER_BR
        };

        //-------------------------------------------------------------------
        // Set Form Variables Reference
        //-------------------------------------------------------------------
        const controlsConfig = this.createFormControlsConfig(new Content);
        this.createForm = this.formBuilder.group(controlsConfig);
        console.log('CF', this.createForm);
    }

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.documents.unsubscribe();
        this.subscriptions.references.unsubscribe();

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------

    createFormControlsConfig(content) {

        const defaults = new Content;

        const controls: any = Object.keys(this.formControls) // Array<string>
            .reduce((controls, key) => {

                const control = this.formControls[key];

                const validation = 'validation' in control ?
                    Validators.compose(control.validation) :
                    null;

                const field = (control.lens && control.lens(key, content)) || content[key] || defaults[key];
                return {
                    ...controls,
                    [key]: [field, validation]
                };
            }, {});

        return controls;
    }

    resetCreateForm() {
        let controlsConfig = this.createFormControlsConfig(new Content);
        this.createForm = this.formBuilder.group(controlsConfig);
        this.createForm.markAsPristine();
    }

    setContentBlockStatus(checked: boolean){
        checked === false ? this.contentStatus = 'enabled' : this.contentStatus = 'disabled';
    }

    createContent() {
        if (this.createForm.valid) {
            const formFieldValues = { ...this.createForm.value };
            
            formFieldValues.DocumentsOrder = formFieldValues.Documents.map(obj => obj.value);
            formFieldValues.Documents = formFieldValues.Documents.map(obj => this.documents.find(document => document.id === obj.value));

            formFieldValues.Status = formFieldValues.Disabled ? 'disabled' : 'enabled';
            delete formFieldValues.Disabled;

            const content = {
                ...new Content,
                ...formFieldValues
            };

            if (this.parent) {
                content.Parents = [this.parent.id]
            }

            // Attach references to content
            const contentHTML = `<body>${[content.Body, content.Footnotes, content.Title].join('')}</body>`; // single node
            const contentReferenceNodes = $(contentHTML).find('sup[type="reference"]');

            content.References = chain($.makeArray(contentReferenceNodes))
                .map(({ innerText }) => parseInt(innerText))
                .map(UniqueID => find(this.references, { UniqueID }))
                .reject(isNil)
                .uniq()
                .value();

            // Attach figures(s3object) to content
            const contentBodyHTML = `<body>${content.Body}</body>`;
            const contentBodyDocument= new DOMParser().parseFromString(contentBodyHTML, "text/html");
            const contentFigureNodes = chain(contentBodyDocument.querySelectorAll('[data-figure]'))
                .map(figure => figure.getAttribute('data-figure'))
                .uniq()
                .value();

            content.Figures = contentFigureNodes;

            this.contentService.createContent(content);
            this.close();
        } else {
            this.createForm.get('Descriptor').markAsTouched();
            this.createForm.get('Type').markAsTouched();
        }
    }

    //-------------------------------------------------------------------
    // Dialogs
    //-------------------------------------------------------------------

    openDocumentMultiSelectDialog(control: AbstractControl): void {

        let documentIds = [];
            documentIds = control.value;
            documentIds = documentIds.map(document => document.value);

        this.documentMultiSelectDialogRef = this.dialog.open(DocumentMultiSelectDialog, {
            width: '60%',
            data: {
                documentIds
            }
        });

        this.documentMultiSelectDialogRef.afterClosed().subscribe(data => {
            if (!data) return;
            
            let selection = data.map(masterversion => {
                // return masterversion.Document.id;
                return {
                    value: masterversion.Document.id,
                    display: masterversion.Title
                }
            });

            selection.sort((a, b) => {
                return documentIds.indexOf(a.value) - documentIds.indexOf(b.value);
            });

            control.setValue(selection);
            control.markAsDirty();

        });
    }

    //-------------------------------------------------------------------
    // Froala Custom Button f(x)
    //-------------------------------------------------------------------

    // selectReferenceForMarkupInjection() {
    //     //-------------------------------------------------------------------
    //     // Zone otherwise will conflict with existing dialog
    //     //-------------------------------------------------------------------
    //     this.zone.run(() => {

    //         this.referenceSelectDialog = this.dialog.open(ReferenceSelectDialog, {
    //             width: '60%',
    //             data: {
    //                 selectedText: this.editor.selection.text(),
    //             }
    //         })

    //         this.referenceSelectDialog.afterClosed().subscribe(data => {
    //             if (data) { 
    //                 this.editor.html.insert(data);
    //                 this.editor.format.toggle('sup'); // First in
    //                 this.editor.format.toggle('sup'); // then out
    //             }
    //         });

    //     });
    // }
}