import { Component, OnInit, OnDestroy, Input, NgZone, HostBinding } from '@angular/core';
import { MatDialog } from '@angular/material';

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { LoggerService } from 'app/services/logger/logger.service';
import { ContentService } from 'app/services/dashboard/content.service';
import { DocumentService } from 'app/services/dashboard/document.service';

import { Content } from 'app/models/content.model';
import { Document } from 'app/models/document.model';

import { ContentBlockDialog } from 'app/components/dashboard/boards/content/content-block-dialog/content-block-dialog.component';
import { ContentBlockEditDialog } from 'app/components/dashboard/boards/content/content-block-edit-dialog/content-block-edit-dialog.component';
import { Status } from 'app/types/status.type';

import { map } from 'rxjs/operators';

@Component({
    selector: 'content-block',
    templateUrl: './content-block.component.html',
    styleUrls: ['./content-block.component.scss']
})
export class ContentBlockComponent implements OnInit, OnDestroy {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    @Input() content: Content;

    @HostBinding('class.content-block-disabled') get contentBlockDisabledClass(){ 
        return this.content.Status === 'disabled' ? this.content.Status : false ; 
    } 

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public contents: Array<Content> = [];
    public children: Array<Content> = [];
    public documents: Array<Document> = [];

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private contentBlockDialogRef;
    private contentBlockEditDialogRef;

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private contentService: ContentService,
        private documentService: DocumentService,
        private zone: NgZone,
        private dialog: MatDialog,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("ContentBlockComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("ContentBlockComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Subscribe to All Contents
        //-------------------------------------------------------------------

        this.subscriptions.contents = this.contentService.contents$.pipe(
            map((content: Content[] ) => content.filter((record: Content) => record.Status !== 'archived'))
        ).subscribe(response => {
            this.zone.run(() => {
                this.contents = [...response];
                this.children = [...response].filter(item => item.Parents && item.Parents.length > 0 && item.Parents[0].id == this.content.id).sort((a: Content, b: Content) => a.Order - b.Order);
                // this.log('CONTENT', this.content);
                // this.log('CONTENTS', this.contents);
                // this.log('CHILDREN', this.children);
            });
        });

        //-------------------------------------------------------------------
        // Subscribe to Documents
        //-------------------------------------------------------------------

        this.subscriptions.documents = this.documentService.documents$.subscribe(response => {
            this.zone.run(() => {
                this.documents = [...response];
            });
        });

    }

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.contents.unsubscribe();
    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    openContentBlockDialog(content: Content): void {
        this.contentBlockDialogRef = this.dialog.open(ContentBlockDialog, {
            width: '90%',
            data: {
                parent: content
            }
        });

        this.contentBlockDialogRef.afterClosed().subscribe(data => {
        });
    }

    openContentBlockEditDialog(content: Content): void {
        this.contentBlockEditDialogRef = this.dialog.open(ContentBlockEditDialog, {
            width: '90%',
            data: {
                parent: content,
                content: content
            }
        });

        this.contentBlockEditDialogRef.afterClosed().subscribe(data => {
        });
    }

    deleteContentBlock(content: Content){
        // this.contentService.deleteContent(content);



    }

    setContentBlockStatus(status: Status, content: Content){
        content.Status = status;
        this.contentService.updateContent(content);
    }


    getDocumentTitle(documentId: string) {
        let document = this.documents.find(document => document.id === documentId);
        if (document && document.MasterVersion) {
            return document.MasterVersion.Title;
        } else {
            return 'Document Not Found';
        }
    }

    onDropListDropped(event: CdkDragDrop<Content[]>){
        if(event.isPointerOverContainer){
            // Be sure to not mutate original array...
            const childrenBeforeSort: Content[] = [...this.children];
            // ...updates array in place
            moveItemInArray(this.children, event.previousIndex, event.currentIndex);

            const childrenToPush: Content[] = [];
            for (const [index, child] of Array.from(this.children.entries())){
                if (childrenBeforeSort[index].id === child.id && child.Order === index) {
                    continue;
                } else {
                    child.Order = index;
                    childrenToPush.push(child);
                }
            }

            this.contentService.updateManyContent(childrenToPush);
        }
    }

}