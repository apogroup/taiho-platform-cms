import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { LoggerService } from 'app/services/logger/logger.service';
import { ContentService } from 'app/services/dashboard/content.service';

import { ContentBlockDialog } from 'app/components/dashboard/boards/content/content-block-dialog/content-block-dialog.component';

import { Content } from 'app/models/content.model';

import { map } from 'rxjs/operators';

@Component({
    selector: 'dashboard-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, OnDestroy {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public contents: Array<Content> = [];
    public orphans: Array<Content> = [];

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private contentBlockDialogRef;

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private contentService: ContentService,
        private zone: NgZone,
        private dialog: MatDialog,
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("ContentComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("ContentComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Subscribe to Subscriptions
        //-------------------------------------------------------------------

        // this.contentService.subscribeToContents();

        //-------------------------------------------------------------------
        // Subscribe to All LexiconTerms
        //-------------------------------------------------------------------

        this.subscriptions.contents = this.contentService.contents$
            .pipe(
                map((content: Content[] ) => content.filter((record: Content) => record.Status !== 'archived'))
            )
            .subscribe(response => {
            this.zone.run(() => {
                this.contents = [...response];
                this.orphans = [...response]
                  .filter(item => item.Parents && item.Parents.length == 0)
                  .sort((a: Content, b: Content) => a.Order - b.Order);
                // this.log('CONTENTS', this.contents);
                // this.log('ORPHANS', this.orphans);
            });
        });

    }
    
    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.contents.unsubscribe();
    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    openContentBlockDialog(): void {
        this.log('opencontentblock', ContentBlockDialog);
        this.contentBlockDialogRef = this.dialog.open(ContentBlockDialog, {
            width: '90%',
            data: {
                parent: false,
            }
        });

        this.contentBlockDialogRef.afterClosed().subscribe(data => {
        });
    }

    onDropListDropped(event: CdkDragDrop<Content[]>){
        if(event.isPointerOverContainer){
            // Be sure to not mutate original array...
            const orphansBeforeSort: Content[] = [...this.orphans];
            // ...updates array in place
            moveItemInArray(this.orphans, event.previousIndex, event.currentIndex);

            const orphansToUpdate: Content[] = [];
            for (const [index, orphan] of Array.from(this.orphans.entries())){
                if (orphansBeforeSort[index].id === orphan.id && orphan.Order === index) {
                    continue;
                } else {
                    orphan.Order = index;
                    orphansToUpdate.push(orphan);
                }
            }

            this.contentService.updateManyContent(orphansToUpdate);
        }
    }

}