import { Component, OnInit, NgZone, QueryList, ViewChildren, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort, MatDialogRef } from '@angular/material';
import { environment } from 'environments/environment';
import { LoggerService } from 'app/services/logger/logger.service';
import { LinkService } from 'app/services/dashboard/link.service';
import { Link } from 'app/models/link.model';
import { HTMLEditorDialog } from '../htmleditor-dialog/htmleditor-dialog.component';
import { SelectionModel } from '@angular/cdk/collections';

declare var $: any;

@Component({
  selector: 'app-link-dialog',
  templateUrl: './link-dialog.component.html',
  styleUrls: ['./link-dialog.component.scss']
})
export class LinkDialog implements OnInit {

  @ViewChild('linksTable', { read: MatSort }) LinksTableSort: MatSort;
  @ViewChild('tablePaginator') LinksTablePaginator: MatPaginator;
  @ViewChildren('tableRowLinkInputs') tableRowLinkInputs: QueryList<any>;


  /////////////////////////////////////////////////////////////////////////
  // Variables
  /////////////////////////////////////////////////////////////////////////

  //-------------------------------------------------------------------
  // Data Subscriptions
  //-------------------------------------------------------------------
  private subscriptions: any = {};
  public links: Array<Link> = [];

  //-------------------------------------------------------------------
  // Filter Input Binders
  //-------------------------------------------------------------------
  public filterValue: string = '';

  //-------------------------------------------------------------------
  // Form Variables Link
  //-------------------------------------------------------------------
  public createForm: FormGroup;
  public linkTableRowForms: FormArray = new FormArray([]);
  public linkDisplayFormControlValue: string = '';

  //-------------------------------------------------------------------
  // Form Variables Link Table
  //-------------------------------------------------------------------
  public LinksTableForm: FormGroup;
  public LinksTableRowForms: FormArray = new FormArray([]);
  public LinksTableRowEditState: Array<boolean> = [];

  //-------------------------------------------------------------------
  // Material Component Variable
  //-------------------------------------------------------------------
  public LinksTableDisplayedColumns: string[] = [
    'Select',
    'Title',
    'Tagline',
    // 'Disabled',
    'Href',
    'Button',
    'Actions',
  ];
  public LinksTableDataSource = new MatTableDataSource([]);
  public selection = new SelectionModel<Link>(true, []);


  //-------------------------------------------------------------------
  // Dialog Components
  //-------------------------------------------------------------------
  private htmlEditorDialogRef;

  //-------------------------------------------------------------------
  // Froala Editor Options
  //-------------------------------------------------------------------
  public inlineEditorOptions: Object = {
    key: environment.froala.key,
    toolbarInline: true,
    toolbarButtons: ['bold', 'italic', 'underline', 'superscript', 'subscript', 'undo', 'redo'],
    showNextToCursor: true,
    pluginsEnabled: [],
    multiLine: false,
    charCounterCount: false, 
    placeholderText: '...',
    pastePlain: true,
    pasteDeniedTags: ['table', 'th', 'tr', 'td'],
    enter: $.FroalaEditor.ENTER_BR,
    theme: 'dark',
  };


  constructor(
    public dialogRef: MatDialogRef<LinkDialog>,
    private logger: LoggerService,
    private linkService: LinkService,
    private formBuilder: FormBuilder,
    private zone: NgZone,
    public dialog: MatDialog,
  ) {
      //-------------------------------------------------------------------
      // Configure Logger
      //-------------------------------------------------------------------

      this.logger.config("LinkComponent",
        {
            label: 'background: #03a9f4; color: #333',
            text: ''
        }
      );
   }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
      this.logger.log("LinkComponent", text, ...args);
    }   

  /////////////////////////////////////////////////////////////////////////
  // Component Life-Cycle Methods
  /////////////////////////////////////////////////////////////////////////

  ngOnInit() {
    //-------------------------------------------------------------------
    // Subscribe to Subscriptions
    //-------------------------------------------------------------------

    //-------------------------------------------------------------------
    // Subscribe to Links
    //-------------------------------------------------------------------

    this.subscriptions.links = this.linkService.links$.subscribe(response => {
      this.zone.run(() => {
        this.links = [...response];
        this.setLinksTable([...response]);
        this.setLinksTableRowForms([...response]);
      });
    });

    //-------------------------------------------------------------------
    // Set Form Variables Link Table
    //-------------------------------------------------------------------
    this.LinksTableRowForms = this.formBuilder.array([]);
    this.LinksTableForm = this.formBuilder.group({ 'LinksTableRowForms': this.LinksTableRowForms });


    //-------------------------------------------------------------------
    // Set Form Variables Link
    //-------------------------------------------------------------------
    this.createForm = this.formBuilder.group({
      ...new Link,
    });
    this.createForm.get('Title').setValidators([Validators.required]);
    // this.log('CF', this.createForm);


  }

  ngOnDestroy(){
    this.subscriptions.links.unsubscribe();
  }

  ngAfterViewInit(){
  
    //-------------------------------------------------------------------
    // Material Components Initializers
    //-------------------------------------------------------------------

    this.zone.run(() => {
      this.setLinksTable([...this.links]);
    });

    //-------------------------------------------------------------------
    // Subscribe to children
    //-------------------------------------------------------------------
    this.tableRowLinkInputs.changes.subscribe(change => this.tableRowLinkInputs = change);

    //-------------------------------------------------------------------
    // Selected Text Filter
    //-------------------------------------------------------------------

    this.applyFilter(this.filterValue);

  }

  /////////////////////////////////////////////////////////////////////////
  // Methods
  /////////////////////////////////////////////////////////////////////////

  //-------------------------------------------------------------------
  //  Setters
  //-------------------------------------------------------------------

  setLinksTable(data: Array<Link>) {
    this.LinksTableDataSource = new MatTableDataSource(data);
    this.LinksTableDataSource.paginator = this.LinksTablePaginator;
    this.LinksTableDataSource.sort = this.LinksTableSort;
    this.LinksTableDataSource.sort.active = 'createdAt';
    this.LinksTableDataSource.sort.direction = 'desc';
  }

  setLinksTableRowForms(data: Array<Link>) {
    this.zone.runOutsideAngular(() => {
      for (let link of data) {
        let row = this.formBuilder.group({...link});
        row.get('Title').setValidators([Validators.required]);
        this.LinksTableRowForms.controls[link.id] = row;
      };
    });
  }

  setTableRowCellInputFocus(rowId, control) {
    setTimeout(() => {
        let index = this.tableRowLinkInputs.toArray().findIndex(el => el.nativeElement.id == `${rowId}-${control}`);
        if (index !== -1) { this.tableRowLinkInputs.toArray()[index].nativeElement.focus(); };
    }, 100); // DOM input initialization (ngIf)
  }

applyFilter(filterValue: string) {
    this.LinksTableDataSource.filter = filterValue.trim().toLowerCase();
}

clearFilter() {
    this.filterValue = '';
    this.LinksTableDataSource.filter = '';
}

resetLinksTableRowForm(data: Link) {
    let row = this.formBuilder.group({ ...data });
    row.get('Title').setValidators([Validators.required]);
    this.LinksTableRowForms.controls[data.id] = row;
}

resetCreateForm() {
    let reset = { ...new Link };
    this.createForm.get('Title').reset();
    this.createForm.get('Tagline').reset();
    this.createForm.get('Disabled').reset();
    this.createForm.get('Href').reset();
    this.createForm.get('Button').reset();
    this.createForm.markAsPristine();
}  

//-------------------------------------------------------------------
//  CRUD
//-------------------------------------------------------------------

createLink() {
  if (this.createForm.valid) {
    let link = this.createForm.value;
    console.log(link);
    this.linkService.createLink(link);
    this.resetCreateForm();
  } else {
    this.createForm.get('Title').markAsTouched();
  }
}

updateLink(link) {
  this.linkService.updateLink(link);
}

deleteLink(linkElement) {
  let link = { ...linkElement };
  this.linkService.deleteLink(link);
}

/////////////////////////////////////////////////////////////////////////
// Bulk
/////////////////////////////////////////////////////////////////////////
destroyLinks() {
  this.linkService.destroyLinks();
}

//-------------------------------------------------------------------
//  Dialog Events
//-------------------------------------------------------------------
openHTMLEditorDialog(control: AbstractControl, options?: Object): void {
  this.htmlEditorDialogRef = this.dialog.open(HTMLEditorDialog, {
      width: '50%',
      data: {
          control,
          options
      }
  });

  this.htmlEditorDialogRef.afterClosed().subscribe(data => {
      if (data !== control.value) { control.markAsDirty(); }
      if (data && data !== '') { control.setValue(data); }
  });
}

selectLink() {
  let link = [ ...this.selection.selected ];

  this.dialogRef.close(link);
}


onNoClick(): void {
  this.dialogRef.close();
}

close() {
  this.dialogRef.close();
}


}
