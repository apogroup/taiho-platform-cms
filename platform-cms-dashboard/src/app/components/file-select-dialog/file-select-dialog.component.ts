import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

declare var $: any;

@Component({
    selector: 'file-select-dialog',
    templateUrl: 'file-select-dialog.component.html',
    styleUrls: ['file-select-dialog.component.scss']
})
export class FileSelectDialog implements OnInit {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    constructor(
        public dialogRef: MatDialogRef<FileSelectDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit() {}

    onNoClick(): void {
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }

    selectClose(file){
        this.dialogRef.close(file);
    }

}