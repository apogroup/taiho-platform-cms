import { Component, OnInit, OnDestroy, Input, Output, ViewChild, NgZone, EventEmitter } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { GatewayS3Service } from 'app/services/gateway/gateway-s3.service';
import { S3ObjectService } from 'app/services/dashboard/s3object.service';
import { ReferenceService } from 'app/services/dashboard/reference.service';
import { DocumentService } from 'app/services/dashboard/document.service';

import { S3UploadDialog } from 'app/components/s3-upload-dialog/s3-upload-dialog.component';

import { S3Object } from 'app/models/s3object.model';
import { ReferenceVersion } from 'app/models/referenceversion.model';
import { DocumentVersion } from 'app/models/documentversion.model';

@Component({
    selector: 's3-upload',
    templateUrl: './s3-upload.component.html',
    styleUrls: ['./s3-upload.component.scss']
})
export class S3UploadComponent implements OnInit, OnDestroy {

    @Input() fileSelectDialog: boolean;
    @Output() fileSelect: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('filesTable', { read: MatSort }) FilesTableSort: MatSort;
    @ViewChild('tablePaginator') FilesTablePaginator: MatPaginator;

    @ViewChild('bucketTable', { read: MatSort }) BucketTableSort: MatSort;
    @ViewChild('bucketTablePaginator') BucketTablePaginator: MatPaginator;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Data S3
    //-------------------------------------------------------------------
    public files: any = [];
    public bucketFiles: any = [];

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';

    //-------------------------------------------------------------------
    // BULK 
    //-------------------------------------------------------------------
    public bulkCreatingS3Objects: boolean = false;

    //-------------------------------------------------------------------
    // Form Variables 
    //-------------------------------------------------------------------
    public createForm: FormGroup;
    public uploading: boolean = false;
    public uploadError: boolean = false;

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public FilesTableDisplayedColumns: string[] = [
        's3cloud',
        'associations',
        'key',
        // 'eTag',
        'lastModified',
        'size',
        'FileIndex',
        'Actions',
    ];
    public BucketTableDisplayedColumns: string[] = [
        'key',
        // 'eTag',
        'lastModified',
        'size',
        'Actions',
    ];

    public FilesTableDataSource = new MatTableDataSource([]);
    public BucketTableDataSource = new MatTableDataSource([]);

    //-------------------------------------------------------------------
    // Dialog Components
    //-------------------------------------------------------------------
    private s3UploadErrorDialogRef;

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public s3objects: Array<S3Object> = [];
    private referenceversions: Array<ReferenceVersion> = [];
    private documentversions: Array<DocumentVersion> = [];

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private gatewayS3Service: GatewayS3Service,
        private s3ObjectService: S3ObjectService,
        private referenceService: ReferenceService,
        private documentService: DocumentService,

        private formBuilder: FormBuilder,
        private zone: NgZone,

        public dialog: MatDialog,   
    ) {

        //-------------------------------------------------------------------
        // Configure Logger
        //-------------------------------------------------------------------

        this.logger.config("S3UploadComponent",
            {
                label: 'background: #03a9f4; color: #333',
                text: ''
            }
        );

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////

    log(text, args?) {
        this.logger.log("S3UploadComponent", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Component Life-Cycle Methods
    /////////////////////////////////////////////////////////////////////////

    ngOnInit() {

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------
        this.applyFilter(this.filterValue);

        //-------------------------------------------------------------------
        // Add Select Column to Table if fileSelectDialog Input
        //-------------------------------------------------------------------
        if (this.fileSelectDialog) {
            this.FilesTableDisplayedColumns = [
                'Select',
                'key',
                // 'eTag',
                'lastModified',
                'size',
                'Actions',
            ];
        }

        //-------------------------------------------------------------------
        // Subscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.s3objects = this.s3ObjectService.s3objects$.subscribe(response => {
            this.zone.run(() => {

                this.s3objects = [...response];
                
                this.log('S3Objects[]', this.s3objects);

                this.getS3BucketFiles().subscribe(res => {

                    let fileObj = {
                        bucketFiles: res
                    };

                    this.setFilesTable([...this.s3objects]);
                    this.setBucketFiles(fileObj);
                    this.mapHasAssociationsToS3Objects(); // Recalls setFilesTable
                });

            });
        });

        this.subscriptions.referenceversions = this.referenceService.referenceversions$.subscribe(response => {
            this.zone.run(() => {
                this.referenceversions = [...response];
                this.mapHasAssociationsToS3Objects();
            });
        });

        this.subscriptions.documentversions = this.documentService.documentversions$.subscribe(response => {
            this.zone.run(() => {
                this.documentversions = [...response];
                this.mapHasAssociationsToS3Objects();
            });
        });

        //-------------------------------------------------------------------
        // Set Form Variables Document
        //-------------------------------------------------------------------
        this.createForm = this.formBuilder.group({
            'Directory': '',
            'File': [[]],
        });
        this.createForm.get('Directory').setValidators([Validators.required]);
        this.createForm.get('File').setValidators([Validators.required]);

    }

    ngOnDestroy() {

        //-------------------------------------------------------------------
        // Unsubscribe to Subscriptions
        //-------------------------------------------------------------------

        this.subscriptions.s3objects.unsubscribe();

    }

    ngAfterViewInit() { 

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        this.applyFilter(this.filterValue);

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////


    setBucketFiles(files){
        
        this.zone.run(() => {

            const { bucketFiles } = files;

            this.files = bucketFiles || this.files;

            console.log("Files[]", this.files);

            this.bucketFiles = [...this.files].filter(file => { return ![...this.s3objects].some(s3object => { return file.key == s3object.key }) });
            console.log("Bucket Files[]", this.bucketFiles);

            this.setBucketFilesTable([...this.bucketFiles]);            
                                                                        
        });

    }

    //-----------------------------------------------------------------------
    // API Gateway Methods
    //-----------------------------------------------------------------------

    getS3BucketFiles() {
        
        let subject = new Subject();

        this.gatewayS3Service.list$().subscribe(res => {
            this.zone.run(() => {

                this.log('Files[]', res);
                
                res.error ? subject.error(res) : subject.next(res);
                subject.complete();

            });

        }, error => {

            this.log('ERROR: (getS3BucketFiles list$.subscribe', error);

        });

        return subject.asObservable();

    }

    getS3BucketFile(file) {

        this.gatewayS3Service.get$(file.key).subscribe(res => {            
            this.zone.run(() => {

                if(res.error){
                    this.openS3UploadErrorDialog(res.error);
                } else {
                    let url: any = res;
                    this.log('url', url);
    
                    window.open(url);
                }

            }, error => {

                this.log('ERROR: (getS3BucketFile get$.subscribe', error);

            });
        });

    }

    putS3BucketFile(directory, file) {

        this.uploading = true;
        let key = `${directory}/${file.name}`;

        let fd = new FormData();
        fd.append('fileToUpload', file);

        this.gatewayS3Service.put$(key, fd).subscribe(res => {

            this.zone.run(() => {

                this.log('File Uploaded[]', key);
                
                //-----------------------------------------------------------------------
                // No longer uploading
                //-----------------------------------------------------------------------
                this.uploading = false;
                
                //-----------------------------------------------------------------------
                // Reset the form?
                //-----------------------------------------------------------------------
                res.key ? this.resetCreateForm() : this.uploadError = true;

                if(res.error) 
                    this.openS3UploadErrorDialog(res.error);

            }, error => {
                
                this.uploadError = true;
                
                this.log('ERROR: (putFile put$.subscribe)', error);
                                
            });
        });
    }

    removeS3BucketFile(file) {

        this.gatewayS3Service.remove$(file.key).subscribe(res => {

            this.zone.run(() => {

                this.log('File Removed[]', res);

                let files = res.data;

                for(let file of files){

                    this.s3ObjectService.handleS3ObjectDeletion(file);

                }

                if(res.error) 
                    this.openS3UploadErrorDialog(res.error);

            });

        }, error => {

            this.log('ERROR: (removeFile remove$.subscribe)', error);

        });
        
    }

    //-----------------------------------------------------------------------
    // Upload
    //-----------------------------------------------------------------------

    upload(directory, file) {
        if (this.createForm.valid) {
            this.log('UPLOAD', [directory, file]);
            this.putS3BucketFile(directory, file[0].file);
        } else {
            this.createForm.get('Directory').markAsTouched();
            this.createForm.get('File').markAsTouched();
        }
    }

    //-------------------------------------------------------------------
    //  Helpers
    //-------------------------------------------------------------------

    createS3ObjectFromBucketFile(file: any) {
        this.s3ObjectService.createS3Object(file).subscribe((res) => {

            let s3Object: any = res;

            this.bucketFiles = [...this.bucketFiles].filter(bucketFile  => bucketFile.key !== s3Object.key);
    
            this.setBucketFilesTable([...this.bucketFiles]);
        });
    }

    createS3ObjectsFromBucketFiles(files: any) {

        this.bulkCreatingS3Objects = true;

        this.s3ObjectService.createS3Objects(files).subscribe((res) => {

            let s3Objects: any = res;

            this.bulkCreatingS3Objects = false;

        });

    }

    //-----------------------------------------------------------------------
    // File Validators & Helpers
    //-----------------------------------------------------------------------

    validateFileExistsInS3(s3object: S3Object){
        return this.files.some(file => file.key == s3object.key);
    }

    async mapHasAssociationsToS3Objects() {
        let s3objects = [...this.s3objects];

        const s3objectsMapped = await s3objects.map((s3object: any) => {
            s3object.hasAssociations = this.associatedToContent(s3object);
            return s3object;
        });

        this.setFilesTable(s3objectsMapped);
    }

    associatedToContent(s3object: S3Object) {
        const documentVersionsMatch = [...this.documentversions].filter((item: any) => {
            if (!s3object.key) return false;

            let fp  = item.FilePath && item.FilePath.key && item.FilePath.key === s3object.key;
            let sfp = item.SourceFilePath && item.SourceFilePath.key && item.SourceFilePath.key === s3object.key;
            let tp  = item.ThumbnailPath && item.ThumbnailPath.key && item.ThumbnailPath.key === s3object.key;

            return (fp || sfp || tp);
        });

        const referenceVersionsMatch = [...this.referenceversions].filter((item: any) => {
            if (!s3object.key) return false;

            let fp  = item.FilePath && item.FilePath.key && item.FilePath.key === s3object.key;
            let afp = item.AnnotatedFilePath && item.AnnotatedFilePath.key && item.AnnotatedFilePath.key === s3object.key;
            let sfp = item.SourceFilePath && item.SourceFilePath.key && item.SourceFilePath.key === s3object.key;
            let tp  = item.ThumbnailPath && item.ThumbnailPath.key && item.ThumbnailPath.key === s3object.key;

            return (fp || afp || sfp || tp);
        });

        return (documentVersionsMatch.length >= 1 || referenceVersionsMatch.length >= 1 || (s3object.Contents && s3object.Contents.length >= 1))
    }
    
    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------

    setFilesTable(data: Array<any>) {
        this.FilesTableDataSource = new MatTableDataSource([...data]);
        this.FilesTableDataSource.paginator = this.FilesTablePaginator;
        this.FilesTableDataSource.sort = this.FilesTableSort;
        // this.FilesTableDataSource.sort.active = 'lastModified';
        // this.FilesTableDataSource.sort.direction = 'desc';
    }
    
    setBucketFilesTable(data: Array<any>) {
        this.BucketTableDataSource = new MatTableDataSource([...data]);
        this.BucketTableDataSource.paginator = this.BucketTablePaginator;
        this.BucketTableDataSource.sort = this.BucketTableSort;
        // this.BucketTableDataSource.sort.active = 'lastModified';
        // this.BucketTableDataSource.sort.direction = 'desc';
    }

    applyFilter(filterValue: string) {
        this.FilesTableDataSource.filter = filterValue.trim().toLowerCase();
        this.BucketTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.FilesTableDataSource.filter = '';
        this.BucketTableDataSource.filter = '';
    }

    resetCreateForm() {
        this.uploading = false;
        this.uploadError = false;

        this.createForm.get('Directory').reset();
        this.createForm.get('File').reset();
        this.createForm.get('Directory').setValue('');
        this.createForm.get('File').setValue([]);
        this.createForm.markAsPristine();
    }

    //-----------------------------------------------------------------------
    // File Actions
    //-----------------------------------------------------------------------

    openFileInTab(file) {
        this.getS3BucketFile(file);
    }

    deleteFile(file) {
        this.removeS3BucketFile(file);
    }

    getFileIndex(file) {
        this.s3ObjectService.getS3ObjectIndex(file.key).subscribe(res => {
            file.indexing = false;
        });
    }


    //-----------------------------------------------------------------------
    // File Select Dialog
    //-----------------------------------------------------------------------

    select(file) {
        this.fileSelect.emit(file);
    }

    //-----------------------------------------------------------------------
    // S3 Upload Error Dialog
    //-----------------------------------------------------------------------

    openS3UploadErrorDialog(message: any): void {
        this.s3UploadErrorDialogRef = this.dialog.open(S3UploadDialog, {
            width: '50%',
            data: {
                message
            }
        });

        this.s3UploadErrorDialogRef.afterClosed().subscribe(data => {

        });
    }

}