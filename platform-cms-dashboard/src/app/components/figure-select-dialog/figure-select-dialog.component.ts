import { Component, OnInit, Inject, NgZone, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSort, MatTableDataSource } from '@angular/material';

import { S3ObjectService } from 'app/services/dashboard/s3object.service';
import { S3Object } from 'app/models/s3object.model';

@Component({
    selector: 'figure-select-dialog',
    templateUrl: 'figure-select-dialog.component.html',
    styleUrls: ['figure-select-dialog.component.scss']
})
export class FigureSelectDialog implements OnInit {

    @ViewChild('figuresTable', { read: MatSort }) FiguresTableSort: MatSort;

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    // Filter Input Binders
    //-------------------------------------------------------------------
    public filterValue: string = '';

    //-------------------------------------------------------------------
    // Data Subscriptions
    //-------------------------------------------------------------------
    private subscriptions: any = {};
    public figures: Array<S3Object> = [];

    //-------------------------------------------------------------------
    // Material Component Variable
    //-------------------------------------------------------------------
    public FiguresTableDisplayedColumns: string[] = [
        'Select',
        'Key',
    ];
    public FiguresTableDataSource = new MatTableDataSource([]);

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        public dialogRef: MatDialogRef<FigureSelectDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private s3ObjectService: S3ObjectService,
        private zone: NgZone
    ) { 
        // dialogRef.disableClose = true;
    }

    ngOnInit() {

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        if (this.data && this.data.selectedText) {
            this.filterValue = this.data.selectedText;
            this.applyFilter(this.filterValue);
        }

        //-------------------------------------------------------------------
        // Subscribe to All Figures
        //-------------------------------------------------------------------

        this.subscriptions.figures = this.s3ObjectService.s3objects$.subscribe(response => {
            this.zone.run(() => {
              this.figures = response.filter(({key}) => key.includes('images/'));
            });
        });

    }

    ngAfterViewInit() {

        //-------------------------------------------------------------------
        // Material Components Initializers
        //-------------------------------------------------------------------

        this.zone.run(() => {
            this.setFiguresTable([...this.figures]);
        });

        //-------------------------------------------------------------------
        // Selected Text Filter
        //-------------------------------------------------------------------

        this.applyFilter(this.filterValue);

    }

    /////////////////////////////////////////////////////////////////////////
    // Methods
    /////////////////////////////////////////////////////////////////////////

    //-------------------------------------------------------------------
    //  Setters
    //-------------------------------------------------------------------
    setFiguresTable(data: Array<S3Object>) {
        this.FiguresTableDataSource = new MatTableDataSource(data);
        this.FiguresTableDataSource.sort = this.FiguresTableSort;
        this.FiguresTableDataSource.sort.active = 'createdAt';
        this.FiguresTableDataSource.sort.direction = 'desc';
        this.FiguresTableDataSource.filterPredicate = (data: any, filter: string) => {
            let ref = [
                data.UniqueID,
                data.ShortCitation,
                data.Citation,
                data.DisplayName,
                data.title,
            ];
            let dataString = JSON.stringify(ref);
            let filterSubstrings = filter.split(" ");
            return filterSubstrings.some(textBlock => dataString.toLowerCase().includes(textBlock.toLowerCase()))
        }
    }

    applyFilter(filterValue: string) {
        this.FiguresTableDataSource.filter = filterValue.trim().toLowerCase();
    }

    clearFilter() {
        this.filterValue = '';
        this.FiguresTableDataSource.filter = '';
    }
    
    selectFigure(figure) {
        this.dialogRef.close(figure);
    }

    //-------------------------------------------------------------------
    //  Dialog
    //-------------------------------------------------------------------

    // onNoClick(): void {
    //     this.dialogRef.close();
    // }

    close() {
        this.dialogRef.close();
    }

}
