import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigurationComponent } from 'app/components/dashboard/boards/configuration/configuration.component';
import { ContentComponent } from 'app/components/dashboard/boards/content/content.component';
import { ReferencesComponent } from 'app/components/dashboard/boards/references/references.component';
import { DocumentsComponent } from 'app/components/dashboard/boards/documents/documents.component';
import { TagsComponent } from 'app/components/dashboard/boards/tags/tags.component';
import { LexiconComponent } from 'app/components/dashboard/boards/lexicon/lexicon.component';
import { GlossaryComponent } from 'app/components/dashboard/boards/glossary/glossary.component';
import { AbbreviationComponent } from 'app/components/dashboard/boards/abbreviation/abbreviation.component';
import { FilesComponent } from 'app/components/dashboard/boards/files/files.component';
import { UserManagementComponent } from 'app/components/dashboard/boards/user-management/user-management.component';
import { CongressesComponent } from 'app/components/dashboard/boards/congresses/congresses.component';

const routes: Routes = [
  { path: '', redirectTo: '/content', pathMatch: 'full' },
  {
    path: 'content',
    component: ContentComponent,
    data: { title: 'Content', icon: 'create', tabSelectedIndex: 0 }
  },
  {
    path: 'references',
    component: ReferencesComponent,
    data: { title: 'References', icon: 'comment', tabSelectedIndex: 1 }
  },
  {
    path: 'documents',
    component: DocumentsComponent,
    data: { title: 'Documents', icon: 'description', tabSelectedIndex: 2 }
  },
  {
    path: 'tags',
    component: TagsComponent,
    data: { title: 'Tags', icon: 'label', tabSelectedIndex: 3 }
  },
  {
    path: 'lexicon',
    component: LexiconComponent,
    data: { title: 'Lexicon', icon: 'book', tabSelectedIndex: 4 }
  },
  {
    path: 'glossary',
    component: GlossaryComponent,
    data: { title: 'Glossary', icon: 'book', tabSelectedIndex: 5 }
  },
  {
    path: 'abbreviations',
    component: AbbreviationComponent,
    data: { title: 'Abbreviations', icon: 'book', tabSelectedIndex: 6 }
  },
  {
    path: 'congresses',
    component: CongressesComponent,
    data: { title: 'Congresses', icon: 'event', tabSelectedIndex: 7 }
  },
  {
    path: 'files',
    component: FilesComponent,
    data: { title: 'Files', icon: 'attach_file', tabSelectedIndex: 8 }
  },
  {
    path: 'config',
    component: ConfigurationComponent,
    data: { title: 'Configuration', icon: 'settings_applications', tabSelectedIndex: 9 }
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
        useHash: true,
        enableTracing: false, // debug
      },
    )
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }