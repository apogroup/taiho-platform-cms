import { Injectable } from '@angular/core'

declare interface LoggerStyle {
    label: string,
    text: string,
}

@Injectable()
export class LoggerService {
    configs: any = {};

    constructor() { }

    config(name: string, style: LoggerStyle) {
        this.configs[name] = {
            label: style.label,
            text: style.text
        }
    }
    
    log(name: string, text?: any, obj1?, obj2?, obj3?, obj4?, obj5?) {
        let defaultLabelConfig = 'background: #666; color: #ccc; font-size: 14px; font-weight: bold;';
        let defaultTextConfig = '';
        let config = this.configs[name];

        if (config) {
            defaultLabelConfig += config.label;
            defaultTextConfig += config.text;
        }
        
        let str = '%c ' + name + ' %c ' + text;
        if (obj5) {
            console.log(str, defaultLabelConfig, defaultTextConfig, obj1, obj2, obj3, obj4, obj5);
        } else if (obj4) {
            console.log(str, defaultLabelConfig, defaultTextConfig, obj1, obj2, obj3, obj4);
        } else if (obj3) {
            console.log(str, defaultLabelConfig, defaultTextConfig, obj1, obj2, obj3);
        } else if (obj2) {
            console.log(str, defaultLabelConfig, defaultTextConfig, obj1, obj2);
        } else if (obj1) {
            console.log(str, defaultLabelConfig, defaultTextConfig, obj1);
        } else {
            console.log(str, defaultLabelConfig, defaultTextConfig);
        }
    }

}