import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { v4 as uuid } from 'uuid';

import { environment } from 'environments/environment';
import { LoggerService } from 'app/services/logger/logger.service';

/////////////////////////////////////////////////////////////////////////
// Define Sails IO Client
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// WebsocketSerivce
/////////////////////////////////////////////////////////////////////////
// Service for Sails socket client (sails.io.js) that establishes 
// a socket connection and wraps the client's methods in obersvables
//---------------------------------------------------------------------//

@Injectable()
export class GatewayS3Service {
    
    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private http: HttpClient,
        private zone: NgZone,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("GatewayS3Service",
            {
                label: 'background: #e4e5e6; color: #333',
                text: ''
            }
        );
        
    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("GatewayS3Service", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Methods 
    /////////////////////////////////////////////////////////////////////////

    list$(): Observable<any> {

        let subject = new Subject();

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            this.http.get(environment.api.url+'/content/files').subscribe(response => {
                if(response){
                    this.log('LIST', response);
                    subject.next(response);
                    subject.complete();
                }
            }, error => {
                this.log('error from LIST', error);
                subject.next(error);
                subject.complete();
            });

        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

    get$(key): any {

        let subject = new Subject();
        
        let encodedKey = key.split('/').map(encodeURIComponent).join('/');

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            this.http.get(environment.api.url+'/content/'+encodedKey).subscribe(response => {
                if(response){
                    this.log('GET', response);
                    subject.next(response);
                    subject.complete();
                }
            }, error => {
                this.log('error from GET', error);
                subject.next(error);
                subject.complete();
            });

        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

    put$(key, file): Observable<any> {

        let subject = new Subject();

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            key = `${key}#${uuid()}`;

            let encodedKey = key.split('/').map(encodeURIComponent).join('/');

            this.http.put(environment.api.url+'/content/'+encodedKey, file).subscribe(response => {
                if(response){
                    this.log('PUT', response);
                    subject.next(response);
                    subject.complete();
                }
            }, error => {
                this.log('error from PUT', error);
                subject.next(error);
                subject.complete();
            });

        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

    remove$(key): Observable<any> {

        let subject = new Subject();

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            let encodedKey = key.split('/').map(encodeURIComponent).join('/');

            this.http.delete(environment.api.url+'/content/'+encodedKey).subscribe(response => {
                if(response){
                    this.log('DELETE', response);
                    subject.next(response);
                    subject.complete();
                }
            }, error => {
                this.log('error from DELETE', error);
                subject.next(error);
                subject.complete();
            });

        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

}