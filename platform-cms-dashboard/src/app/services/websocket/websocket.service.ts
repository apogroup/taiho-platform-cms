import { Injectable, NgZone } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import * as socketIOClient from 'socket.io-client';
import * as sailsIOClient from 'sails.io.js';

// import { SailsClient } from 'ngx-sails';
// import { ISailsRequestOpts } from 'ngx-sails/sails-client';

import { environment } from 'environments/environment';
import { LoggerService } from 'app/services/logger/logger.service';

import { ConnectionStatus } from 'app/models/connection.status.model';

/////////////////////////////////////////////////////////////////////////
// Define Sails IO Client
/////////////////////////////////////////////////////////////////////////

let io: sailsIOClient = sailsIOClient(socketIOClient);

/////////////////////////////////////////////////////////////////////////
// WebsocketSerivce
/////////////////////////////////////////////////////////////////////////
// Service for Sails socket client (sails.io.js) that establishes 
// a socket connection and wraps the client's methods in obersvables
//---------------------------------------------------------------------//

@Injectable()
export class WebsocketService {

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    private SailsSocketIOClient = io;

    public connection$: BehaviorSubject<ConnectionStatus>;

    private pubSubs: Array<any> = [];

    constructor(
        private logger: LoggerService,
        private zone: NgZone,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("WebSocketService",
            { 
                label: 'background: #a39efe; color: #333', 
                text: '' 
            }
        );

        /////////////////////////////////////////////////////////////////////
        // SAILS.IO.JS Sockets
        /////////////////////////////////////////////////////////////////////

        //-------------------------------------------------------------------
        // Listen to Connection Events
        //-------------------------------------------------------------------

        this.listenToConnection();

        //-------------------------------------------------------------------
        // Establish Socket Connection
        //-------------------------------------------------------------------
        this.SailsSocketIOClient.sails.url = environment.api.url;
        
        //-------------------------------------------------------------------
        // Establish Default Eager Socket Connection
        //-------------------------------------------------------------------

        // io.sails.url = environment.api.url;

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------

        this.connection$ = new BehaviorSubject<ConnectionStatus>({
            connected: false,
            status: 'Connecting'
        });

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("WebSocketService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Socket Event Listeners
    /////////////////////////////////////////////////////////////////////////
    // sailsjs.com
    // /documentation/reference/web-sockets/socket-client/io-socket-on
    //---------------------------------------------------------------------//

    listenToConnection() {

        this.log("Listening for connection...");

        //-------------------------------------------------------------------
        // Explicitly listen for socket connect
        //-------------------------------------------------------------------

        this.SailsSocketIOClient.socket.on('connect', (msg) => {
            this.connection$.next({
                connected: true,
                status: 'Connected'
            });
            this.log("SailsSocketIOClient Connected", msg);
        });

        this.SailsSocketIOClient.socket.on('connect_error', (msg) => {
            this.connection$.next({
                connected: false,
                status: 'Connect Error'
            });
            // this.log("SailsSocketIOClient Connection Errored", msg);
            this.log("SailsSocketIOClient Connection Errored");
        });

        this.SailsSocketIOClient.socket.on('disconnect', (msg) => {

            //-------------------------------------------------------------------
            // Try to reconnect forever
            //-------------------------------------------------------------------

            this.SailsSocketIOClient.socket._raw.io._reconnection = true;
            this.SailsSocketIOClient.socket._raw.io._reconnectionAttempts = Infinity;

            this.connection$.next({
                connected: false,
                status: 'Disconnected'
            });
            this.log("SailsSocketIOClient Disconnected", msg);
        });
        
        // this.SailsSocketIOClient.socket.on('reconnect', (msg) => {
        //     this.connection$.next({
        //         connected: true,
        //         status: 'Reconnected'
        //     });
        //     this.log("SailsSocketIOClient Reconnected", msg);
        // });

        this.SailsSocketIOClient.socket.on('reconnect_failed', (msg) => {
            this.connection$.next({
                connected: false,
                status: 'Reconnect Failed'
            });
            this.log("SailsSocketIOClient Reconnection Failed", msg);
        });

        return;

    }

    /////////////////////////////////////////////////////////////////////////
    // Sails.js Socket Method Pass-throughs
    /////////////////////////////////////////////////////////////////////////

    //---------------------------------------------------------------------//
    // Universal Socket Method Observable
    //---------------------------------------------------------------------//

    socket$(method, url, data?: any): Observable<any> {

        let subject = new Subject();

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            this.SailsSocketIOClient.socket[method](url, data, (resData, jwres) => {

                if (jwres.statusCode < 200 || jwres.statusCode >= 400) {
                    subject.next({
                        data: resData,
                        statusCode: jwres.statusCode,
                        response: jwres,
                        error: jwres.error
                    });
                } else {
                    subject.next({
                        data: resData,
                        statusCode: jwres.statusCode,
                        response: jwres
                    });
                }

                subject.complete();

            });

        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }
    
    //---------------------------------------------------------------------//
    // Return method observables via socket$ for io.socket.*()
    //---------------------------------------------------------------------//

    get$(url, data?: any): Observable<any> { 
        
        return this.socket$('get', url, data);
    
    }

    post$(url, data?: any): Observable<any> {

        return this.socket$('post', url, data);

    }

    put$(url, data?: any): Observable<any> {

        return this.socket$('put', url, data);

    }

    patch$(url, data?: any): Observable<any> {

        return this.socket$('patch', url, data);

    }

    delete$(url, data?: any): Observable<any> {

        return this.socket$('delete', url, data);

    }

    //---------------------------------------------------------------------//
    // PubSub io.socket.on()
    //---------------------------------------------------------------------//

    on$(eventName: string): Observable<any> {

        if (!this.pubSubs[eventName] || this.pubSubs[eventName].isComplete) {

            this.pubSubs[eventName] = new Subject();

            //=== START ZONE ==============================================//
            this.zone.runOutsideAngular(() => {

                this.SailsSocketIOClient.socket.on(eventName, msg => {

                    this.log("on", [eventName, msg]);

                    this.pubSubs[eventName].next(msg)

                });

            });
            //=== END ZONE ===============================================//

        }

        return this.pubSubs[eventName].asObservable();

    }


}