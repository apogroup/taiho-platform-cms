import { Injectable } from '@angular/core'
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { S3Object } from 'app/models/s3object.model';

/////////////////////////////////////////////////////////////////////////
// S3Object Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class S3ObjectService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private s3objects: Array<any> = [];
    public s3objects$: BehaviorSubject<Array<any>>;

    private s3object: S3Object;
    public s3object$: BehaviorSubject<any>;

    private user: any = {};

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService,
        private userService: UserService,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("S3ObjectService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------
        this.s3objects$ = new BehaviorSubject<Array<any>>([]);

        //-------------------------------------------------------------------
        // Subscribe to user
        //-------------------------------------------------------------------
        this.userService.user$.subscribe(user => {
            this.user = user;
        });
    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("S3ObjectService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToS3Objects() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`s3object`).subscribe(res => {

            this.log('S3Objects[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.s3objects = res.data;
                this.s3objects$.next(this.s3objects);
            }
            if (res.verb === 'created') {
                let index = this.s3objects.findIndex(item => item.id == res.id);
                if(index == -1){ 
                    this.s3objects.unshift(res.data); 
                    this.s3objects$.next(this.s3objects);
                }
            }
            if (res.verb === 'updated') {
                let index = this.s3objects.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.s3objects[index] = res.data;
                    this.s3objects[index].id = res.id;
                    this.s3objects$.next(this.s3objects);
                }
            }
            if (res.verb === 'destroyed') {
                this.s3objects = this.s3objects.filter(item => item.id != res.id);
                this.s3objects$.next(this.s3objects);
            }

        }, error => {

            this.log('ERROR: (subscribetoS3Objects)', error);

        });

    }

    /////////////////////////////////////////////////////////////////////////
    // S3Object Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create an S3Object 
    //-----------------------------------------------------------------------

    createS3Object(file: any) {

        let subject = new Subject();

        //-----------------------------------------------------------------------
        // Validate the S3Object does not already exist, then create
        //-----------------------------------------------------------------------
        let key = encodeURIComponent(file.key);
        
        //-----------------------------------------------------------------------
        // Prepare S3Object Body
        //-----------------------------------------------------------------------

        let s3Object = { ...new S3Object, ...file }

        //-----------------------------------------------------------------------
        // Push S3Object to model
        //-----------------------------------------------------------------------

        this.model.create$('s3object', { ...s3Object }).subscribe((res: any) => {

            this.log('S3Object Created', res);

            //------------------------------------- --------------------------
            // Publish S3Object to S3Objects
            //---------------------------------------------------------------
            
            this.s3objects.push(res.data);
            this.s3objects$.next(this.s3objects);
            
            
            //---------------------------------------------------------------
            // Satisfy S3Object Create
            //---------------------------------------------------------------

            subject.next(res.data);
            subject.complete();

        }, error => {

            this.log('ERROR: (createS3Object)', error);

        });

        return subject.asObservable();
        
    }
    
    //-----------------------------------------------------------------------
    // Create S3Objects -- BULK
    //-----------------------------------------------------------------------
   
    createS3Objects(files: any) {

        let subject = new Subject();
        
        files.forEach((file, index) => {
            
            //-----------------------------------------------------------------------
            // Validate the S3Object does not already exist, then create
            //-----------------------------------------------------------------------

            let key = encodeURIComponent(file.key);
            
            //-----------------------------------------------------------------------
            // Prepare S3Object Body
            //-----------------------------------------------------------------------

            let s3Object = { ...new S3Object, ...file }
            
            //-----------------------------------------------------------------------
            // Push S3Object to model
            //-----------------------------------------------------------------------

            this.model.create$('s3object', { ...s3Object }).subscribe((res: any) => {
    
                this.log('S3Object Created', res);
    
                this.s3objects.push(res.data);
                
                if(index === files.length - 1){

                    //---------------------------------------------------------------
                    // Publish S3Objects to S3Objects
                    //---------------------------------------------------------------
            
                    this.s3objects$.next(this.s3objects);
                                    
                    //---------------------------------------------------------------
                    // Satisfy S3Objects Create
                    //---------------------------------------------------------------
            
                    subject.next(this.s3objects);
                    subject.complete();
                }

            }, error => {
                
                this.log('ERROR: (createS3Object)', error);
                
            });
            
        });
            
        return subject.asObservable();
        
    }
    

    //-----------------------------------------------------------------------
    // Get an S3Object 
    //-----------------------------------------------------------------------

    // getS3Object(file: any) {
    //     this.model.get$(`s3object?where={"key": "${file.key}"}`).subscribe((res: any) => {

    //         if (res.statusCode === 200 && res.data.length >= 1) {

                
    //         } 

    //     })        
    // }

    //-----------------------------------------------------------------------
    // Retrieve FileIndex from Server
    //-----------------------------------------------------------------------

    getS3ObjectIndex(key: any) {

        let subject = new Subject();

        this.model.get$('s3object/generate-index', { key }).subscribe((res: any) => {

            //---------------------------------------------------------------
            // Publish S3Object to S3Objects
            //---------------------------------------------------------------
            // this.s3objects[this.s3objects.findIndex(item => item.id === res.data.id)];

            let index = this.s3objects.findIndex(item => item.id == res.data.id);
            if (index !== -1) {
                this.s3objects[index] = res.data;
            }
            this.s3objects$.next(this.s3objects);

            //---------------------------------------------------------------
            // Satisfy S3Object Update
            //---------------------------------------------------------------
            subject.next(res.data);
            subject.complete();

        }, error => {

            this.log('ERROR: (getS3ObjectIndex)', error);

        });

        return subject.asObservable();
    }

    //-----------------------------------------------------------------------
    // Delete an S3Object 
    //-----------------------------------------------------------------------

    // deleteS3Object(file: any) {

    //     let s3Object = file;

    //     this.model.get$(`s3object/${s3Object.id}`).subscribe((res: any) => {

    //         if (res.statusCode === 200 && res.data) {

    //             // this.log('Deleting S3Object', s3Object);

    //             this.model.delete$('s3object', s3Object.id).subscribe((res: any) => {

    //                 // this.log('Deleted S3Object', s3Object);

    //                 this.s3objects = this.s3objects.filter(item => item.id != res.data.id);

    //                 this.s3objects$.next(this.s3objects);

    //             })

    //         } else {
    //             this.s3objects$.next(this.s3objects);
    //         }

    //     }, error => {

    //         this.log('ERROR: (deleteS3Object)', error);

    //     });                
    // }

    /////////////////////////////////////////////////////////////////////////
    // S3Object Helpers
    /////////////////////////////////////////////////////////////////////////

    handleS3ObjectDeletion(file: any){

        let s3Object = file;

        //---------------------------------------------------------------
        // Publish S3Object Updates to S3Objects
        //---------------------------------------------------------------

        this.s3objects = this.s3objects.filter(item => item.id != s3Object.id);
        this.s3objects$.next(this.s3objects);

    }

}