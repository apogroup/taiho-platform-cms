import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LoggerService } from '../logger/logger.service';
import { ModelService } from '../model/model.service';
import { UserService } from './user.service';
import { Link } from 'app/models/link.model';

/////////////////////////////////////////////////////////////////////////
// Link Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable({
  providedIn: 'root'
})
export class LinkService {

  /////////////////////////////////////////////////////////////////////////
  // Variables
  /////////////////////////////////////////////////////////////////////////

  private links: Array<any> = [];
  public links$: BehaviorSubject<Array<any>>;
  private user: any = {};

  /////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////

  constructor(
    private logger: LoggerService,
    private model: ModelService,
    private userService: UserService
) { 
      /////////////////////////////////////////////////////////////////////
      // Logger
      /////////////////////////////////////////////////////////////////////
      // Configure
      //-------------------------------------------------------------------

      this.logger.config("LinkService",
      {
          label: 'background: #9c27b0; color: #333',
          text: ''
      }
    );

    //-------------------------------------------------------------------
    // Define Observable Subscriptions
    //-------------------------------------------------------------------
    this.links$ = new BehaviorSubject<Array<any>>([]);

    //-------------------------------------------------------------------
    // Subscribe to user
    //-------------------------------------------------------------------
    this.userService.user$.subscribe(user => {
        this.user = user;
    });
}


    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
      this.logger.log("LinkService", text, ...args);
    }

  /////////////////////////////////////////////////////////////////////////
  // Link Management Methods
  /////////////////////////////////////////////////////////////////////////

  //-----------------------------------------------------------------------
  // Subscribe to Links (all)
  //-----------------------------------------------------------------------

  subscribeToLinks() {

      //---------------------------------------------------------------
      // Subscribe to Model
      //---------------------------------------------------------------

      this.model.pubsub$(`link`).subscribe(res => {

        this.log('Contents[]', res);

        //---------------------------------------------------------------
        // Publish according to subscription verb
        //---------------------------------------------------------------

        if (!res.verb) {
            this.links = [...res.data].sort((a, b) => a.createdAt - b.createdAt);
            this.links$.next(this.links);
        }
        if (res.verb === 'created') {
            this.links.push(res.data);
            this.links$.next(this.links);
        }
        if (res.verb === 'updated') {
            let index = this.links.findIndex(item => item.id == res.id);
            if (index !== -1) {
                this.links[index] = res.data;
                this.links[index].id = res.id;
            }
            this.links$.next(this.links);
        }
        if (res.verb === 'destroyed') {
            this.links = this.links.filter(item => item.id != res.id);
            this.links$.next(this.links);
        }

    }, error => {

        this.log('ERROR: (subscribeToLinks)', error);

    });


    }

    /////////////////////////////////////////////////////////////////////////
    // Link Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // Links
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a Link
    //-----------------------------------------------------------------------

    createLink(link: Link){

      let body = { ...link };

      delete body.id;
      delete body.createdAt;
      delete body.updatedAt;

      if (this.user && this.user.id) {
        link.CreatedBy = this.user.id;
        link.LastUpdatedBy = this.user.id;
      }

      //-----------------------------------------------------------------------
      // 1. Push link to model
      //-----------------------------------------------------------------------

      this.model.create$('link', { ...body }).subscribe((res: any) => {

        this.log('Link Created', res);

        //---------------------------------------------------------------
        // Publish link to links
        //---------------------------------------------------------------

        this.links.push(res.data);
        this.links$.next(this.links);

      }, error => {

        this.log('ERROR: (createLink)', error);
      });

    }

    //-----------------------------------------------------------------------
    // Update Link
    //-----------------------------------------------------------------------

    updateLink(link: Link) {

      //-----------------------------------------------------------------------
      // Prepare Link
      //-----------------------------------------------------------------------

      delete link.updatedAt;
      delete link.CreatedBy;
      delete link.LastUpdatedBy;

      if (this.user && this.user.id) { link.LastUpdatedBy = this.user.id; } else { link.LastUpdatedBy = null };

      //-------------------------------------------------------------------
      // Push to Model
      //-------------------------------------------------------------------

      this.model.update$('link', link.id, link).subscribe((res: any) => {
        this.log('Link Updated: ', res);
        //---------------------------------------------------------------
        // Publish link to links
        //---------------------------------------------------------------

        this.links[this.links.findIndex(item => item.id == res.data.id)] = res.data;
        this.links$.next(this.links);

      }, error => {

          this.log('ERROR: (updateLinks)', error);

      });

  }

  deleteLink(link){

    //-------------------------------------------------------------------
    // Validate Link Exist
    //-------------------------------------------------------------------

    this.model.get$(`link/${link.id}`).subscribe((res: any) => {
      if (res.statusCode === 200 && res.data) {

        //-------------------------------------------------------------------
        // Delete Link
        //-------------------------------------------------------------------
        this.log('Deleting Link', link);
        this.model.delete$('link', link.id).subscribe((res: any) => {
          this.log('Deleting Link', link);
          this.links = this.links.filter(item => item.id != res.data.id);
          this.links$.next(this.links);
        });

      }
    });


  }

    /////////////////////////////////////////////////////////////////////////
    // ALL LINKS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Destroy Links
    //-----------------------------------------------------------------------

    destroyLinks(){

      //-------------------------------------------------------------------
      // For each links delete$ congress from DB 
      //-------------------------------------------------------------------
      this.log("Destroying Links", this.links);

      for (let link of this.links) {
          //-------------------------------------------------------------------
          // Validate Link Exists
          //-------------------------------------------------------------------

          this.model.get$(`link/${link.id}`).subscribe((res: any) => {
            if (res.statusCode === 200 && res.data) {

              //-------------------------------------------------------------------
              // Delete Link
              //-------------------------------------------------------------------
              this.log('Deleting Link', link);
              this.model.delete$('link', link.id).subscribe((res: any) => {
                this.log('Deleted Link', link);
                this.links = this.links.filter(item => item.id != res.data.id);
                this.links$.next(this.links);
              });

            }
          });

      }

  }


}