import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs';
import { findIndex, get, reject, unionBy } from 'lodash';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { Setting } from 'app/models/setting.model';
import { User } from 'app/models/user.model';
/////////////////////////////////////////////////////////////////////////
// Setting Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class SettingService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private settings: Setting[] = [];
    public settings$: BehaviorSubject<Setting[]>;

    private user: User;

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService,
        private userService: UserService,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("SettingService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------

        this.settings$ = new BehaviorSubject<Setting[]>([]);

        //-------------------------------------------------------------------
        // Subscribe to user
        //-------------------------------------------------------------------
        this.userService.user$.subscribe(user => {
            this.user = user;
        });

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("SettingService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToSettings() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`setting`).subscribe(res => {

            this.log('Settings[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            switch (res.verb) {
                case undefined:
                    this.settings = get(res, 'data', []);
                    break;

                case 'created':
                    this.settings.unshift(res.data);
                    break;

                case 'updated':
                    const index = findIndex(this.settings, { id: res.id });
                    if (index > -1) {
                        this.settings[index] = {
                            ...res.data,
                            id: res.id
                        }
                    }
                    break;

                case 'destroyed':
                    this.settings = reject(this.settings, { id: res.id });
                    break;

                default: break;
            }

            this.settings$.next(this.settings);

        }, error => {

            this.log('ERROR: (subscribeToSettings)', error);

        });

    }


    /////////////////////////////////////////////////////////////////////////
    // Setting Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // TERMS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Update Setting
    //-----------------------------------------------------------------------

    updateSetting(setting: Setting) {

        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------
        const { id } = setting;

        this.model.update$('setting', id, setting).subscribe((response: any) => {

            this.log('Setting Updated: ', response);

            //---------------------------------------------------------------
            // Publish Setting to Settings
            //---------------------------------------------------------------
            const newSetting = get(response, 'data', {});
            this.settings = unionBy([newSetting], this.settings, 'id');
            this.settings$.next(this.settings);

        }, error => {

            this.log('ERROR: (updateSetting)', error);

        });

    }

}