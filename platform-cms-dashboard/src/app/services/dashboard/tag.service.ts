import { Injectable } from '@angular/core'
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { Tag } from 'app/models/tag.model';
import { Subtag } from 'app/models/subtag.model';

/////////////////////////////////////////////////////////////////////////
// Tag Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class TagService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private tags: Array<any> = [];
    public tags$: BehaviorSubject<Array<any>>;
    private subtags: Array<any> =[];
    public subtags$: BehaviorSubject<Array<any>>;

    private user: any = {};

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService,
        private userService: UserService,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("TagService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------

        this.tags$ = new BehaviorSubject<Array<any>>([]);
        this.subtags$ = new BehaviorSubject<Array<any>>([]);
        
        //-------------------------------------------------------------------
        // Subscribe to user
        //-------------------------------------------------------------------
        this.userService.user$.subscribe(user => {
            this.user = user;
        });
            
    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("TagService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToTags() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`tag`).subscribe(res => {

            this.log('Tags[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.tags = res.data;
                this.tags$.next(this.tags);
            }
            if (res.verb === 'created') {
                this.tags.unshift(res.data);
                this.tags$.next(this.tags);
            }
            if (res.verb === 'updated') {
                let index = this.tags.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.tags[index] = res.data;
                    this.tags[index].id = res.id;
                }
                this.tags$.next(this.tags);
            }
            if (res.verb === 'destroyed') {
                this.tags = this.tags.filter(item => item.id != res.id);
                this.tags$.next(this.tags);
            }

        }, error => {

            this.log('ERROR: (subscribeToTags)', error);

        });

    }

    subscribeToSubtags() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`subtag`).subscribe(res => {

            this.log('Subtags[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.subtags = res.data;
                this.subtags$.next(this.subtags);
            }
            if (res.verb === 'created') {
                this.subtags.unshift(res.data);
                this.subtags$.next(this.subtags);
            }
            if (res.verb === 'updated') {
                let index = this.subtags.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.subtags[index] = res.data;
                    this.subtags[index].id = res.id;
                }
                this.subtags$.next(this.subtags);
            }
            if (res.verb === 'destroyed') {
                this.subtags = this.subtags.filter(item => item.id != res.id);
                this.subtags$.next(this.subtags);
            }

            //---------------------------------------------------------------
            // Publish to subscription verb in TAG
            //---------------------------------------------------------------

            if (res.verb === 'created') {
                let tagIndex = this.tags.findIndex(item => item.id == res.data.Tag.id);
                if (tagIndex !== -1) {
                    this.tags[tagIndex].Subtags.unshift(res.data);
                    this.tags$.next(this.tags);
                }
            }
            if (res.verb === 'updated') {
                let tagIndex = this.tags.findIndex(item => item.id == res.data.Tag.id);
                if (tagIndex !== -1) {
                    let subtagIndex = this.tags[tagIndex].Subtags.findIndex(item => item.id == res.data.id);
                    if(subtagIndex !== -1) {
                        this.tags[tagIndex].Subtags[subtagIndex] = res.data;
                        this.tags$.next(this.tags);
                    } else {
                        this.tags[tagIndex].Subtags.unshift(res.data);
                        this.tags$.next(this.tags);
                    }
                }
            }
            if (res.verb === 'destroyed') {
                let tagIndex = this.tags.findIndex(item => item.id == res.data.Tag.id);
                if (tagIndex !== -1) {
                    let subtagIndex = this.tags[tagIndex].Subtags.findIndex(item => item.id == res.data.id);
                    if (subtagIndex !== -1) {
                        this.tags[tagIndex].Subtags.splice(subtagIndex, 1);
                        this.tags$.next(this.tags);
                    }
                }
            }

        }, error => {

            this.log('ERROR: (subscribeToSubtags)', error);

        });

    }


    /////////////////////////////////////////////////////////////////////////
    // Tag Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // TAGS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a Tag
    //-----------------------------------------------------------------------

    createTag(tag: string){

        //-----------------------------------------------------------------------
        // Validate the tag does not already exist, then create
        //-----------------------------------------------------------------------

        this.model.get$(`tag?where={"Tag": "${tag}"}`).subscribe((res: any) => {

            if (res.statusCode === 200 && res.data.length < 1) {

                //-----------------------------------------------------------------------
                // Prepare body
                //-----------------------------------------------------------------------

                let body = new Tag;
                    body.Tag = tag;

                    delete body.id;
                    delete body.createdAt;
                    delete body.updatedAt;
                    body.CreatedBy = null;
                    body.LastUpdatedBy = null;

                    if(this.user && this.user.id) {
                        body.CreatedBy = this.user.id;
                        body.LastUpdatedBy = this.user.id;
                    }

                    this.log('Creating Tag: ', body);

                //-----------------------------------------------------------------------
                // Push to model
                //-----------------------------------------------------------------------

                this.model.create$('tag', { ...body }).subscribe((res: any) => {

                    //---------------------------------------------------------------
                    // Publish Tag to Tags
                    //---------------------------------------------------------------

                    this.tags.push(res.data);
                    this.tags$.next(this.tags);

                }, error => {

                    this.log('ERROR: (createTag)', error);

                });

            }

        });

    }

    //-----------------------------------------------------------------------
    // Update Tag
    //-----------------------------------------------------------------------

    updateTag(tag: Tag) {

        //-------------------------------------------------------------------
        // Prepare Body
        //-------------------------------------------------------------------

        tag = { ...tag };
        // delete tag.createdAt;
        delete tag.updatedAt;
        delete tag.Subtags;
        delete tag.ReferenceVersions;

        delete tag.CreatedBy;
        delete tag.LastUpdatedBy;

        if (this.user && this.user.id) { tag.LastUpdatedBy = this.user.id; } else { tag.LastUpdatedBy = null };

        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------

        this.model.update$('tag', tag.id, tag).subscribe((res: any) => {
            this.log('Tag Updated: ', res);
            //---------------------------------------------------------------
            // Publish Tag to Tags
            //---------------------------------------------------------------

            this.tags[this.tags.findIndex(item => item.id == res.data.id)] = res.data;
            this.tags$.next(this.tags);

        }, error => {

            this.log('ERROR: (updateTag)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Delete Tag
    //-----------------------------------------------------------------------

    deleteTag(tag: Tag) {

            //-------------------------------------------------------------------
            // Validate Tag Exist
            //-------------------------------------------------------------------

            this.model.get$(`tag/${tag.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    //-------------------------------------------------------------------
                    // Delete Tag
                    //-------------------------------------------------------------------
                    this.log('Deleting Tag', tag);
                    this.model.delete$('tag', tag.id).subscribe((res: any) => {
                        this.log('Deleted Tag', tag);
                        this.tags = this.tags.filter(item => item.id != res.data.id);
                        this.tags$.next(this.tags);
                    });

                    //-------------------------------------------------------------------
                    // For each tag subtag, delete$ from DB
                    // Parent association broken on delete of parent
                    //-------------------------------------------------------------------
                    for (let subtag of res.data) {

                        //-------------------------------------------------------------------
                        // Validate Subtag Exist
                        //-------------------------------------------------------------------

                        this.model.get$(`subtag/${subtag.id}`).subscribe((res: any) => {
                            if (res.statusCode === 200 && res.data) {

                                //-------------------------------------------------------------------
                                // Validate Subtag
                                //-------------------------------------------------------------------
                                this.log('Deleting Subtag', subtag);
                                this.model.delete$('subtag', subtag.id).subscribe((res: any) => {
                                    this.log('Deleted Subtag', subtag);
                                    this.subtags = this.subtags.filter(item => item.id != res.data.id);
                                    this.subtags$.next(this.subtags);
                                });

                            }
                        });

                    }

                }
            });

    }

    /////////////////////////////////////////////////////////////////////////
    // SUBTAGS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a Subtag
    //-----------------------------------------------------------------------

    createSubtag(tagId: string, subtag: string) {

        //-----------------------------------------------------------------------
        // Validate the tag does not already exist, then create
        //-----------------------------------------------------------------------

        this.model.get$(`subtag?where={"Tag": "${tagId}", "Subtag": "${subtag}"}`).subscribe((res: any) => {

            if (res.statusCode === 200 && res.data.length < 1) {

                //-----------------------------------------------------------------------
                // Prepare body
                //-----------------------------------------------------------------------

                let body = new Subtag;
                body.Tag = tagId;
                body.Subtag = subtag;

                delete body.id;
                delete body.createdAt;
                delete body.updatedAt;
                body.CreatedBy = null;
                body.LastUpdatedBy = null;

                if (this.user && this.user.id) {
                    body.CreatedBy = this.user.id;
                    body.LastUpdatedBy = this.user.id;
                }

                this.log('Creating Subtag: ', body);

                //-----------------------------------------------------------------------
                // Push to model
                //-----------------------------------------------------------------------

                this.model.create$('subtag', { ...body }).subscribe((res: any) => {
                    
                    this.log('Created Subtag', res);

                    //---------------------------------------------------------------
                    // Publish Subtag to Subtags
                    //---------------------------------------------------------------

                    this.subtags.push(res.data);
                    this.subtags$.next(this.subtags);

                    //---------------------------------------------------------------
                    // Publish Subtag to Tag
                    //---------------------------------------------------------------

                    let tagIndex = this.tags.findIndex(item => res.data.Tag && item.id == res.data.Tag.id);
                    if (tagIndex !== -1) {
                        this.tags[tagIndex].Subtags.push(res.data);
                        this.tags$.next(this.tags);
                    }

                }, error => {

                    this.log('ERROR: (createSubtag)', error);

                });

            }

        });

    }

    //-----------------------------------------------------------------------
    // Update Subtag
    //-----------------------------------------------------------------------

    updateSubtag(subtag: Subtag) {

        //-------------------------------------------------------------------
        // Prepare Body
        //-------------------------------------------------------------------

        subtag = { ...subtag };
        // delete subtag.createdAt;
        delete subtag.updatedAt;
        delete subtag.Tag;
        delete subtag.ReferenceVersions;

        delete subtag.CreatedBy;
        delete subtag.LastUpdatedBy;

        if (this.user && this.user.id) { subtag.LastUpdatedBy = this.user.id; } else { subtag.LastUpdatedBy = null };
        
        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------

        this.model.update$('subtag', subtag.id, subtag).subscribe((res: any) => {
            this.log('Subtag Updated: ', res);
            //---------------------------------------------------------------
            // Publish Subtag to Subtags
            //---------------------------------------------------------------

            this.subtags[this.subtags.findIndex(item => item.id == res.data.id)] = res.data;
            this.subtags$.next(this.subtags);

            //---------------------------------------------------------------
            // Publish Subtag to Tags
            //---------------------------------------------------------------
            let tagIndex = this.tags.findIndex(item => item.id == res.data.Tag.id);
            if (tagIndex !== -1) {
                let subtagIndex = this.tags[tagIndex].Subtags.findIndex(item => item.id == res.data.id);
                if (subtagIndex !== -1) {
                    this.tags[tagIndex].Subtags[subtagIndex] = res.data;
                    this.tags$.next(this.tags);
                } else {
                    this.tags[tagIndex].Subtags.unshift(res.data);
                    this.tags$.next(this.tags);
                }
            }

        }, error => {

            this.log('ERROR: (updateSubtag)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Delete Subtag
    //-----------------------------------------------------------------------

    deleteSubtag(subtag: Subtag) {

        //-------------------------------------------------------------------
        // Validate Subtag Exist
        //-------------------------------------------------------------------

        this.model.get$(`subtag/${subtag.id}`).subscribe((res: any) => {
            if (res.statusCode === 200 && res.data) {

                //-------------------------------------------------------------------
                // Validate Subtag
                //-------------------------------------------------------------------
                this.log('Deleting Subtag', subtag);
                this.model.delete$('subtag', subtag.id).subscribe((res: any) => {
                    this.log('Deleted Subtag', subtag);
                    this.subtags = this.subtags.filter(item => item.id != res.data.id);
                    this.subtags$.next(this.subtags);

                    //---------------------------------------------------------------
                    // Publish Subtag Deletion to Tags
                    //---------------------------------------------------------------
                    let tagIndex = this.tags.findIndex(item => item.id == subtag.Tag.id);
                    if (tagIndex !== -1) {
                        let subtagIndex = this.tags[tagIndex].Subtags.findIndex(item => item.id == res.data.id);
                        if (subtagIndex !== -1) {
                            this.tags[tagIndex].Subtags.splice(subtagIndex, 1);
                            this.tags$.next(this.tags);
                        }
                    }
                });

            }
        });

    }

    /////////////////////////////////////////////////////////////////////////
    // ALL TAGS & SUBTAGS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Destroy Tags
    //-----------------------------------------------------------------------

    destroyTags() {

        //-------------------------------------------------------------------
        // For each tag delete$ tag from DB 
        //-------------------------------------------------------------------
        this.log("Destroying Tags", this.tags);

        for (let tag of this.tags) {

            //-------------------------------------------------------------------
            // Validate Tag Exist
            //-------------------------------------------------------------------

            this.model.get$(`tag/${tag.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    //-------------------------------------------------------------------
                    // Delete Tag
                    //-------------------------------------------------------------------
                    this.log('Deleting Tag', tag);
                    this.model.delete$('tag', tag.id).subscribe((res: any) => {
                        this.log('Deleted Tag', tag);
                        this.tags = this.tags.filter(item => item.id != res.data.id);
                        this.tags$.next(this.tags);
                    });

                    //-------------------------------------------------------------------
                    // For each tag subtag, delete$ from DB
                    // Parent association broken on delete of parent
                    //-------------------------------------------------------------------
                    for (let subtag of res.data) {

                        //-------------------------------------------------------------------
                        // Validate Subtag Exist
                        //-------------------------------------------------------------------

                        this.model.get$(`subtag/${subtag.id}`).subscribe((res: any) => {
                            if (res.statusCode === 200 && res.data) {

                                //-------------------------------------------------------------------
                                // Validate Subtag
                                //-------------------------------------------------------------------
                                this.log('Deleting Subtag', subtag);
                                this.model.delete$('subtag', subtag.id).subscribe((res: any) => {
                                    this.log('Deleted Subtag', subtag);
                                    this.subtags = this.subtags.filter(item => item.id != res.data.id);
                                    this.subtags$.next(this.subtags);
                                });

                            }
                        });

                    }

                }
            });

        }

    }


}