import { Injectable } from '@angular/core'
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { Reference } from 'app/models/reference.model';
import { ReferenceVersion } from 'app/models/referenceversion.model';

/////////////////////////////////////////////////////////////////////////
// Tag Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class ReferenceService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private references: Array<any> = [];
    public references$: BehaviorSubject<Array<any>>;
    private referenceversions: Array<any> =[];
    public referenceversions$: BehaviorSubject<Array<any>>;
    private referenceshadowcopys: Array<any> = [];
    public referenceshadowcopys$: BehaviorSubject<Array<any>>;

    private user: any = {};

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService,
        private userService: UserService,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("ReferenceService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------
        this.references$ = new BehaviorSubject<Array<any>>([]);
        this.referenceversions$ = new BehaviorSubject<Array<any>>([]);
        this.referenceshadowcopys$ = new BehaviorSubject<Array<any>>([]);

        //-------------------------------------------------------------------
        // Subscribe to user
        //-------------------------------------------------------------------
        this.userService.user$.subscribe(user => {
            this.user = user;
        });
    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("ReferenceService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToReferences() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`reference`).subscribe(res => {

            this.log('References[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.references = res.data;
                this.references$.next(this.references);
            }
            if (res.verb === 'created') {
                this.references.unshift(res.data);
                this.references$.next(this.references);
            }
            if (res.verb === 'updated') {
                let index = this.references.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.references[index] = res.data;
                    this.references[index].id = res.id;
                }
                this.references$.next(this.references);
            }
            if (res.verb === 'destroyed') {
                this.references = this.references.filter(item => item.id != res.id);
                this.references$.next(this.references);
            }

        }, error => {

            this.log('ERROR: (subscribeToReferences)', error);

        });

    }

    subscribeToReferenceVersions() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`referenceversion`).subscribe(res => {

            this.log('ReferenceVersions[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.referenceversions = res.data;
                this.referenceversions$.next(this.referenceversions);
            }
            if (res.verb === 'created') {
                this.referenceversions.unshift(res.data);
                this.referenceversions$.next(this.referenceversions);
            }
            if (res.verb === 'updated') {
                let index = this.referenceversions.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.referenceversions[index] = res.data;
                    this.referenceversions[index].id = res.id;
                }
                this.referenceversions$.next(this.referenceversions);
            }
            if (res.verb === 'destroyed') {
                this.referenceversions = this.referenceversions.filter(item => item.id != res.id);
                this.referenceversions$.next(this.referenceversions);
            }

        }, error => {

            this.log('ERROR: (subscribeToReferenceVersions)', error);

        });

    }


    /////////////////////////////////////////////////////////////////////////
    // Reference Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // References
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a Reference & initial ReferenceVersion
    //-----------------------------------------------------------------------
    // All Reference must be initialized with a ReferenceVersion vice versa
    // What is the point of a reference pointer without a reference?
    //-----------------------------------------------------------------------

    createReference(referenceVersion: ReferenceVersion){

        //-----------------------------------------------------------------------
        // 1. Prepare Reference Body
        //-----------------------------------------------------------------------

        let reference = new Reference;
            delete reference.id;
            delete reference.createdAt;
            delete reference.updatedAt;
            reference.UniqueID = referenceVersion.UniqueID;
            reference.MasterVersion = null; // Expecting an id respresenting the association record, or null
            reference.CreatedBy = null; // Expecting an id respresenting the association record, or null
            reference.LastUpdatedBy = null; // Expecting an id respresenting the association record, or null

        if (this.user && this.user.id) {
            reference.CreatedBy = this.user.id;
            reference.LastUpdatedBy = this.user.id;
        }

        //-----------------------------------------------------------------------
        // 2. Prepare Reference Version
        //-----------------------------------------------------------------------

        let version = {...referenceVersion};
            delete version.id;
            delete version.createdAt;
            delete version.updatedAt;
            version.CreatedBy = null; // Expecting an id respresenting the association record, or null
            version.LastUpdatedBy = null; // Expecting an id respresenting the association record, or null

        if (this.user && this.user.id) {
            version.CreatedBy = this.user.id;
            version.LastUpdatedBy = this.user.id;
        }
        //-----------------------------------------------------------------------
        // 1. Push Reference to model
        //-----------------------------------------------------------------------

        this.model.create$('reference', { ...reference }).subscribe((res: any) => {

            this.log('Reference Created', res);

            //---------------------------------------------------------------
            // Publish Reference to References
            //---------------------------------------------------------------

            this.references.push(res.data);
            this.references$.next(this.references);

            //---------------------------------------------------------------
            // Perserve Reference for 3. ReferenceVersion association on Reference
            //---------------------------------------------------------------

            reference = {} = { ...res.data };

            //-----------------------------------------------------------------------
            // 2. Push ReferenceVersion to model
            //-----------------------------------------------------------------------
            // Associate Reference on ReferenceVersion
            //-----------------------------------------------------------------------

            version.Reference = res.data.id; // Expecting an id respresenting the association record, or null

            this.model.create$('referenceversion', { ...version }).subscribe((res: any) => {

                this.log('Reference Version Created', res);

                //---------------------------------------------------------------
                // Publish Reference to References
                //---------------------------------------------------------------

                this.referenceversions.push(res.data);
                this.referenceversions$.next(this.referenceversions);

                //-----------------------------------------------------------------------
                // 3. Update Reference to associate ReferenceVersion
                //-----------------------------------------------------------------------
                // Prepare Reference body for PATCH
                //-----------------------------------------------------------------------
                reference.MasterVersion = res.data.id;

                this.updateReference(reference);

            }, error => {

                this.log('ERROR: (createReferenceVersion)', error);

            });

        }, error => {

            this.log('ERROR: (createReference)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Update Reference
    //-----------------------------------------------------------------------

    updateReference(reference: Reference) {

        //-----------------------------------------------------------------------
        // 1. Prepare Reference
        //-----------------------------------------------------------------------

        delete reference.updatedAt;
        delete reference.Versions;

        delete reference.CreatedBy;
        delete reference.LastUpdatedBy;

        if (this.user && this.user.id) { reference.LastUpdatedBy = this.user.id; } else { reference.LastUpdatedBy = null };

        //-----------------------------------------------------------------------
        // 2. Push ReferenceVersion to model
        //-----------------------------------------------------------------------

        this.model.update$(`reference`, reference.id, { ...reference }).subscribe((res: any) => {

            this.log('Reference Updated', res);

            //---------------------------------------------------------------
            // Publish Reference to References
            //---------------------------------------------------------------

            this.references[this.references.findIndex(item => item.id == res.data.id)] = res.data;
            this.references$.next(this.references);

        }, error => {

            this.log('ERROR: (updateReference)', error);

        });

    }

    deleteReference(reference) {

        //-------------------------------------------------------------------
        // Validate Reference Exist
        //-------------------------------------------------------------------

        this.model.get$(`reference/${reference.id}`).subscribe((res: any) => {
            if (res.statusCode === 200 && res.data) {

                //-------------------------------------------------------------------
                // Delete Reference
                //-------------------------------------------------------------------
                this.log('Deleting Reference', reference);
                this.model.delete$('reference', reference.id).subscribe((res: any) => {
                    this.log('Deleted Reference', reference);
                    this.references = this.references.filter(item => item.id != res.data.id);
                    this.references$.next(this.references);
                });

                //-------------------------------------------------------------------
                // For each reference referenceversion, delete$ from DB
                // Parent association broken on delete of parent
                //-------------------------------------------------------------------
                for (let referenceversion of res.data.Versions) {

                    //-------------------------------------------------------------------
                    // Validate ReferenceVersion Exist
                    //-------------------------------------------------------------------

                    this.model.get$(`referenceversion/${referenceversion.id}`).subscribe((res: any) => {
                        if (res.statusCode === 200 && res.data) {

                            //-------------------------------------------------------------------
                            // Validate ReferenceVersion
                            //-------------------------------------------------------------------
                            this.log('Deleting ReferenceVersion', referenceversion);
                            this.model.delete$('referenceversion', referenceversion.id).subscribe((res: any) => {
                                this.log('Deleted ReferenceVersion', referenceversion);
                                this.referenceversions = this.referenceversions.filter(item => item.id != res.data.id);
                                this.referenceversions$.next(this.referenceversions);
                            });

                        }
                    });

                }

            }
        });

    }

    /////////////////////////////////////////////////////////////////////////
    // ReferenceVersions
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a ReferenceVersion and associate back to Reference
    //-----------------------------------------------------------------------

    createReferenceVersion(referenceVersion: ReferenceVersion, reference: Reference, master?: boolean) {

        //-----------------------------------------------------------------------
        // 1. Prepare Reference Version
        //-----------------------------------------------------------------------

        let version = { ...referenceVersion };
        delete version.id;
        delete version.createdAt;
        delete version.updatedAt;
        version.CreatedBy = null; // Expecting an id respresenting the association record, or null
        version.LastUpdatedBy = null; // Expecting an id respresenting the association record, or null

        if (this.user && this.user.id) {
            version.CreatedBy = this.user.id;
            version.LastUpdatedBy = this.user.id;
        }

        //-----------------------------------------------------------------------
        // 2. Push ReferenceVersion to model
        //-----------------------------------------------------------------------
        // Associate Reference on ReferenceVersion
        //-----------------------------------------------------------------------

        version.Reference = reference.id;

        this.model.create$('referenceversion', { ...version }).subscribe((res: any) => {

            this.log('Reference Version Created', res);

            //---------------------------------------------------------------
            // Publish Reference to References
            //---------------------------------------------------------------

            this.referenceversions.push(res.data);
            this.referenceversions$.next(this.referenceversions);

            //---------------------------------------------------------------
            // Set as Master
            //---------------------------------------------------------------
            if(master) {

                //-----------------------------------------------------------------------
                // 3. Update Reference to associate ReferenceVersion
                //-----------------------------------------------------------------------
                // Prepare Reference body for PATCH
                //-----------------------------------------------------------------------
                reference.MasterVersion = res.data.id;

                this.updateReference(reference);

            }

        }, error => {

            this.log('ERROR: (createReferenceVersion)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Update ReferenceVersion
    //-----------------------------------------------------------------------

    updateReferenceVersion(referenceVersion: ReferenceVersion) {

        //-----------------------------------------------------------------------
        // 1. Prepare Reference Version
        //-----------------------------------------------------------------------
        // Delete or reassign reference back onto referenceversion
        //-----------------------------------------------------------------------

        let version = { ...referenceVersion };
        delete version.updatedAt;

        if (version.Reference && version.Reference.id) { version.Reference = version.Reference.id } else { delete version.Reference };
        
        delete version.CreatedBy;
        delete version.LastUpdatedBy; 

        if (this.user && this.user.id) { version.LastUpdatedBy = this.user.id; } else { version.LastUpdatedBy = null };

        //-----------------------------------------------------------------------
        // 2. Push ReferenceVersion to model
        //-----------------------------------------------------------------------

        this.model.update$(`referenceversion`, version.id, { ...version }).subscribe((res: any) => {

            this.log('ReferenceVersion Updated', res);

            //---------------------------------------------------------------
            // Publish ReferenceVersion to Reference
            //---------------------------------------------------------------

            this.referenceversions[this.referenceversions.findIndex(item => item.id == res.data.id)] = res.data;
            this.referenceversions$.next(this.referenceversions);

            //---------------------------------------------------------------
            // Publish ReferenceVersion to Reference MasterVersion if it exist
            //---------------------------------------------------------------

            if (this.references[this.references.findIndex(item => item.MasterVersion && item.MasterVersion.id == res.data.id)]) {
                this.references[this.references.findIndex(item => item.MasterVersion && item.MasterVersion.id == res.data.id)].MasterVersion = res.data;
                this.references$.next(this.references);
            };

        }, error => {

            this.log('ERROR: (updateReference)', error);

        });

    }

    deleteReferenceVersion(referenceversion) {

        //-------------------------------------------------------------------
        // Validate ReferenceVersion Exist
        //-------------------------------------------------------------------

        this.model.get$(`referenceversion/${referenceversion.id}`).subscribe((res: any) => {
            if (res.statusCode === 200 && res.data) {

                //-------------------------------------------------------------------
                // Validate ReferenceVersion
                //-------------------------------------------------------------------
                this.log('Deleting ReferenceVersion', referenceversion);
                this.model.delete$('referenceversion', referenceversion.id).subscribe((res: any) => {
                    this.log('Deleted ReferenceVersion', referenceversion);
                    this.referenceversions = this.referenceversions.filter(item => item.id != res.data.id);
                    this.referenceversions$.next(this.referenceversions);
                });

            }
        });
   
    }

    /////////////////////////////////////////////////////////////////////////
    // ALL DOCUMENTS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Destroy References
    //-----------------------------------------------------------------------

    destroyReferences() {

        //-------------------------------------------------------------------
        // For each reference delete$ reference from DB 
        //-------------------------------------------------------------------
        this.log("Destroying References", this.references);

        for (let reference of this.references) {

            //-------------------------------------------------------------------
            // Validate Reference Exist
            //-------------------------------------------------------------------

            this.model.get$(`reference/${reference.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    //-------------------------------------------------------------------
                    // Delete Reference
                    //-------------------------------------------------------------------
                    this.log('Deleting Reference', reference);
                    this.model.delete$('reference', reference.id).subscribe((res: any) => {
                        this.log('Deleted Reference', reference);
                        this.references = this.references.filter(item => item.id != res.data.id);
                        this.references$.next(this.references);
                    });

                    //-------------------------------------------------------------------
                    // For each reference referenceversion, delete$ from DB
                    // Parent association broken on delete of parent
                    //-------------------------------------------------------------------
                    for (let referenceversion of res.data.Versions) {

                        //-------------------------------------------------------------------
                        // Validate ReferenceVersion Exist
                        //-------------------------------------------------------------------

                        this.model.get$(`referenceversion/${referenceversion.id}`).subscribe((res: any) => {
                            if (res.statusCode === 200 && res.data) {

                                //-------------------------------------------------------------------
                                // Validate ReferenceVersion
                                //-------------------------------------------------------------------
                                this.log('Deleting ReferenceVersion', referenceversion);
                                this.model.delete$('referenceversion', referenceversion.id).subscribe((res: any) => {
                                    this.log('Deleted ReferenceVersion', referenceversion);
                                    this.referenceversions = this.referenceversions.filter(item => item.id != res.data.id);
                                    this.referenceversions$.next(this.referenceversions);
                                });

                            }
                        });

                    }

                }
            });

        }

    }

}