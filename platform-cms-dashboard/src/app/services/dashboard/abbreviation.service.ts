import { Injectable } from '@angular/core'
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { Abbreviation } from 'app/models/abbreviation.model';
/////////////////////////////////////////////////////////////////////////
// Abbreviation Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class AbbreviationService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private abbreviations: Array<any> = [];
    public abbreviations$: BehaviorSubject<Array<any>>;

    private user: any = {};

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService,
        private userService: UserService,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("AbbreviationService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------

        this.abbreviations$ = new BehaviorSubject<Array<any>>([]);
        
        //-------------------------------------------------------------------
        // Subscribe to user
        //-------------------------------------------------------------------
        this.userService.user$.subscribe(user => {
            this.user = user;
        });
            
    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("AbbreviationService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToAbbreviations() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`abbreviation`).subscribe(res => {

            this.log('Abbreviations[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.abbreviations = res.data;
                this.abbreviations$.next(this.abbreviations);
            }
            if (res.verb === 'created') {
                this.abbreviations.unshift(res.data);
                this.abbreviations$.next(this.abbreviations);
            }
            if (res.verb === 'updated') {
                let index = this.abbreviations.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.abbreviations[index] = res.data;
                    this.abbreviations[index].id = res.id;
                }
                this.abbreviations$.next(this.abbreviations);
            }
            if (res.verb === 'destroyed') {
                this.abbreviations = this.abbreviations.filter(item => item.id != res.id);
                this.abbreviations$.next(this.abbreviations);
            }

        }, error => {

            this.log('ERROR: (subscribeToAbbreviations)', error);

        });

    }


    /////////////////////////////////////////////////////////////////////////
    // Abbreviation Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // TERMS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a Abbreviation
    //-----------------------------------------------------------------------

    createAbbreviation(abbreviation: Abbreviation){

        //-----------------------------------------------------------------------
        // Validate the abbreviation does not already exist, then create
        //-----------------------------------------------------------------------

        this.model.get$(`abbreviation?where={"Abbreviation": "${abbreviation}"}`).subscribe((res: any) => {

            if (res.statusCode === 200 && res.data.length < 1) {

                //-----------------------------------------------------------------------
                // Prepare body
                //-----------------------------------------------------------------------

                let body = { ...abbreviation };

                    delete body.id;
                    delete body.createdAt;
                    delete body.updatedAt;
                    body.CreatedBy = null;
                    body.LastUpdatedBy = null;

                    if(this.user && this.user.id) {
                        body.CreatedBy = this.user.id;
                        body.LastUpdatedBy = this.user.id;
                    }

                    this.log('Creating Abbreviation: ', body);

                //-----------------------------------------------------------------------
                // Push to model
                //-----------------------------------------------------------------------

                this.model.create$('abbreviation', { ...body }).subscribe((res: any) => {

                    //---------------------------------------------------------------
                    // Publish Abbreviation to Abbreviations
                    //---------------------------------------------------------------

                    this.abbreviations.push(res.data);
                    this.abbreviations$.next(this.abbreviations);

                }, error => {

                    this.log('ERROR: (createAbbreviation)', error);

                });

            }

        });

    }

    //-----------------------------------------------------------------------
    // Update Abbreviation
    //-----------------------------------------------------------------------

    updateAbbreviation(abbreviation: Abbreviation) {

        //-------------------------------------------------------------------
        // Prepare Body
        //-------------------------------------------------------------------

        abbreviation = { ...abbreviation };
        // delete abbreviation.createdAt;
        delete abbreviation.updatedAt;

        delete abbreviation.CreatedBy;
        delete abbreviation.LastUpdatedBy;

        if (this.user && this.user.id) { abbreviation.LastUpdatedBy = this.user.id; } else { abbreviation.LastUpdatedBy = null };

        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------

        this.model.update$('abbreviation', abbreviation.id, abbreviation).subscribe((res: any) => {
            this.log('Abbreviation Updated: ', res);
            //---------------------------------------------------------------
            // Publish Abbreviation to Abbreviations
            //---------------------------------------------------------------

            this.abbreviations[this.abbreviations.findIndex(item => item.id == res.data.id)] = res.data;
            this.abbreviations$.next(this.abbreviations);

        }, error => {

            this.log('ERROR: (updateAbbreviation)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Delete Abbreviation
    //-----------------------------------------------------------------------

    deleteAbbreviation(abbreviation: Abbreviation) {

            //-------------------------------------------------------------------
            // Validate Abbreviation Exist
            //-------------------------------------------------------------------

            this.model.get$(`abbreviation/${abbreviation.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    //-------------------------------------------------------------------
                    // Delete Abbreviation
                    //-------------------------------------------------------------------
                    this.log('Deleting Abbreviation', abbreviation);
                    this.model.delete$('abbreviation', abbreviation.id).subscribe((res: any) => {
                        this.log('Deleted Abbreviation', abbreviation);
                        this.abbreviations = this.abbreviations.filter(item => item.id != res.data.id);
                        this.abbreviations$.next(this.abbreviations);
                    });

                }
            });

    }

    /////////////////////////////////////////////////////////////////////////
    // ALL TERMS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Destroy Abbreviations
    //-----------------------------------------------------------------------

    destroyAbbreviations() {

        //-------------------------------------------------------------------
        // For each abbreviation delete$ abbreviation from DB 
        //-------------------------------------------------------------------
        this.log("Destroying Abbreviations", this.abbreviations);

        for (let abbreviation of this.abbreviations) {

            //-------------------------------------------------------------------
            // Validate Abbreviation Exist
            //-------------------------------------------------------------------

            this.model.get$(`abbreviation/${abbreviation.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    //-------------------------------------------------------------------
                    // Delete Abbreviation
                    //-------------------------------------------------------------------
                    this.log('Deleting Abbreviation', abbreviation);
                    this.model.delete$('abbreviation', abbreviation.id).subscribe((res: any) => {
                        this.log('Deleted Abbreviation', abbreviation);
                        this.abbreviations = this.abbreviations.filter(item => item.id != res.data.id);
                        this.abbreviations$.next(this.abbreviations);
                    });

                }
            });

        }

    }


}