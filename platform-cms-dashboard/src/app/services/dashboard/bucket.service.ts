import { Injectable } from '@angular/core'
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { findIndex, get, reject, unionBy } from 'lodash';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { Abbreviation } from 'app/models/abbreviation.model';
import { Bucket } from 'app/models/bucket.model';
/////////////////////////////////////////////////////////////////////////
// Bucket Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class BucketService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private buckets: Bucket[] = [];
    public buckets$: BehaviorSubject<Bucket[]>;

    private user: any = {};

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService,
        private userService: UserService,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("BucketService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------

        this.buckets$ = new BehaviorSubject<Bucket[]>([]);

        //-------------------------------------------------------------------
        // Subscribe to user
        //-------------------------------------------------------------------
        this.userService.user$.subscribe(user => {
            this.user = user;
        });

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("BucketService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToBuckets() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`bucket`).subscribe(res => {

            this.log('Buckets[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            switch (res.verb) {
                case undefined:
                    this.buckets = get(res, 'data', []);
                    break;

                case 'created':
                    this.buckets.unshift(res.data);
                    break;

                case 'updated':
                    const index = findIndex(this.buckets, { id: res.id });
                    if (index > -1) {
                        this.buckets[index] = {
                            ...res.data,
                            id: res.id
                        }
                    }
                    break;

                case 'destroyed':
                    this.buckets = reject(this.buckets, { id: res.id });
                    break;

                default: break;
            }

            this.buckets$.next(this.buckets);

        }, error => {

            this.log('ERROR: (subscribeToBuckets)', error);

        });

    }


    /////////////////////////////////////////////////////////////////////////
    // Bucket Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // TERMS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Update Bucket
    //-----------------------------------------------------------------------

    updateBucket(bucket: Bucket) {

        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------
        const { id } = bucket;

        this.model.update$('bucket', id, bucket).subscribe((response: any) => {

            this.log('Bucket Updated: ', response);

            //---------------------------------------------------------------
            // Publish Bucket to Buckets
            //---------------------------------------------------------------
            const newBucket = get(response, 'data', {});
            this.buckets = unionBy([newBucket], this.buckets, 'id');
            this.buckets$.next(this.buckets);

        }, error => {

            this.log('ERROR: (updateBucket)', error);

        });

    }

}