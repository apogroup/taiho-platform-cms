import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LoggerService } from '../logger/logger.service';
import { ModelService } from '../model/model.service';
import { UserService } from './user.service';
import { Congress } from 'app/models/congress.model';
import { S3Object } from 'app/models/s3object.model';

/////////////////////////////////////////////////////////////////////////
// Congress Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable({
  providedIn: 'root'
})
export class CongressService {

  /////////////////////////////////////////////////////////////////////////
  // Variables
  /////////////////////////////////////////////////////////////////////////

  private congresses: Array<any> = [];
  public congresses$: BehaviorSubject<Array<any>>;
  private user: any = {};

  /////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////

  constructor(
    private logger: LoggerService,
    private model: ModelService,
    private userService: UserService
  ) {
      /////////////////////////////////////////////////////////////////////
      // Logger
      /////////////////////////////////////////////////////////////////////
      // Configure
      //-------------------------------------------------------------------

      this.logger.config("CongressService",
      {
          label: 'background: #9c27b0; color: #333',
          text: ''
      }
    );

      //-------------------------------------------------------------------
      // Define Observable Subscriptions
      //-------------------------------------------------------------------
      this.congresses$ = new BehaviorSubject<Array<any>>([]);

      //-------------------------------------------------------------------
      // Subscribe to user
      //-------------------------------------------------------------------
      this.userService.user$.subscribe(user => {
          this.user = user;
      });
  }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
      this.logger.log("CongressService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Congress Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subscribe to Congresses (all)
    //-----------------------------------------------------------------------

      subscribeToCongresses() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`congress`).subscribe(res => {

          this.log('Contents[]', res);

          //---------------------------------------------------------------
          // Publish according to subscription verb
          //---------------------------------------------------------------

          if (!res.verb) {
              this.congresses = [...res.data].sort((a, b) => a.createdAt - b.createdAt);
              this.congresses$.next(this.congresses);
          }
          if (res.verb === 'created') {
              this.congresses.push(res.data);
              this.congresses$.next(this.congresses);
          }
          if (res.verb === 'updated') {
              let index = this.congresses.findIndex(item => item.id == res.id);
              if (index !== -1) {
                  this.congresses[index] = res.data;
                  this.congresses[index].id = res.id;
              }
              this.congresses$.next(this.congresses);
          }
          if (res.verb === 'destroyed') {
              this.congresses = this.congresses.filter(item => item.id != res.id);
              this.congresses$.next(this.congresses);
          }

      }, error => {

          this.log('ERROR: (subscribeToCongresses)', error);

      });


      }

    /////////////////////////////////////////////////////////////////////////
    // Congress Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // Congresses
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a Congress
    //-----------------------------------------------------------------------

    createCongress(congress: Congress){

      let body = { ...congress };

      delete body.id;
      delete body.createdAt;
      delete body.updatedAt;

      if (this.user && this.user.id) {
        congress.CreatedBy = this.user.id;
        congress.LastUpdatedBy = this.user.id;
      }

      //-----------------------------------------------------------------------
      // 1. Push Congress to model
      //-----------------------------------------------------------------------

      this.model.create$('congress', { ...body }).subscribe((res: any) => {

        this.log('Congress Created', res);

        //---------------------------------------------------------------
        // Publish Congress to Congresses
        //---------------------------------------------------------------

        this.congresses.push(res.data);
        this.congresses$.next(this.congresses);

      }, error => {

        this.log('ERROR: (createCongress)', error);
      });

    }

    //-----------------------------------------------------------------------
    // Update Congress
    //-----------------------------------------------------------------------

    updateCongress(congress: Congress) {

        //-----------------------------------------------------------------------
        // Prepare Congress
        //-----------------------------------------------------------------------

        delete congress.updatedAt;
        delete congress.CreatedBy;
        delete congress.LastUpdatedBy;

        if (this.user && this.user.id) { congress.LastUpdatedBy = this.user.id; } else { congress.LastUpdatedBy = null };

        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------

        this.model.update$('congress', congress.id, congress).subscribe((res: any) => {
          this.log('congress Updated: ', res);
          //---------------------------------------------------------------
          // Publish congress to congresss
          //---------------------------------------------------------------

          this.congresses[this.congresses.findIndex(item => item.id == res.data.id)] = res.data;
          this.congresses$.next(this.congresses);

        }, error => {

            this.log('ERROR: (updateCongresses)', error);

        });

    }

    deleteCongress(congress){

        //-------------------------------------------------------------------
        // Validate Congress Exist
        //-------------------------------------------------------------------

        this.model.get$(`congress/${congress.id}`).subscribe((res: any) => {
          if (res.statusCode === 200 && res.data) {

            //-------------------------------------------------------------------
            // Delete Congress
            //-------------------------------------------------------------------
            this.log('Deleting Congress', congress);
            this.model.delete$('congress', congress.id).subscribe((res: any) => {
              this.log('Deleting Congress', congress);
              this.congresses = this.congresses.filter(item => item.id != res.data.id);
              this.congresses$.next(this.congresses);
            });

          }
        });


    }

    /////////////////////////////////////////////////////////////////////////
    // ALL CONGRESSES
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Destroy Congresses
    //-----------------------------------------------------------------------

    destroyCongresses(){

        //-------------------------------------------------------------------
        // For each congresses delete$ congress from DB 
        //-------------------------------------------------------------------
        this.log("Destroying Congresses", this.congresses);

        for (let congress of this.congresses) {
            //-------------------------------------------------------------------
            // Validate Congress Exists
            //-------------------------------------------------------------------

            this.model.get$(`congress/${congress.id}`).subscribe((res: any) => {
              if (res.statusCode === 200 && res.data) {

                //-------------------------------------------------------------------
                // Delete Congress
                //-------------------------------------------------------------------
                this.log('Deleting Congress', congress);
                this.model.delete$('congress', congress.id).subscribe((res: any) => {
                  this.log('Deleted Congress', congress);
                  this.congresses = this.congresses.filter(item => item.id != res.data.id);
                  this.congresses$.next(this.congresses);
                });

              }
            });

        }

    }

  }