import { Injectable } from '@angular/core'
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { Content } from 'app/models/content.model';
import { Document } from 'app/models/document.model';
import { Reference } from 'app/models/reference.model';
import { S3Object } from 'app/models/s3object.model';

import { cloneDeep, unionBy } from 'lodash/fp';

/////////////////////////////////////////////////////////////////////////
// Content Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class ContentService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private contents: Array<any> = [];
    public contents$: BehaviorSubject<Array<any>>;

    private user: any = {};

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService,
        private userService: UserService,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("ContentService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------

        this.contents$ = new BehaviorSubject<Array<any>>([]);
        
        //-------------------------------------------------------------------
        // Subscribe to user
        //-------------------------------------------------------------------
        this.userService.user$.subscribe(user => {
            this.user = user;
        });
            
    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("ContentService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToContents() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`content`).subscribe(res => {

            this.log('Contents[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.contents = [...res.data].sort((a, b) => a.createdAt - b.createdAt);
                this.contents$.next(this.contents);
            }
            if (res.verb === 'created') {
                this.contents.push(res.data);
                this.contents$.next(this.contents);
            }
            if (res.verb === 'updated') {
                let index = this.contents.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.contents[index] = res.data;
                    this.contents[index].id = res.id;
                }
                this.contents$.next(this.contents);
            }
            if (res.verb === 'destroyed') {
                this.contents = this.contents.filter(item => item.id != res.id);
                this.contents$.next(this.contents);
            }

        }, error => {

            this.log('ERROR: (subscribeToContents)', error);

        });

    }


    /////////////////////////////////////////////////////////////////////////
    // Content Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // TERMS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a Content
    //-----------------------------------------------------------------------

    createContent(content: Content){

        //-----------------------------------------------------------------------
        // Prepare body
        //-----------------------------------------------------------------------

        let body = { ...content };

            delete body.id;
            delete body.createdAt;
            delete body.updatedAt;
            if(!body.Parents) { delete body.Parents };
            if(!body.Children) { delete body.Children };
            body.CreatedBy = null;
            body.LastUpdatedBy = null;

            if(this.user && this.user.id) {
                body.CreatedBy = this.user.id;
                body.LastUpdatedBy = this.user.id;
            }

            body.Documents = content.Documents.map((document: Document) => document.id);
            body.References = content.References.map((reference: Reference) => reference.id);

            this.log('Creating Content: ', body);

        //-----------------------------------------------------------------------
        // Push to model
        //-----------------------------------------------------------------------

        this.model.create$('content', { ...body }).subscribe((res: any) => {

            //---------------------------------------------------------------
            // Publish Content to Contents
            //---------------------------------------------------------------

            this.contents.push(res.data);
            this.contents$.next(this.contents);

        }, error => {

            this.log('ERROR: (createContent)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Update Content
    //-----------------------------------------------------------------------

    updateContent(content: Content) {

        //-------------------------------------------------------------------
        // Prepare Body
        //-------------------------------------------------------------------

        content = { ...content };
        // delete content.createdAt;
        delete content.updatedAt;
        delete content.Parents;
        delete content.Children;

        delete content.CreatedBy;
        delete content.LastUpdatedBy;

        if (this.user && this.user.id) { content.LastUpdatedBy = this.user.id; } else { content.LastUpdatedBy = null };

        content.Documents = content.Documents.map((document: Document) => document.id);
        content.References = content.References.map((reference: Reference) => reference.id);

        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------

        this.model.update$('content', content.id, content).subscribe((res: any) => {
            this.log('Content Updated: ', res);
            //---------------------------------------------------------------
            // Publish Content to Contents
            //---------------------------------------------------------------

            this.contents[this.contents.findIndex(item => item.id == res.data.id)] = res.data;
            this.contents$.next(this.contents);

        }, error => {

            this.log('ERROR: (updateContent)', error);

        });

    }

    updateManyContent(content: Content[]) {

        //-------------------------------------------------------------------
        // Prepare Body
        //-------------------------------------------------------------------
        const body: Content[] = cloneDeep(content);

        for(const content of body){
            // delete content.createdAt;
            delete content.updatedAt;
            delete content.Parents;
            delete content.Children;

            delete content.CreatedBy;
            delete content.LastUpdatedBy;

            if (this.user && this.user.id) { content.LastUpdatedBy = this.user.id; } else { content.LastUpdatedBy = null; }

            content.Documents = content.Documents.map((document: Document) => document.id);
            content.References = content.References.map((reference: Reference) => reference.id);
        }

        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------

        this.model.updateMany$('content', body).subscribe((res: any) => {
            this.log('Content Updated: ', res);
            //---------------------------------------------------------------
            // Publish Content to Contents
            //---------------------------------------------------------------


            this.contents = unionBy('id', this.contents, res.data);

            for(const record of res.data as Content[]){
                const index = this.contents.findIndex(item => item.id == record.id)
                this.contents[index] = record;
            }

            this.contents$.next(this.contents);

        }, error => {

            this.log('ERROR: (updateContent)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Delete Content
    //-----------------------------------------------------------------------

    deleteContent(content: Content) {

            //-------------------------------------------------------------------
            // Validate Content Exist
            //-------------------------------------------------------------------

            this.model.get$(`content/${content.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    this.log('FOUND CONTENT, delete', res.data);

                    //-------------------------------------------------------------------
                    // Delete Content
                    //-------------------------------------------------------------------
                    this.log('Deleting Content', content);
                    this.model.delete$('content', content.id).subscribe((res: any) => {
                        this.log('Deleted Content', content);
                        this.contents = this.contents.filter(item => item.id != res.data.id);
                        this.contents$.next(this.contents);
                    });

                    //-------------------------------------------------------------------
                    // Recursively Delete Children
                    //-------------------------------------------------------------------

                    if(res.data.Children) {
                        res.data.Children.forEach(child => {
                            this.deleteContent(child);
                        })
                    }
                    
                }
            });

    }

    /////////////////////////////////////////////////////////////////////////
    // ALL TERMS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Destroy Contents
    //-----------------------------------------------------------------------

    destroyContents() {

        //-------------------------------------------------------------------
        // For each content delete$ content from DB 
        //-------------------------------------------------------------------
        this.log("Destroying Contents", this.contents);

        for (let content of this.contents) {

            //-------------------------------------------------------------------
            // Validate Content Exist
            //-------------------------------------------------------------------

            this.model.get$(`content/${content.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    //-------------------------------------------------------------------
                    // Delete Content
                    //-------------------------------------------------------------------
                    this.log('Deleting Content', content);
                    this.model.delete$('content', content.id).subscribe((res: any) => {
                        this.log('Deleted Content', content);
                        this.contents = this.contents.filter(item => item.id != res.data.id);
                        this.contents$.next(this.contents);
                    });

                }
            });

        }

    }


}