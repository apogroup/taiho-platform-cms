import { Injectable } from '@angular/core'
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { Document } from 'app/models/document.model';
import { DocumentVersion } from 'app/models/documentversion.model';

/////////////////////////////////////////////////////////////////////////
// Tag Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class DocumentService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private documents: Array<any> = [];
    public documents$: BehaviorSubject<Array<any>>;
    private documentversions: Array<any> =[];
    public documentversions$: BehaviorSubject<Array<any>>;
    private documentshadowcopys: Array<any> = [];
    public documentshadowcopys$: BehaviorSubject<Array<any>>;

    private user: any = {};

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService,
        private userService: UserService,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("DocumentService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------
        this.documents$ = new BehaviorSubject<Array<any>>([]);
        this.documentversions$ = new BehaviorSubject<Array<any>>([]);
        this.documentshadowcopys$ = new BehaviorSubject<Array<any>>([]);

        //-------------------------------------------------------------------
        // Subscribe to user
        //-------------------------------------------------------------------
        this.userService.user$.subscribe(user => {
            this.user = user;
        });
    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("DocumentService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToDocuments() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`document`).subscribe(res => {

            this.log('Documents[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.documents = res.data;
                this.documents$.next(this.documents);
            }
            if (res.verb === 'created') {
                this.documents.unshift(res.data);
                this.documents$.next(this.documents);
            }
            if (res.verb === 'updated') {
                let index = this.documents.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.documents[index] = res.data;
                    this.documents[index].id = res.id;
                }
                this.documents$.next(this.documents);
            }
            if (res.verb === 'destroyed') {
                this.documents = this.documents.filter(item => item.id != res.id);
                this.documents$.next(this.documents);
            }

        }, error => {

            this.log('ERROR: (subscribeToDocuments)', error);

        });

    }

    subscribeToDocumentVersions() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`documentversion`).subscribe(res => {

            this.log('DocumentVersions[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.documentversions = res.data;
                this.documentversions$.next(this.documentversions);
            }
            if (res.verb === 'created') {
                this.documentversions.unshift(res.data);
                this.documentversions$.next(this.documentversions);
            }
            if (res.verb === 'updated') {
                let index = this.documentversions.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.documentversions[index] = res.data;
                    this.documentversions[index].id = res.id;
                }
                this.documentversions$.next(this.documentversions);
            }
            if (res.verb === 'destroyed') {
                this.documentversions = this.documentversions.filter(item => item.id != res.id);
                this.documentversions$.next(this.documentversions);
            }

        }, error => {

            this.log('ERROR: (subscribeToDocumentVersions)', error);

        });

    }


    /////////////////////////////////////////////////////////////////////////
    // Document Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // Documents
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a Document & initial DocumentVersion
    //-----------------------------------------------------------------------
    // All Document must be initialized with a DocumentVersion vice versa
    // What is the point of a document pointer without a reference?
    //-----------------------------------------------------------------------

    createDocument(documentVersion: DocumentVersion){

        //-----------------------------------------------------------------------
        // 1. Prepare Document Body
        //-----------------------------------------------------------------------

        let document = new Document;
            delete document.id;
            delete document.createdAt;
            delete document.updatedAt;
            document.MasterVersion = null; // Expecting an id respresenting the association record, or null
            document.CreatedBy = null; // Expecting an id respresenting the association record, or null
            document.LastUpdatedBy = null; // Expecting an id respresenting the association record, or null

        if (this.user && this.user.id) {
            document.CreatedBy = this.user.id;
            document.LastUpdatedBy = this.user.id;
        }

        //-----------------------------------------------------------------------
        // 2. Prepare Document Version
        //-----------------------------------------------------------------------

        let version = {...documentVersion};
            delete version.id;
            delete version.createdAt;
            delete version.updatedAt;
            version.CreatedBy = null; // Expecting an id respresenting the association record, or null
            version.LastUpdatedBy = null; // Expecting an id respresenting the association record, or null

        if (this.user && this.user.id) {
            version.CreatedBy = this.user.id;
            version.LastUpdatedBy = this.user.id;
        }
        //-----------------------------------------------------------------------
        // 1. Push Document to model
        //-----------------------------------------------------------------------

        this.model.create$('document', { ...document }).subscribe((res: any) => {

            this.log('Document Created', res);

            //---------------------------------------------------------------
            // Publish Document to Documents
            //---------------------------------------------------------------

            this.documents.push(res.data);
            this.documents$.next(this.documents);

            //---------------------------------------------------------------
            // Perserve Document for 3. DocumentVersion association on Document
            //---------------------------------------------------------------

            document = {} = { ...res.data };

            //-----------------------------------------------------------------------
            // 2. Push DocumentVersion to model
            //-----------------------------------------------------------------------
            // Associate Document on DocumentVersion
            //-----------------------------------------------------------------------

            version.Document = res.data.id; // Expecting an id respresenting the association record, or null

            this.model.create$('documentversion', { ...version }).subscribe((res: any) => {

                this.log('Document Version Created', res);

                //---------------------------------------------------------------
                // Publish Document to Documents
                //---------------------------------------------------------------

                this.documentversions.push(res.data);
                this.documentversions$.next(this.documentversions);

                //-----------------------------------------------------------------------
                // 3. Update Document to associate DocumentVersion
                //-----------------------------------------------------------------------
                // Prepare Document body for PATCH
                //-----------------------------------------------------------------------
                document.MasterVersion = res.data.id;

                this.updateDocument(document);

            }, error => {

                this.log('ERROR: (createDocumentVersion)', error);

            });

        }, error => {

            this.log('ERROR: (createDocument)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Update Document
    //-----------------------------------------------------------------------

    updateDocument(document: Document) {

        //-----------------------------------------------------------------------
        // 1. Prepare Document
        //-----------------------------------------------------------------------

        delete document.updatedAt;
        delete document.Versions;

        delete document.CreatedBy;
        delete document.LastUpdatedBy;

        if (this.user && this.user.id) { document.LastUpdatedBy = this.user.id; } else { document.LastUpdatedBy = null };

        //-----------------------------------------------------------------------
        // 2. Push DocumentVersion to model
        //-----------------------------------------------------------------------

        this.model.update$(`document`, document.id, { ...document }).subscribe((res: any) => {

            this.log('Document Updated', res);

            //---------------------------------------------------------------
            // Publish Document to Documents
            //---------------------------------------------------------------

            this.documents[this.documents.findIndex(item => item.id == res.data.id)] = res.data;
            this.documents$.next(this.documents);

        }, error => {

            this.log('ERROR: (updateDocument)', error);

        });

    }

    deleteDocument(document) {

        //-------------------------------------------------------------------
        // Validate Document Exist
        //-------------------------------------------------------------------

        this.model.get$(`document/${document.id}`).subscribe((res: any) => {
            if (res.statusCode === 200 && res.data) {

                //-------------------------------------------------------------------
                // Delete Document
                //-------------------------------------------------------------------
                this.log('Deleting Document', document);
                this.model.delete$('document', document.id).subscribe((res: any) => {
                    this.log('Deleted Document', document);
                    this.documents = this.documents.filter(item => item.id != res.data.id);
                    this.documents$.next(this.documents);
                });

                //-------------------------------------------------------------------
                // For each document documentversion, delete$ from DB
                // Parent association broken on delete of parent
                //-------------------------------------------------------------------
                for (let documentversion of res.data.Versions) {

                    //-------------------------------------------------------------------
                    // Validate DocumentVersion Exist
                    //-------------------------------------------------------------------

                    this.model.get$(`documentversion/${documentversion.id}`).subscribe((res: any) => {
                        if (res.statusCode === 200 && res.data) {

                            //-------------------------------------------------------------------
                            // Validate DocumentVersion
                            //-------------------------------------------------------------------
                            this.log('Deleting DocumentVersion', documentversion);
                            this.model.delete$('documentversion', documentversion.id).subscribe((res: any) => {
                                this.log('Deleted DocumentVersion', documentversion);
                                this.documentversions = this.documentversions.filter(item => item.id != res.data.id);
                                this.documentversions$.next(this.documentversions);
                            });

                        }
                    });

                }

            }
        });

    }

    /////////////////////////////////////////////////////////////////////////
    // DocumentVersions
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a DocumentVersion and associate back to Document
    //-----------------------------------------------------------------------

    createDocumentVersion(documentVersion: DocumentVersion, document: Document, master?: boolean) {

        //-----------------------------------------------------------------------
        // 1. Prepare Document Version
        //-----------------------------------------------------------------------

        let version = { ...documentVersion };
        delete version.id;
        delete version.createdAt;
        delete version.updatedAt;
        version.CreatedBy = null; // Expecting an id respresenting the association record, or null
        version.LastUpdatedBy = null; // Expecting an id respresenting the association record, or null

        if (this.user && this.user.id) {
            version.CreatedBy = this.user.id;
            version.LastUpdatedBy = this.user.id;
        }

        //-----------------------------------------------------------------------
        // 2. Push DocumentVersion to model
        //-----------------------------------------------------------------------
        // Associate Document on DocumentVersion
        //-----------------------------------------------------------------------

        version.Document = document.id;

        this.model.create$('documentversion', { ...version }).subscribe((res: any) => {

            this.log('Document Version Created', res);

            //---------------------------------------------------------------
            // Publish Document to Documents
            //---------------------------------------------------------------

            this.documentversions.push(res.data);
            this.documentversions$.next(this.documentversions);

            //---------------------------------------------------------------
            // Set as Master
            //---------------------------------------------------------------
            if(master) {

                //-----------------------------------------------------------------------
                // 3. Update Document to associate DocumentVersion
                //-----------------------------------------------------------------------
                // Prepare Document body for PATCH
                //-----------------------------------------------------------------------
                document.MasterVersion = res.data.id;

                this.updateDocument(document);

            }

        }, error => {

            this.log('ERROR: (createDocumentVersion)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Update DocumentVersion
    //-----------------------------------------------------------------------

    updateDocumentVersion(documentVersion: DocumentVersion) {

        //-----------------------------------------------------------------------
        // 1. Prepare Document Version
        //-----------------------------------------------------------------------
        // Delete or reassign document back onto documentversion
        //-----------------------------------------------------------------------

        let version = { ...documentVersion };
        delete version.updatedAt;

        if (version.Document && version.Document.id) { version.Document = version.Document.id } else { delete version.Document };
        
        delete version.CreatedBy;
        delete version.LastUpdatedBy; 

        if (this.user && this.user.id) { version.LastUpdatedBy = this.user.id; } else { version.LastUpdatedBy = null };

        //-----------------------------------------------------------------------
        // 2. Push DocumentVersion to model
        //-----------------------------------------------------------------------

        this.model.update$(`documentversion`, version.id, { ...version }).subscribe((res: any) => {

            this.log('DocumentVersion Updated', res);

            //---------------------------------------------------------------
            // Publish DocumentVersion to Document
            //---------------------------------------------------------------

            this.documentversions[this.documentversions.findIndex(item => item.id == res.data.id)] = res.data;
            this.documentversions$.next(this.documentversions);

            //---------------------------------------------------------------
            // Publish DocumentVersion to Document MasterVersion if it exist
            //---------------------------------------------------------------

            if (this.documents[this.documents.findIndex(item => item.MasterVersion && item.MasterVersion.id == res.data.id)]) {
                this.documents[this.documents.findIndex(item => item.MasterVersion && item.MasterVersion.id == res.data.id)].MasterVersion = res.data;
                this.documents$.next(this.documents);
            };

        }, error => {

            this.log('ERROR: (updateDocument)', error);

        });

    }

    deleteDocumentVersion(documentversion) {

        //-------------------------------------------------------------------
        // Validate DocumentVersion Exist
        //-------------------------------------------------------------------

        this.model.get$(`documentversion/${documentversion.id}`).subscribe((res: any) => {
            if (res.statusCode === 200 && res.data) {

                //-------------------------------------------------------------------
                // Validate DocumentVersion
                //-------------------------------------------------------------------
                this.log('Deleting DocumentVersion', documentversion);
                this.model.delete$('documentversion', documentversion.id).subscribe((res: any) => {
                    this.log('Deleted DocumentVersion', documentversion);
                    this.documentversions = this.documentversions.filter(item => item.id != res.data.id);
                    this.documentversions$.next(this.documentversions);
                });

            }
        });
   
    }

    /////////////////////////////////////////////////////////////////////////
    // ALL DOCUMENTS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Destroy Documents
    //-----------------------------------------------------------------------

    destroyDocuments() {

        //-------------------------------------------------------------------
        // For each document delete$ document from DB 
        //-------------------------------------------------------------------
        this.log("Destroying Documents", this.documents);

        for (let document of this.documents) {

            //-------------------------------------------------------------------
            // Validate Document Exist
            //-------------------------------------------------------------------

            this.model.get$(`document/${document.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    //-------------------------------------------------------------------
                    // Delete Document
                    //-------------------------------------------------------------------
                    this.log('Deleting Document', document);
                    this.model.delete$('document', document.id).subscribe((res: any) => {
                        this.log('Deleted Document', document);
                        this.documents = this.documents.filter(item => item.id != res.data.id);
                        this.documents$.next(this.documents);
                    });

                    //-------------------------------------------------------------------
                    // For each document documentversion, delete$ from DB
                    // Parent association broken on delete of parent
                    //-------------------------------------------------------------------
                    for (let documentversion of res.data.Versions) {

                        //-------------------------------------------------------------------
                        // Validate DocumentVersion Exist
                        //-------------------------------------------------------------------

                        this.model.get$(`documentversion/${documentversion.id}`).subscribe((res: any) => {
                            if (res.statusCode === 200 && res.data) {

                                //-------------------------------------------------------------------
                                // Validate DocumentVersion
                                //-------------------------------------------------------------------
                                this.log('Deleting DocumentVersion', documentversion);
                                this.model.delete$('documentversion', documentversion.id).subscribe((res: any) => {
                                    this.log('Deleted DocumentVersion', documentversion);
                                    this.documentversions = this.documentversions.filter(item => item.id != res.data.id);
                                    this.documentversions$.next(this.documentversions);
                                });

                            }
                        });

                    }

                }
            });

        }

    }

}