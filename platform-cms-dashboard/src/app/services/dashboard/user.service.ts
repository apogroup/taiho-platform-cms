import { Injectable } from '@angular/core'
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';

import { User } from 'app/models/user.model';

/////////////////////////////////////////////////////////////////////////
// User Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class UserService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private users: Array<any> = [];
    public users$: BehaviorSubject<Array<any>>;
    private user: User;
    public user$: BehaviorSubject<any>;

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("UserService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------

        this.users$ = new BehaviorSubject<Array<any>>([]);
        this.user$ = new BehaviorSubject({});

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("UserService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToAllUsers() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`user`).subscribe(res => {

            this.log('Users[]', res);
            
            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.users = res.data;
                this.users$.next(this.users);
            }
            if (res.verb === 'created') {
                this.users.unshift(res.data);
                this.users$.next(this.users);
            }
            if (res.verb === 'updated') {
                let index = this.users.findIndex(item => item.id == res.id);
                if(index !== -1) {
                    this.users[index] = res.data;
                    this.users[index].id = res.id;
                }
                this.users$.next(this.users);
            }
            if (res.verb === 'destroyed') {
                this.users = this.users.filter(item => item.id != res.id);
                this.users$.next(this.users);
            }

        }, error => {

            this.log('ERROR: (subscribeToAllUsers)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Get Session User
    //-----------------------------------------------------------------------
    
    getSessionUser() {
        this.model.get$('session-user').subscribe((res: any) => {
            this.log('Session User{}', res);
            if(res.data) {
                this.user = res.data;
                this.user$.next(this.user);
            }
        });
    }

    
    /////////////////////////////////////////////////////////////////////////
    // User Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Update User
    //-----------------------------------------------------------------------

    updateUser(user: User) {

        //-------------------------------------------------------------------
        // Prepare Body
        //-------------------------------------------------------------------

        user = { ...user };
        // delete user.createdAt;
        delete user.updatedAt;

        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------

        this.model.update$('user', user.id, user).subscribe((res: any) => {
            this.log('User Updated: ', res);
            //---------------------------------------------------------------
            // Publish User to Users
            //---------------------------------------------------------------

            this.users[this.users.findIndex(item => item.id == res.data.id)] = res.data;
            this.users$.next(this.users);

        }, error => {

            this.log('ERROR: (updateUser)', error);

        });

    }


}