import { Injectable } from '@angular/core'
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { LoggerService } from 'app/services/logger/logger.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';

import { LexiconTerm } from 'app/models/lexiconterm.model';
/////////////////////////////////////////////////////////////////////////
// Lexicon Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class LexiconService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    private terms: Array<any> = [];
    public terms$: BehaviorSubject<Array<any>>;

    private user: any = {};

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private model: ModelService,
        private userService: UserService,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("LexiconService",
            {
                label: 'background: #9c27b0; color: #333',
                text: ''
            }
        );

        //-------------------------------------------------------------------
        // Define Observable Subscriptions
        //-------------------------------------------------------------------

        this.terms$ = new BehaviorSubject<Array<any>>([]);
        
        //-------------------------------------------------------------------
        // Subscribe to user
        //-------------------------------------------------------------------
        this.userService.user$.subscribe(user => {
            this.user = user;
        });
            
    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("LexiconService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // User Management Methods
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Subsribe to Users (all)
    //-----------------------------------------------------------------------

    subscribeToTerms() {

        //---------------------------------------------------------------
        // Subscribe to Model
        //---------------------------------------------------------------

        this.model.pubsub$(`lexiconterm`).subscribe(res => {

            this.log('LexiconTerms[]', res);

            //---------------------------------------------------------------
            // Publish according to subscription verb
            //---------------------------------------------------------------

            if (!res.verb) {
                this.terms = res.data;
                this.terms$.next(this.terms);
            }
            if (res.verb === 'created') {
                this.terms.unshift(res.data);
                this.terms$.next(this.terms);
            }
            if (res.verb === 'updated') {
                let index = this.terms.findIndex(item => item.id == res.id);
                if (index !== -1) {
                    this.terms[index] = res.data;
                    this.terms[index].id = res.id;
                }
                this.terms$.next(this.terms);
            }
            if (res.verb === 'destroyed') {
                this.terms = this.terms.filter(item => item.id != res.id);
                this.terms$.next(this.terms);
            }

        }, error => {

            this.log('ERROR: (subscribeToTerms)', error);

        });

    }


    /////////////////////////////////////////////////////////////////////////
    // Term Methods
    /////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////
    // TERMS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Create a Term
    //-----------------------------------------------------------------------

    createTerm(term: LexiconTerm){

        //-----------------------------------------------------------------------
        // Validate the term does not already exist, then create
        //-----------------------------------------------------------------------

        this.model.get$(`lexiconterm?where={"Term": "${term}"}`).subscribe((res: any) => {

            if (res.statusCode === 200 && res.data.length < 1) {

                //-----------------------------------------------------------------------
                // Prepare body
                //-----------------------------------------------------------------------

                let body = { ...term };

                    delete body.id;
                    delete body.createdAt;
                    delete body.updatedAt;
                    body.CreatedBy = null;
                    body.LastUpdatedBy = null;

                    if(this.user && this.user.id) {
                        body.CreatedBy = this.user.id;
                        body.LastUpdatedBy = this.user.id;
                    }

                    this.log('Creating Lexicon Term: ', body);

                //-----------------------------------------------------------------------
                // Push to model
                //-----------------------------------------------------------------------

                this.model.create$('lexiconterm', { ...body }).subscribe((res: any) => {

                    //---------------------------------------------------------------
                    // Publish Term to LexiconTerms
                    //---------------------------------------------------------------

                    this.terms.push(res.data);
                    this.terms$.next(this.terms);

                }, error => {

                    this.log('ERROR: (createTerm)', error);

                });

            }

        });

    }

    //-----------------------------------------------------------------------
    // Update Term
    //-----------------------------------------------------------------------

    updateTerm(term: LexiconTerm) {

        //-------------------------------------------------------------------
        // Prepare Body
        //-------------------------------------------------------------------

        term = { ...term };
        // delete term.createdAt;
        delete term.updatedAt;

        delete term.CreatedBy;
        delete term.LastUpdatedBy;

        if (this.user && this.user.id) { term.LastUpdatedBy = this.user.id; } else { term.LastUpdatedBy = null };

        //-------------------------------------------------------------------
        // Push to Model
        //-------------------------------------------------------------------

        this.model.update$('lexiconterm', term.id, term).subscribe((res: any) => {
            this.log('Term Updated: ', res);
            //---------------------------------------------------------------
            // Publish Term to LexiconTerms
            //---------------------------------------------------------------

            this.terms[this.terms.findIndex(item => item.id == res.data.id)] = res.data;
            this.terms$.next(this.terms);

        }, error => {

            this.log('ERROR: (updateTerm)', error);

        });

    }

    //-----------------------------------------------------------------------
    // Delete Term
    //-----------------------------------------------------------------------

    deleteTerm(term: LexiconTerm) {

            //-------------------------------------------------------------------
            // Validate Term Exist
            //-------------------------------------------------------------------

            this.model.get$(`lexiconterm/${term.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    //-------------------------------------------------------------------
                    // Delete Term
                    //-------------------------------------------------------------------
                    this.log('Deleting Term', term);
                    this.model.delete$('lexiconterm', term.id).subscribe((res: any) => {
                        this.log('Deleted Term', term);
                        this.terms = this.terms.filter(item => item.id != res.data.id);
                        this.terms$.next(this.terms);
                    });

                }
            });

    }

    /////////////////////////////////////////////////////////////////////////
    // ALL TERMS
    /////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------
    // Destroy Terms
    //-----------------------------------------------------------------------

    destroyTerms() {

        //-------------------------------------------------------------------
        // For each term delete$ term from DB 
        //-------------------------------------------------------------------
        this.log("Destroying Terms", this.terms);

        for (let term of this.terms) {

            //-------------------------------------------------------------------
            // Validate Term Exist
            //-------------------------------------------------------------------

            this.model.get$(`lexiconterm/${term.id}`).subscribe((res: any) => {
                if (res.statusCode === 200 && res.data) {

                    //-------------------------------------------------------------------
                    // Delete Term
                    //-------------------------------------------------------------------
                    this.log('Deleting Term', term);
                    this.model.delete$('lexiconterm', term.id).subscribe((res: any) => {
                        this.log('Deleted Term', term);
                        this.terms = this.terms.filter(item => item.id != res.data.id);
                        this.terms$.next(this.terms);
                    });

                }
            });

        }

    }


}