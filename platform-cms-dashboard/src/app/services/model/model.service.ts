import { Injectable, NgZone } from '@angular/core'
import { Observable, Subject } from 'rxjs';
import * as uuid from 'uuid';

import { LoggerService } from 'app/services/logger/logger.service';
import { WebsocketService } from 'app/services/websocket/websocket.service';

/////////////////////////////////////////////////////////////////////////
// Model Service
/////////////////////////////////////////////////////////////////////////
// Service to subscribe, create, and update Sails models
// Uses websocket connection$ to queue observers if socket not connected
//---------------------------------------------------------------------//

@Injectable()
export class ModelService {

    /////////////////////////////////////////////////////////////////////////
    // Variables
    /////////////////////////////////////////////////////////////////////////

    //---------------------------------------------------------------------//
    // Status
    //---------------------------------------------------------------------//
    private connected$: boolean = false;
    private subscriptions: any = {
        'blast': {},
        'pubsub': {},
        'get': {},
        'create': {},
        'update': {},
        'delete': {},
        'connect': {},
    };

    /////////////////////////////////////////////////////////////////////////
    // Constructor
    /////////////////////////////////////////////////////////////////////////

    constructor(
        private logger: LoggerService,
        private websocket: WebsocketService,
        private zone: NgZone,
    ) {

        /////////////////////////////////////////////////////////////////////
        // Logger
        /////////////////////////////////////////////////////////////////////
        // Configure
        //-------------------------------------------------------------------

        this.logger.config("ModelService",
            {
                label: 'background: #018; color: #eee',
                text: ''
            }
        );

        /////////////////////////////////////////////////////////////////////
        // Subscribe to Websocket Status
        /////////////////////////////////////////////////////////////////////

        this.websocket.connection$.subscribe(response => {
            this.connected$ = response.connected;
        });

    }

    /////////////////////////////////////////////////////////////////////////
    // Logger 
    /////////////////////////////////////////////////////////////////////////
    // log()
    //-----------------------------------------------------------------------

    log(text, args?) {
        this.logger.log("ModelService", text, ...args);
    }

    /////////////////////////////////////////////////////////////////////////
    // Model Methods
    /////////////////////////////////////////////////////////////////////////

    //---------------------------------------------------------------------//
    // Subscribe to a blast via sails on
    //---------------------------------------------------------------------//

    blast$(model: string): Observable<any> {

        let subject = new Subject();

        //-----------------------------------------------------------------//
        // Observable will only subscribe if there is a connected socket &
        // if there is not will wait for connection$ update
        //-----------------------------------------------------------------//
        // Note: it is possible for these observables will never resolve
        //-----------------------------------------------------------------//

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            //---------------------------------------------------------//
            // CAUTION: If this websocket connection is not unsubsribed
            // from this websocket request will run every time the
            // socket connects -- this can result in progressive duplicate
            // request -- not safe for setters, memory leak for getters
            //---------------------------------------------------------//

            let subscription_uuid = uuid();

            this.subscriptions['blast'][subscription_uuid] = this.websocket.connection$.subscribe(response => { if(response.connected) {
                
                //---------------------------------------------------------//
                // Clean up this connection$ subscription
                //---------------------------------------------------------//
                setTimeout(() => {
                    if (this.subscriptions['blast'][subscription_uuid]) {
                        this.subscriptions['blast'][subscription_uuid].unsubscribe();
                    }
                }, 500);

                //---------------------------------------------------------//
                // Handle broadcast messages from the socket
                //---------------------------------------------------------//

                this.websocket.on$(model).subscribe(res => {
                    res.error ? subject.error(res) : subject.next(res);
                    res.error && subject.complete();
                });

            }});

        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }
    
    //---------------------------------------------------------------------//
    // Subscribe to a model via sails PubSub
    //---------------------------------------------------------------------//

    pubsub$(model: string): Observable<any> {

        let subject = new Subject();

        //-----------------------------------------------------------------//
        // Observable will only subscribe if there is a connected socket &
        // if there is not will wait for connection$ update
        //-----------------------------------------------------------------//
        // Note: it is possible for these observables will never resolve
        //-----------------------------------------------------------------//
        
        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            //---------------------------------------------------------//
            // CAUTION: If this websocket connection is not unsubsribed
            // from this websocket request will run every time the
            // socket connects -- this can result in progressive duplicate
            // request -- not safe for setters, memory leak for getters
            //---------------------------------------------------------//

            let subscription_uuid = uuid();

            this.subscriptions['pubsub'][subscription_uuid] = this.websocket.connection$.subscribe(response => { if(response.connected) {
                
                //---------------------------------------------------------//
                // Clean up this connection$ subscription
                //---------------------------------------------------------//
                setTimeout(() => {
                    if (this.subscriptions['pubsub'][subscription_uuid]) {
                        this.subscriptions['pubsub'][subscription_uuid].unsubscribe();
                    }
                }, 500);

                //---------------------------------------------------------//
                // Subscribe
                //---------------------------------------------------------//

                this.websocket.get$('/' + model).subscribe(res => {
                    res.error ? subject.error(res) : subject.next(res);
                    res.error && subject.complete();
                });

                //---------------------------------------------------------//
                // Handle broadcast messages from the socket
                //---------------------------------------------------------//

                this.websocket.on$(model).subscribe(res => {
                    res.error ? subject.error(res) : subject.next(res);
                    res.error && subject.complete();
                });

            }});

        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

    //---------------------------------------------------------------------//
    // Create a model
    //---------------------------------------------------------------------//

    get$(model: string, data?: any) {

        let subject = new Subject();

        //-----------------------------------------------------------------//
        // Observable will only post if there is a connected socket &
        // if there is not will wait for connection$ update
        //-----------------------------------------------------------------//
        // Note: it is possible for these observables will never resolve
        //-----------------------------------------------------------------//

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            //---------------------------------------------------------//
            // CAUTION: If this websocket connection is not unsubsribed
            // from this websocket request will run every time the
            // socket connects -- this can result in progressive duplicate
            // request -- not safe for setters, memory leak for getters
            //---------------------------------------------------------//

            let subscription_uuid = uuid();

            this.subscriptions['get'][subscription_uuid] = this.websocket.connection$.subscribe(response => { if(response.connected) {
                
                //---------------------------------------------------------//
                // Clean up this connection$ subscription
                //---------------------------------------------------------//
                setTimeout(() => {
                    if (this.subscriptions['get'][subscription_uuid]) {
                        this.subscriptions['get'][subscription_uuid].unsubscribe();
                    }
                }, 500);

                //---------------------------------------------------------//
                // Get
                //---------------------------------------------------------//

                this.websocket.get$(`/${model}`, data).subscribe(res => {
                    res.error ? subject.error(res) : subject.next(res);
                    subject.complete();
                });

            }});
        
        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

    //---------------------------------------------------------------------//
    // Create a model
    //---------------------------------------------------------------------//

    create$(model: string, data?: any) {
        
        let subject = new Subject();

        //-----------------------------------------------------------------//
        // Observable will only post if there is a connected socket &
        // if there is not will wait for connection$ update
        //-----------------------------------------------------------------//
        // Note: it is possible for these observables will never resolve
        //-----------------------------------------------------------------//

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {
        
            //---------------------------------------------------------//
            // CAUTION: If this websocket connection is not unsubsribed
            // from this websocket request will run every time the
            // socket connects -- this can result in progressive duplicate
            // request -- not safe for setters, memory leak for getters
            //---------------------------------------------------------//

            let subscription_uuid = uuid();

            this.subscriptions['create'][subscription_uuid] = this.websocket.connection$.subscribe(response => { if(response.connected) {
                
                //---------------------------------------------------------//
                // Clean up this connection$ subscription
                //---------------------------------------------------------//
                setTimeout(() => {
                    if (this.subscriptions['create'][subscription_uuid]) {
                        this.subscriptions['create'][subscription_uuid].unsubscribe();
                    }
                }, 500);
        
                //---------------------------------------------------------//
                // Post
                //---------------------------------------------------------//

                this.websocket.post$(`/${model}`, data).subscribe(res => {
                    res.error ? subject.error(res) : subject.next(res);
                    subject.complete();
                });

            }});
        
        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

    update$(model: string, id: string, data: any) {

        let subject = new Subject();

        //-----------------------------------------------------------------//
        // Observable will only post if there is a connected socket &
        // if there is not will wait for connection$ update
        //-----------------------------------------------------------------//
        // Note: it is possible for these observables will never resolve
        //-----------------------------------------------------------------//

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            //---------------------------------------------------------//
            // CAUTION: If this websocket connection is not unsubsribed
            // from this websocket request will run every time the
            // socket connects -- this can result in progressive duplicate
            // request -- not safe for setters, memory leak for getters
            //---------------------------------------------------------//

            let subscription_uuid = uuid();

            this.subscriptions['update'][subscription_uuid] = this.websocket.connection$.subscribe(response => { if(response.connected) {
                
                //---------------------------------------------------------//
                // Clean up this connection$ subscription
                //---------------------------------------------------------//
                setTimeout(() => {
                    if (this.subscriptions['update'][subscription_uuid]) {
                        this.subscriptions['update'][subscription_uuid].unsubscribe();
                    }
                }, 500);
                    
                //---------------------------------------------------------//
                // Put
                //---------------------------------------------------------//

                this.websocket.patch$(`/${model}/${id}`, data).subscribe(res => {
                    res.error ? subject.error(res) : subject.next(res);
                    subject.complete();
                });

            }});
        
        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

    updateMany$(model: string, data: any[]) {

        const subject = new Subject();

        //-----------------------------------------------------------------//
        // Observable will only post if there is a connected socket &
        // if there is not will wait for connection$ update
        //-----------------------------------------------------------------//
        // Note: it is possible for these observables will never resolve
        //-----------------------------------------------------------------//

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            //---------------------------------------------------------//
            // CAUTION: If this websocket connection is not unsubsribed
            // from this websocket request will run every time the
            // socket connects -- this can result in progressive duplicate
            // request -- not safe for setters, memory leak for getters
            //---------------------------------------------------------//

            const subscription_uuid = uuid();

            this.subscriptions['update'][subscription_uuid] = this.websocket.connection$.subscribe(response => { if(response.connected) {
                
                //---------------------------------------------------------//
                // Clean up this connection$ subscription
                //---------------------------------------------------------//
                setTimeout(() => {
                    if (this.subscriptions['update'][subscription_uuid]) {
                        this.subscriptions['update'][subscription_uuid].unsubscribe();
                    }
                }, 500);
                    
                //---------------------------------------------------------//
                // Put
                //---------------------------------------------------------//

                this.websocket.patch$(`/${model}`, data).subscribe(res => {
                    res.error ? subject.error(res) : subject.next(res);
                    subject.complete();
                });

            }});
        
        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

    delete$(model: string, id: string) {

        let subject = new Subject();

        //-----------------------------------------------------------------//
        // Observable will only post if there is a connected socket &
        // if there is not will wait for connection$ update
        //-----------------------------------------------------------------//
        // Note: it is possible for these observables will never resolve
        //-----------------------------------------------------------------//

        //=== START ZONE ==================================================//
        this.zone.runOutsideAngular(() => {

            //---------------------------------------------------------//
            // CAUTION: If this websocket connection is not unsubsribed
            // from this websocket request will run every time the
            // socket connects -- this can result in progressive duplicate
            // request -- not safe for setters, memory leak for getters
            //---------------------------------------------------------//

            let subscription_uuid = uuid();

            this.subscriptions['delete'][subscription_uuid] = this.websocket.connection$.subscribe(response => { if(response.connected) {
                
                //---------------------------------------------------------//
                // Clean up this connection$ subscription
                //---------------------------------------------------------//
                setTimeout(() => {
                    if (this.subscriptions['delete'][subscription_uuid]) {
                        this.subscriptions['delete'][subscription_uuid].unsubscribe();
                    }
                }, 500);

                //---------------------------------------------------------//
                // Put
                //---------------------------------------------------------//

                this.websocket.delete$(`/${model}/${id}`).subscribe(res => {
                    res.error ? subject.error(res) : subject.next(res);
                    subject.complete();
                });

            }});

        });
        //=== END ZONE ===================================================//

        return subject.asObservable();

    }

}