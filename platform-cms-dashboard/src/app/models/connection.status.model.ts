export class ConnectionStatus {
    constructor(
        public connected: boolean = false,
        public status: string = '. . .',
    ) { }
}