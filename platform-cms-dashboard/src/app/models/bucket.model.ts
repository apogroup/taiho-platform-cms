export class Bucket {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Key: string = '',
        public Title?: string,
        public Route?: string,
        public Description?: string,
        public Sidebar?: object[],
        public ExportTitle?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Export?: any
    ) { }
}