export class GlossaryTerm {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Disabled: boolean = false,
        public Term: string = '',
        public Definition: string = '',

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
    ) { }
}