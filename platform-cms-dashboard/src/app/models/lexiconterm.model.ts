export class LexiconTerm {
    constructor(
        
        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Disabled: boolean = false,
        public Term: string = '',
        public Example: string = '',
        public Rationale: string = '',

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
    ) { }
}