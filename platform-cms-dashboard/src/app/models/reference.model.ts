export class Reference {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public UniqueID: number = 0,
        
        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public MasterVersion: any = null,
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
        public Versions: Array<any> = [], // PATCH: always delete or server will diassociate objectid
        public Contents: Array<any> = [], // PATCH: always delete or server will diassociate objectid
        public DocumentVersions: Array<any> = [], // PATCH: always delete or server will diassociate objectid

    ) { }
}