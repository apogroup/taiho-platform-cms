export class Subtag {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,
        
        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Disabled: boolean = false,
        public Subtag: string = '',

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public Tag: any = null,
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
        public ReferenceVersions: Array<any> = [], // PATCH: always delete or server will diassociate objectid

    ) { }
}