export class DocumentVersion {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public VersionId: string = '',
        public Title: string = '',
        public Filename: string = '',
        public FilePath: Object = { key: null, eTag: null, lastModified: null, size: null },
        public SourceFilePath: Object = { key: null, eTag: null, lastModified: null, size: null },
        public ThumbnailPath: Object = { key: null, eTag: null, lastModified: null, size: null },
        public Disabled: boolean = false,
        public Cache: boolean = false,

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
        public Document: any = null,
        public References: Array<any> = [], // PATCH: always delete or server will diassociate objectid


    ) { }   
}