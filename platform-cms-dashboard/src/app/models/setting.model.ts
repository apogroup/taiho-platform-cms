export class Setting {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Key: string = '',
        public Name: string = '',
        public Value: any = '',
        public Type: string = 'text'

    ) { }
}