export class Congress {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Event: string = '',
        public Location: string = '',
        public Dates: string = '',
        public StartDate: Date = null,
        public EndDate: Date = null,
        public Specialty?: string,
        public Attendance?: string,
        public Attendees?: string,
        public Image: Object = { key: null, eTag: null, lastModified: null, size: null },

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
        // public Documents: Array<any> = [],
        public Links: Array<any> = [],

    ) { }
}