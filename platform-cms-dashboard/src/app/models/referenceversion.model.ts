export class ReferenceVersion {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public UniqueID: number = null,
        public VersionId: string = '',
        public Citation: string = '',
        public ShortCitation: string = '',
        public DisplayName: string = '',
        public Title: string = '',
        public Filename: string = '',
        public FilePath: Object = { key: null, eTag: null, lastModified: null, size: null },
        public AnnotatedFilePath: Object = { key: null, eTag: null, lastModified: null, size: null },
        public SourceFilePath: Object = { key: null, eTag: null, lastModified: null, size: null },
        public ThumbnailPath: Object = { key: null, eTag: null, lastModified: null, size: null },
        public Annotated: boolean = false,
        public Disabled: boolean = false,
        public Cache: boolean = false,
        
        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        // public FilePath: any = null,
        // public AnnotatedFilePath: any = null,
        // public ThumbnailPath: any = null,
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
        public Reference: any = null,
        public Tags: any = null,
        public Subtags: any = null,

    ) { }
}