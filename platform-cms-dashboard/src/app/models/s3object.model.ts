export class S3Object {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public key?: string,
        public eTag?: string,
        public FileIndex?: string,
        public lastModified?: string,
        public size?: number,

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public Contents: Array<any> = [], // PATCH: always delete or server will diassociate objectid
        
    ) { }
}