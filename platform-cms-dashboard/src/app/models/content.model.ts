import { Status } from 'app/types/status.type';

export class Content {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Status: Status = 'enabled',
        public Descriptor: string = '',
        public Route: string = '',
        public Label: string = '',
        public Type: string = '',
        public Title: string = '',
        public Body: string = '',
        public Footnotes: string = '',
        public Abbreviations: string = '',
        public DocumentsOrder: Array<any> = [],
        public Order?: number,

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
        public Documents: Array<any> = [], // PATCH: always delete or server will diassociate objectid
        public References: Array<any> = [], // PATCH: always delete or server will diassociate objectid
        public Parents: any = null,
        public Children: any = null,
        public Figures: any = null,

    ) { }
}
