export class User {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,
        
        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Connected: boolean = false,
        public Disabled: boolean = false,
        public Email: string = '',
        public Username: string = '',
        public FirstName: string = 'None',
        public LastName: string = '',
        public Groups: string = '',

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        
    ) { }
}