export class Document {
    constructor(

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        
        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public MasterVersion: any = null,
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
        public Versions: Array<any> = [], // PATCH: always delete or server will diassociate objectid
        public Contents: Array<any> = [], // PATCH: always delete or server will diassociate objectid

    ) { }
}