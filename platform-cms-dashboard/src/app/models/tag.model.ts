export class Tag {
    constructor(
        
        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Disabled: boolean = false,
        public Tag: string = '',

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
        public Subtags: Array<any> = [],
        public ReferenceVersions: Array<any> = [], // PATCH: always delete or server will diassociate objectid

    ) { }
}