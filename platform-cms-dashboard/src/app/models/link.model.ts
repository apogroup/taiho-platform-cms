export class Link {
    constructor (

        //-----------------------------------------------------------------------
        // Default
        //-----------------------------------------------------------------------
        public createdAt?: Date,
        public updatedAt?: Date,
        public id?: string,

        //-----------------------------------------------------------------------
        // Primitives
        //-----------------------------------------------------------------------
        public Title: string = '',
        public Tagline: string = '',
        public Disabled: boolean = false,
        public Href: string = '',
        public Button: string = '',

        //-----------------------------------------------------------------------
        // Associations
        //-----------------------------------------------------------------------
        public CreatedBy: any = null,
        public LastUpdatedBy: any = null,
        public Congress: any = null

    ) { }
}