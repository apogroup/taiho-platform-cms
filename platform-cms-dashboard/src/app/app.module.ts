import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatCheckboxModule, MatNativeDateModule } from '@angular/material';
import { MatButtonModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

import { DragDropModule } from '@angular/cdk/drag-drop';

import { NgPipesModule } from 'ngx-pipes';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';

import { TagInputModule } from 'ngx-chips';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { FroalaEditorPluginsModule } from 'app/froala-editor-plugins/froala-editor-plugins.module';

import { AppRoutingModule } from 'app/routing/app-routing.module';
import { AppComponent } from 'app/app.component';

import { ConnectionComponent } from 'app/components/connection/connection.component';
import { UserManagementComponent } from 'app/components/dashboard/boards/user-management/user-management.component';
import { TagsComponent } from 'app/components/dashboard/boards/tags/tags.component';
import { DocumentsComponent } from 'app/components/dashboard/boards/documents/documents.component';
import { LexiconComponent } from 'app/components/dashboard/boards/lexicon/lexicon.component';
import { GlossaryComponent } from 'app/components/dashboard/boards/glossary/glossary.component';
import { AbbreviationComponent } from 'app/components/dashboard/boards/abbreviation/abbreviation.component';
import { FilesComponent } from 'app/components/dashboard/boards/files/files.component';
import { S3UploadComponent } from 'app/components/s3-upload/s3-upload.component';
import { ReferencesComponent } from 'app/components/dashboard/boards/references/references.component';
import { ConfigurationComponent } from 'app/components/dashboard/boards/configuration/configuration.component';
import { ContentComponent } from 'app/components/dashboard/boards/content/content.component';
import { ContentBlockComponent } from 'app/components/dashboard/boards/content/content-block/content-block.component';
import { CongressesComponent } from './components/dashboard/boards/congresses/congresses.component';
import { BucketConfigurationComponent } from 'app/components/dashboard/boards/configuration/bucket-configuration/bucket-configuration.component';
import { SettingsConfigurationComponent } from 'app/components/dashboard/boards/configuration/settings-configuration/settings-configuration.component';

import { LoggerService } from 'app/services/logger/logger.service';
import { GatewayS3Service } from 'app/services/gateway/gateway-s3.service';
import { WebsocketService } from 'app/services/websocket/websocket.service';
import { ModelService } from 'app/services/model/model.service';
import { UserService } from 'app/services/dashboard/user.service';
import { TagService } from 'app/services/dashboard/tag.service';
import { DocumentService } from 'app/services/dashboard/document.service';
import { LexiconService } from 'app/services/dashboard/lexicon.service';
import { GlossaryService } from 'app/services/dashboard/glossary.service';
import { AbbreviationService } from 'app/services/dashboard/abbreviation.service';
import { ReferenceService } from 'app/services/dashboard/reference.service';
import { ContentService } from 'app/services/dashboard/content.service';
import { BucketService } from 'app/services/dashboard/bucket.service';
import { SettingService } from 'app/services/dashboard/setting.service';
import { S3ObjectService } from 'app/services/dashboard/s3object.service';
import { CongressService } from './services/dashboard/congress.service';
import { LinkService } from './services/dashboard/link.service';

import { HTMLEditorDialog } from 'app/components/htmleditor-dialog/htmleditor-dialog.component';
import { TagEditDialog } from 'app/components/dashboard/boards/tags/tag-edit-dialog/tag-edit-dialog.component';
import { DocumentVersionDialog } from 'app/components/dashboard/boards/documents/document-version-dialog/document-version-dialog.component';
import { DocumentMultiSelectDialog } from 'app/components/document-multiselect-dialog/document-multiselect-dialog.component';
import { FileSelectDialog } from 'app/components/file-select-dialog/file-select-dialog.component';
import { ReferenceVersionDialog } from 'app/components/dashboard/boards/references/reference-version-dialog/reference-version-dialog.component';
import { ContentBlockDialog } from 'app/components/dashboard/boards/content/content-block-dialog/content-block-dialog.component';
import { ContentBlockEditDialog } from 'app/components/dashboard/boards/content/content-block-edit-dialog/content-block-edit-dialog.component';
import { ReferenceSelectDialog } from 'app/components/reference-select-dialog/reference-select-dialog.component';
import { ReferenceMultiSelectDialog } from 'app/components/reference-multiselect-dialog/reference-multiselect-dialog.component';
import { FigureSelectDialog } from 'app/components/figure-select-dialog/figure-select-dialog.component';
import { S3UploadDialog } from 'app/components/s3-upload-dialog/s3-upload-dialog.component';
import { LinkDialog } from './components/link-dialog/link-dialog.component';

import { S3KeyPipe } from 'app/pipes/s3-key.pipe';
import { S3KeyRmHashPipe } from 'app/pipes/s3-key-rm-hash.pipe';
import { OrderByIdArrayPipe } from 'app/pipes/order-by-id-array.pipe';
import { MarkAsSafePipe } from 'app/pipes/mark-as-safe.pipe';

const inputFileConfig: InputFileConfig = {};

@NgModule({
  declarations: [

    //-------------------------------------------------------------------
    // Components
    //-------------------------------------------------------------------
    AppComponent,
    ConnectionComponent,
    UserManagementComponent,
    TagsComponent,
    DocumentsComponent,
    LexiconComponent,
    GlossaryComponent,
    AbbreviationComponent,
    FilesComponent,
    S3UploadComponent,
    ReferencesComponent,
    ContentComponent,
    ContentBlockComponent,
    CongressesComponent,
    ConfigurationComponent,
    BucketConfigurationComponent,
    SettingsConfigurationComponent,

    //-------------------------------------------------------------------
    // Material Dialog Components
    //-------------------------------------------------------------------
    HTMLEditorDialog,
    TagEditDialog,
    DocumentVersionDialog,
    DocumentMultiSelectDialog,
    FileSelectDialog,
    ReferenceVersionDialog,
    ContentBlockDialog,
    ContentBlockEditDialog,
    ReferenceSelectDialog,
    ReferenceMultiSelectDialog,
    FigureSelectDialog,
    S3UploadDialog,
    LinkDialog,

    //-------------------------------------------------------------------
    // Pipes
    //-------------------------------------------------------------------
    S3KeyPipe,
    S3KeyRmHashPipe,
    OrderByIdArrayPipe,
    MarkAsSafePipe,

  ],
  entryComponents: [

    //-------------------------------------------------------------------
    // Dialogs
    //-------------------------------------------------------------------
    HTMLEditorDialog,
    TagEditDialog,
    DocumentVersionDialog,
    DocumentMultiSelectDialog,
    FileSelectDialog,
    ReferenceVersionDialog,
    ContentBlockDialog,
    ContentBlockEditDialog,
    ReferenceSelectDialog,
    ReferenceMultiSelectDialog,
    FigureSelectDialog,
    S3UploadDialog,
    LinkDialog,

  ],
  imports: [

    //-------------------------------------------------------------------
    // Dependencies
    //-------------------------------------------------------------------
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,

    //-------------------------------------------------------------------
    // NGX
    //-------------------------------------------------------------------

    NgPipesModule,
    TagInputModule,
    InputFileModule.forRoot(inputFileConfig),
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
    FroalaEditorPluginsModule,

    //-------------------------------------------------------------------
    // Angular Material
    //-------------------------------------------------------------------
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    DragDropModule,

    //-------------------------------------------------------------------
    // Application Modules
    //-------------------------------------------------------------------
    AppRoutingModule,

  ],
  providers: [

    //-------------------------------------------------------------------
    // Application Services
    //-------------------------------------------------------------------
    LoggerService,
    GatewayS3Service,
    WebsocketService,
    ModelService,
    UserService,
    TagService,
    DocumentService,
    LexiconService,
    GlossaryService,
    AbbreviationService,
    ReferenceService,
    S3ObjectService,
    ContentService,
    CongressService,
    LinkService,
    BucketService,
    SettingService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
