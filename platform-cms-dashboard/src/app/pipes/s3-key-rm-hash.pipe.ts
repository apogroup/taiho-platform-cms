import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'S3keyRmHash' })
export class S3KeyRmHashPipe implements PipeTransform {
  transform(key: string): string {
    let html: string = '';

    if (!key) {
      return html;
    }

    let [directory, filenameWithHash] = key.split('/');
    let [filename] = filenameWithHash.split('#');

    html = [
      `<span class="s3-key-directory-discrete">${directory}</span>`,
      `<span class="s3-key-filename">${filename}</span>`,
    ].join('');

    return html;
  }
}