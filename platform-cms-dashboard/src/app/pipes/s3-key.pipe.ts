import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'S3key'})
export class S3KeyPipe implements PipeTransform {
  transform(key: string): string {
    let html: string = '';

    if(!key) {
      return html;
    }
    
    let [directory, filenameWithHash] = key.split('/');
    let [filename, hash] = filenameWithHash.split('#');

    html = [
        `<span class="s3-key-directory">${directory}</span>`,
        `<span class="s3-key-filename">${filename}</span>`,
        `<span class="s3-key-hash">${hash}</span>`,
    ].join('');

    return html;
  }
}