import { Pipe, PipeTransform } from "@angular/core";
import { sortBy } from 'lodash';

@Pipe({
    name: 'orderByIdArray'
})
export class OrderByIdArrayPipe implements PipeTransform {
    transform(array: Array<any>, orderArray: Array<any>): Array<any> {
        let sortObj = [...orderArray].reduce((acc, value, index) => {
            acc[value] = index;
            return acc;
        }, {});

        return sortBy([...array], (obj)=>{
            return sortObj[obj.id];
        });
    }
}