import { NgModule, NgZone } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDialog } from '@angular/material';
import { ReferenceSelectDialog } from 'app/components/reference-select-dialog/reference-select-dialog.component';

declare var $: any;

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ReferencePluginModule {

  /////////////////////////////////////////////////////////////////////////
  // Dialog
  /////////////////////////////////////////////////////////////////////////

  private referenceSelectDialog;

  constructor(
    public dialog: MatDialog,
    private zone: NgZone
  ) {
    $.FroalaEditor.PLUGINS.reference = (editor) => {

      const _init = () => {

        // Edit on keyup.
        editor.events.on('keyup', function (e) {
          if (e.which != $.FroalaEditor.KEYCODE.ESC) {
            _edit(e);
          }
        });

        // editor.events.on('window.mouseup', _edit);
        editor.events.on('window.mouseup', (e) => {
          if ($(e.target).is('sup[type="reference"]')) {
            _edit(e);
          }
        })

        if (editor.helpers.isMobile()) {
          editor.events.$on(editor.$doc, 'selectionchange', _edit);
        }

        // _initInsertPopup(true);

        // Init on link.
        if (editor.el.tagName == 'SUP' && editor.el.getAttribute('type') == "reference") {
          editor.$el.addClass('fr-view');
        }

        // Hit ESC when focus is in link edit popup.
        editor.events.on('toolbar.esc', function () {
          if (editor.popups.isVisible('reference.edit')) {
            editor.events.disableBlur();
            editor.events.focus();

            return false;
          }
        }, true);
      };

      const get = () => {
        if (editor.$wp) {
          var c_el = editor.selection.ranges(0).commonAncestorContainer;

          try {
            if (c_el && ((c_el.contains && c_el.contains(editor.el)) || !editor.el.contains(c_el) || editor.el == c_el)) c_el = null;
          }
          catch (ex) {
            c_el = null;
          }

          if (c_el && c_el.tagName === 'SUP' && c_el.getAttribute('type') == "reference") return c_el;
          var s_el = editor.selection.element();
          var e_el = editor.selection.endElement();

          if (s_el.tagName != 'SUP' && !editor.node.isElement(s_el)) {
            s_el = $(s_el).parentsUntil(editor.$el, 'sup:first').get(0);
          }

          if (e_el.tagName != 'SUP' && !editor.node.isElement(e_el)) {
            e_el = $(e_el).parentsUntil(editor.$el, 'sup:first').get(0);
          }

          try {
            if (e_el && ((e_el.contains && e_el.contains(editor.el)) || !editor.el.contains(e_el) || editor.el == e_el)) e_el = null;
          }
          catch (ex) {
            e_el = null;
          }

          try {
            if (s_el && ((s_el.contains && s_el.contains(editor.el)) || !editor.el.contains(s_el) || editor.el == s_el)) s_el = null;
          }
          catch (ex) {
            s_el = null;
          }

          if (e_el && e_el == s_el && e_el.tagName == 'SUP' && e_el.getAttribute('type') == "reference") {

            // We do not clicking at the end / input of links because in IE the selection is changing shortly after mouseup.
            // https://jsfiddle.net/jredzam3/
            if ((editor.browser.msie || editor.helpers.isMobile()) && (editor.selection.info(s_el).atEnd || editor.selection.info(s_el).atStart)) {
              return null;
            }

            return s_el;
          }

          return null;
        }
        else if (editor.el.tagName == 'SUP' && editor.el.getAttribute('type') == "reference") {
          return editor.el;
        }
        else {
          return null;
        }
      };

      const _edit = (e) => {
        // console.log('_EDIT, e', e);

        if (editor.core.hasFocus()) {
          _hideEditPopup();

          // Do not show edit popup when ALT is hit.
          if (e && e.type === 'keyup' && (e.altKey || e.which == $.FroalaEditor.KEYCODE.ALT)) return true;

          setTimeout(function () {

            // No event passed.
            // Event passed and (left click or other event type).
            if (!e || (e && (e.which == 1 || e.type != 'mouseup'))) {
              var sup = get();

              if (sup) {
                if (e) {
                  e.stopPropagation();
                }
                _showEditPopup(sup);
              }
            }

            if (e && e.target && (e.target.nodeName == 'SUP' && e.target.getAttribute('type') == "reference")) {
              e.stopPropagation();
              editor.selection.setAtEnd(e.target);
              editor.selection.restore();

              var sup = get();

              if (sup) {
                if (e) {
                  e.stopPropagation();
                }
                _showEditPopup(sup);
              }
            }

          }, editor.helpers.isIOS() ? 100 : 0);
        }
      };

      const _hideEditPopup = () => {
        editor.popups.hide('reference.edit');
      };

      const _showEditPopup = (sup) => {
        var $popup = editor.popups.get('reference.edit');

        if (!$popup) $popup = _initEditPopup();

        var $sup = $(sup);

        if (!editor.popups.isVisible('reference.edit')) {
          editor.popups.refresh('reference.edit');
        }
        editor.popups.setContainer('reference.edit', editor.$sc);
        var left = $sup.offset().left + $(sup).outerWidth() / 2;
        var top = $sup.offset().top + $sup.outerHeight();
        editor.popups.show('reference.edit', left, top, $sup.outerHeight());
      };

      const _initEditPopup = () => {
        // Buttons.
        var reference_buttons = '';

        if (editor.opts.referenceEditButtons.length >= 1) {
          reference_buttons = '<div class="fr-buttons">' + editor.button.buildList(editor.opts.referenceEditButtons) + '</div>';
        }

        var template = {
          buttons: reference_buttons
        };

        // Set the template in the popup.
        var $popup = editor.popups.create('reference.edit', template);

        if (editor.$wp) {
          editor.events.$on(editor.$wp, 'scroll.reference-edit', function () {
            if (get() && editor.popups.isVisible('reference.edit')) {
              _showEditPopup(get());
            }
          });
        }

        return $popup;
      };

      //-------------------------------------------------------------------
      // Froala Custom Button f(x)s
      //-------------------------------------------------------------------

      const remove = () => {
        var reference = get();

        if (editor.events.trigger('reference.beforeRemove', [reference]) === false) return false;

        if (reference) {
          var prev = $(reference).prev()[0];
          if (prev && prev.nodeName == 'SUP' && prev.innerHTML == ", ") {
            prev.remove();
          }
          editor.selection.save();
          $(reference).remove();
          editor.selection.restore();
          _hideEditPopup();
        }
      };

      const select = () => {
        var s_el = editor.selection.element();
        var e_el = editor.selection.endElement();

        // console.log('sel.el', s_el);
        // console.log('end.el', e_el);

        //-------------------------------------------------------------------
        // Zone otherwise will conflict with existing dialog
        //-------------------------------------------------------------------
        this.zone.run(() => {

          this.referenceSelectDialog = this.dialog.open(ReferenceSelectDialog, {
            width: '60%',
            data: {
              selectedText: editor.selection.text(),
            }
          })

          this.referenceSelectDialog.afterClosed().subscribe(data => {
            if (data) {
              console.log('select(): Injecting <sup> with reference data: ', data);
              let comma = '<sup>, </sup>';
              let markup = `<sup type="reference" data-reference="${data.Reference.id}" data-reference-uid="${data.UniqueID}" contenteditable="false" class="fr-deletable">${data.UniqueID}</sup>`;
              // markup = comma + markup;

              editor.selection.save();
              editor.html.insert(markup);
              editor.selection.restore();

              setTimeout(function () {
                if (editor.format.is('sup')) {
                  editor.format.toggle('sup');
                }
              }, 100);

            }
          });

        });
      };

      const replace = () => {
        var reference = get();
        if (editor.events.trigger('reference.beforeRemove', [reference]) === false) return false;
        _hideEditPopup();

        //-------------------------------------------------------------------
        // Zone otherwise will conflict with existing dialog
        //-------------------------------------------------------------------
        this.zone.run(() => {

          this.referenceSelectDialog = this.dialog.open(ReferenceSelectDialog, {
            width: '60%',
            data: {
              selectedText: editor.selection.text(),
            }
          })

          this.referenceSelectDialog.afterClosed().subscribe(data => {
            if (data) {
              console.log('replace(): Injecting <sup> with reference data: ', data);
              var markup = `<sup type="reference" data-reference="${data.Reference.id}" data-reference-uid="${data.UniqueID}" contenteditable="false" class="fr-deletable">${data.UniqueID}</sup>`;

              if (reference) {
                editor.selection.save();
                $(reference).replaceWith(markup);
                editor.selection.restore();
              }

              setTimeout(function () {
                if (editor.format.is('sup')) {
                  editor.format.toggle('sup');
                }
              }, 100);
            }
          });

        });
      };

      const append = () => {
        var reference = get();
        if (editor.events.trigger('reference.beforeRemove', [reference]) === false) return false;
        _hideEditPopup();

        //-------------------------------------------------------------------
        // Zone otherwise will conflict with existing dialog
        //-------------------------------------------------------------------
        this.zone.run(() => {

          this.referenceSelectDialog = this.dialog.open(ReferenceSelectDialog, {
            width: '60%',
            data: {
              selectedText: editor.selection.text(),
            }
          })

          this.referenceSelectDialog.afterClosed().subscribe(data => {
            if (data) {
              console.log('append(): Injecting <sup> with reference data: ', data);
              let comma = '<sup notreference>, </sup>';
              let markup = `<sup type="reference" data-reference="${data.Reference.id}" data-reference-uid="${data.UniqueID}" contenteditable="false" class="fr-deletable">${data.UniqueID}</sup>`;
              markup = comma + markup;

              if (reference) {
                // editor.selection.save();
                markup = reference.outerHTML + markup;
                $(reference).replaceWith(markup);
                editor.selection.setAtEnd(reference);
                // editor.selection.restore();
              }

              setTimeout(function () {
                if (editor.format.is('sup')) {
                  editor.format.toggle('sup');
                }
              }, 100);
            }
          });

        });
      };

      return {
        _init,
        get,
        select,
        replace,
        remove,
        append,
      };
    }

    $.FroalaEditor.DefineIcon('referenceEdit', { NAME: 'pencil' });
    $.FroalaEditor.RegisterCommand('referenceEdit', {
      title: 'Edit Reference',
      undo: false,
      refreshAfterCallback: false,
      callback: function () {
        this.reference.replace();
      },
      refresh: function ($btn) {
      },
      plugin: 'reference'
    });

    $.FroalaEditor.DefineIcon('referenceRemove', { NAME: 'trash' });
    $.FroalaEditor.RegisterCommand('referenceRemove', {
      title: 'Remove Reference',
      undo: false,
      refreshAfterCallback: false,
      callback: function () {
        this.reference.remove();
      },
      refresh: function ($btn) {
      },
      plugin: 'reference'
    });

    $.FroalaEditor.DefineIcon('referenceAppend', { NAME: 'plus' });
    $.FroalaEditor.RegisterCommand('referenceAppend', {
      title: 'Add Reference to Series',
      undo: false,
      refreshAfterCallback: false,
      callback: function () {
        this.reference.append();
      },
      refresh: function ($btn) {
      },
      plugin: 'reference'
    });

    $.FroalaEditor.DefineIcon('referenceSelect', { NAME: 'tag' });
    $.FroalaEditor.RegisterCommand('referenceSelect', {
      title: 'Reference',
      focus: true,
      undo: true,
      refreshAfterCallback: false,
      callback: function () {
        this.reference.select();
      },
      plugin: 'reference'
    });
  }
}
