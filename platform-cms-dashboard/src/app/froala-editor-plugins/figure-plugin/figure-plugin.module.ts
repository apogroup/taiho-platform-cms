import { NgModule, NgZone } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDialog } from '@angular/material';
import { FigureSelectDialog } from 'app/components/figure-select-dialog/figure-select-dialog.component';

declare var $: any;

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class FigurePluginModule {

  /////////////////////////////////////////////////////////////////////////
  // Dialog
  /////////////////////////////////////////////////////////////////////////

  private figureSelectDialog;

  constructor(
    public dialog: MatDialog,
    private zone: NgZone
  ) {
    $.FroalaEditor.PLUGINS.figure = (editor) => {

      let activeFigure;

      const _init = () => {

        // Edit on keyup.
        // editor.events.on('keyup', function (e) {
        // 	if (e.which != $.FroalaEditor.KEYCODE.ESC) {
        // 		_edit(e);
        // 	}
        // });

        // editor.events.on('window.mouseup', _edit);
        editor.events.on('window.mouseup', (e) => {

          // Do not show edit popup when ALT is hit.
          const isAltPressed = e && e.type === 'keyup' && (e.altKey || e.which == $.FroalaEditor.KEYCODE.ALT);

          const { target } = event;
          if (!isAltPressed && editor.core.hasFocus() && $(target).is('.figure-preview')) {
            e.stopPropagation();

            editor.selection.setAfter(target);
            editor.selection.restore();

            _edit(target);
          }
        });

        // if (editor.helpers.isMobile()) {
        // 	editor.events.$on(editor.$doc, 'selectionchange', _edit);
        // }

        // _initInsertPopup(true);

        // Init on link.
        if ($(editor.$el).is('.figure-preview')) {
          editor.$el.addClass('fr-view');
        }

        // Hit ESC when focus is in link edit popup.
        editor.events.on('toolbar.esc', function () {
          if (editor.popups.isVisible('figure.edit')) {
            editor.events.disableBlur();
            editor.events.focus();

            return false;
          }
        }, true);
      };

      const _edit = (figure) => {
        // console.log('EDIT', figure);

        _hideEditPopup();

        setTimeout(function () {

          _showEditPopup(figure);

        }, editor.helpers.isIOS() ? 100 : 0);

      };

      const _hideEditPopup = () => {
        editor.popups.hide('figure.edit');
      };

      const _showEditPopup = (figure) => {
        if (!editor.popups.get('figure.edit')) {
          _initEditPopup();
        }

        if (!editor.popups.isVisible('figure.edit')) {
          editor.popups.refresh('figure.edit');
        }

        activeFigure = $(figure);
        const height = activeFigure.outerHeight();
        const width = activeFigure.outerWidth();
        const offset = activeFigure.offset();

        const left = offset.left + width / 2;
        const top = offset.top + height;

        editor.popups.setContainer('figure.edit', editor.$sc);
        editor.popups.show('figure.edit', left, top, height);
      };

      const _initEditPopup = () => {
        // Buttons.
        const buttons = editor.button.buildList(editor.opts.figureEditButtons);
        const figureButtons = `<div class="fr-buttons">${buttons}</div>`;

        const template = {
          buttons: figureButtons
        };

        // Set the template in the popup.
        const $popup = editor.popups.create('figure.edit', template);

        // if (editor.$wp) {
        // 	editor.events.$on(editor.$wp, 'scroll.figure-edit', function (e) {
        // 		console.log(e);
        // 		if (activeFigure && editor.popups.isVisible('figure.edit')) {
        // 			_showEditPopup(activeFigure);
        // 		}
        // 	});
        // }

        return $popup;
      };

      //-------------------------------------------------------------------
      // Froala Custom Button f(x)s
      //-------------------------------------------------------------------

      const remove = () => {
        // console.log('REMOVE', activeFigure);
        if (editor.events.trigger('figure.beforeRemove', [activeFigure]) === false) {
          return false;
        }

        editor.selection.save();
        activeFigure.remove();
        editor.selection.restore();

        activeFigure = null;

        _hideEditPopup();
      };

      const replace = () => {
        console.log(activeFigure);

        if (editor.events.trigger('figure.beforeRemove', [activeFigure]) === false) return false;
        _hideEditPopup();

        //-------------------------------------------------------------------
        // Zone otherwise will conflict with existing dialog
        //-------------------------------------------------------------------
        this.zone.run(() => {

          this.figureSelectDialog = this.dialog.open(FigureSelectDialog, {
            width: '60%',
            data: {
              selectedText: editor.selection.text(),
            }
          })

          this.figureSelectDialog.afterClosed().subscribe(figure => {
            if (figure && figure.key) {

              const markup = createFigure(figure);

              if (activeFigure) {
                this.zone.run(() => {
                  editor.selection.save();
                  activeFigure.replaceWith(markup);
                  editor.selection.restore();
                });
              }

            }
          });

        });
      };

      const createFigure = ({ key, id }) => {
        const href = key.split('/').map(encodeURIComponent).join('/');
        return `
					<div class="figure-preview" data-figure="${id}" contenteditable="false">
						<img class="figure-preview-image" src="/content/${href}"/>
					</div>
				`;
      };

      const select = () => {
        //-------------------------------------------------------------------
        // Zone otherwise will conflict with existing dialog
        //-------------------------------------------------------------------
        this.zone.run(() => {

          this.figureSelectDialog = this.dialog.open(FigureSelectDialog, {
            width: '60%',
            data: {
              selectedText: editor.selection.text(),
            }
          })

          this.figureSelectDialog.afterClosed().subscribe(figure => {
            if (figure && figure.key) {

              const markup = createFigure(figure);

              this.zone.run(() => {
                editor.selection.save();
                editor.html.insert(markup);
                editor.selection.restore();

              })
            }
          });

        });
      };

      return {
        _init,
        createFigure,
        select,
        replace,
        remove
      };
    };

    $.FroalaEditor.DefineIcon('figureSelect', { NAME: 'image' });
    $.FroalaEditor.RegisterCommand('figureSelect', {
      title: 'Figure',
      focus: true,
      undo: true,
      refreshAfterCallback: false,
      callback: function () {
        this.figure.select();
      },
      plugin: 'figure'
    });

    $.FroalaEditor.DefineIcon('figureRemove', { NAME: 'trash' });
    $.FroalaEditor.RegisterCommand('figureRemove', {
      title: 'Figure',
      focus: true,
      undo: true,
      refreshAfterCallback: false,
      callback: function () {
        this.figure.remove();
      },
      plugin: 'figure'
    });

    $.FroalaEditor.DefineIcon('figureReplace', { NAME: 'pencil' });
    $.FroalaEditor.RegisterCommand('figureReplace', {
      title: 'Figure',
      focus: true,
      undo: true,
      refreshAfterCallback: false,
      callback: function () {
        this.figure.replace();
      },
      plugin: 'figure'
    });

  }

}
