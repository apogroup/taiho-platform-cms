import { NgModule } from '@angular/core';

import { ReferencePluginModule } from 'app/froala-editor-plugins/reference-plugin/reference-plugin.module';
import { FigurePluginModule } from 'app/froala-editor-plugins/figure-plugin/figure-plugin.module';

declare var $: any;

@NgModule({
  declarations: [],
  imports: [
    ReferencePluginModule,
    FigurePluginModule
  ]
})
export class FroalaEditorPluginsModule {
  constructor() {

    /////////////////////////////////////////////////////////////////////////
    // Froala Configuration
    /////////////////////////////////////////////////////////////////////////

    $.FroalaEditor.POPUP_TEMPLATES = $.extend($.FroalaEditor.POPUP_TEMPLATES, {
      'reference.edit': '[_BUTTONS_]',
      'figure.edit': '[_BUTTONS_]',
    });

    $.FroalaEditor.DEFAULTS = $.extend($.FroalaEditor.DEFAULTS, {
      referenceEditButtons: ['referenceEdit', 'referenceRemove', '|', 'referenceAppend'],
      figureEditButtons: ['figureReplace', 'figureRemove'],
    });
  }
}
