import filter from 'lodash/filter';
import { Component, OnInit, NgZone } from '@angular/core';
import { Router, Route } from '@angular/router';
import { Subscription } from 'rxjs';

import { UserService } from 'app/services/dashboard/user.service';
import { TagService } from 'app/services/dashboard/tag.service';
import { DocumentService } from 'app/services/dashboard/document.service';
import { LexiconService } from 'app/services/dashboard/lexicon.service';
import { GlossaryService } from 'app/services/dashboard/glossary.service';
import { AbbreviationService } from 'app/services/dashboard/abbreviation.service';
import { ReferenceService } from 'app/services/dashboard/reference.service';
import { S3ObjectService } from 'app/services/dashboard/s3object.service';
import { ContentService } from 'app/services/dashboard/content.service';
import { CongressService } from './services/dashboard/congress.service';
import { BucketService } from 'app/services/dashboard/bucket.service';
import { SettingService } from 'app/services/dashboard/setting.service';
import { User } from 'app/models/user.model';

import { environment } from 'environments/environment';
import { LinkService } from './services/dashboard/link.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	/////////////////////////////////////////////////////////////////////////
	// Variables
	/////////////////////////////////////////////////////////////////////////
	public sidebarRoutes: Route[];
	public title: string = environment.title;

	/////////////////////////////////////////////////////////////////////////
	// Data Subscriptions
	/////////////////////////////////////////////////////////////////////////
	private subscriptions: Subscription = new Subscription;

	public users: Array<User> = [];
	public connectedUsers: Array<User> = [];

	/////////////////////////////////////////////////////////////////////////
	// Constructor
	/////////////////////////////////////////////////////////////////////////
	constructor(
		private userService: UserService,
		private tagService: TagService,
		private documentService: DocumentService,
		private lexiconService: LexiconService,
		private glossaryService: GlossaryService,
		private abbreviationService: AbbreviationService,
		private referenceService: ReferenceService,
		private s3ObjectService: S3ObjectService,
		private contentService: ContentService,
		private congressService: CongressService,
		private linkService: LinkService,
		private bucketService: BucketService,
		private settingService: SettingService,
		private zone: NgZone,
		private router: Router,
	) {
	}

	ngOnInit() {

		//-------------------------------------------------------------------
		// Initialize sidebar navigation links
		//-------------------------------------------------------------------

		this.sidebarRoutes = this.router.config
			.filter(({ path }) => path.length);

		//-------------------------------------------------------------------
		// Subscribe to All Users
		//-------------------------------------------------------------------

		const users$ = this.userService.users$.subscribe((users: User[]) => {
			this.zone.run(() => {
				this.users = users;
				this.connectedUsers = filter(users);
			});
		});
		this.subscriptions.add(users$);


		//-------------------------------------------------------------------
		// Subscribe to Subscriptions
		//-------------------------------------------------------------------

		this.userService.subscribeToAllUsers();
		this.tagService.subscribeToTags();
		this.tagService.subscribeToSubtags();
		this.documentService.subscribeToDocuments();
		this.documentService.subscribeToDocumentVersions();
		this.lexiconService.subscribeToTerms();
		this.glossaryService.subscribeToTerms();
		this.abbreviationService.subscribeToAbbreviations();
		this.referenceService.subscribeToReferences();
		this.referenceService.subscribeToReferenceVersions();
		this.s3ObjectService.subscribeToS3Objects();
		this.contentService.subscribeToContents();
		this.congressService.subscribeToCongresses();
		this.linkService.subscribeToLinks();
		this.bucketService.subscribeToBuckets();
		this.settingService.subscribeToSettings();

		//-------------------------------------------------------------------
		// Get the Session User
		//-------------------------------------------------------------------

		this.userService.getSessionUser();

	}

	ngOnDestroy() {

		this.subscriptions.unsubscribe();

	}

	/**
	 * trackRouteByPath
	 */
	public trackRouteByPath(route: Route): string {
		return route.path;
	}
}