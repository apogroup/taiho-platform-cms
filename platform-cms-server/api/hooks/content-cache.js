/**
 * content-cache hook
 *
 * @description :: A hook definition.  Extends Sails by adding shadow routes, implicit actions, and/or initialization logic.
 * @docs        :: https://sailsjs.com/docs/concepts/extending-sails/hooks
 */

module.exports = function defineContentCacheHook(sails) {

  return {

    /**
     * Runs when this Sails app loads/lifts.
     */
    initialize: async function (done) {

      sails.after('hook:orm:loaded', () => {

        const CacheDataStore = sails.getDatastore('apiCache');

        sails.on('cache:bust', async () => {

          sails.log('Busting cache...');

          try {

            await CacheDataStore.leaseConnection(async db => {
              await db.collection('content').deleteMany()
            });

          } catch (error) {

            sails.log.error('Cache bust failed!');
            sails.log.error(error);

          }
        });

        return done();

      });


    }

  };

};
