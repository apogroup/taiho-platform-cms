/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const passport = require('passport');

const authenticate = (req, res) =>
    new Promise((resolve, reject) =>
        passport.authenticate(
            'okta-saml',
            (error, assertion) => {
                if (error) {
                    reject(new Error(error));
                } else if (!assertion) {
                    reject(new Error('User not authenticated'));
                } else {
                    resolve(assertion);
                }
            })(req, res)
    );

const login = (req, assertion) =>
    new Promise((resolve, reject) =>
        req.login(assertion, (err) => {
            if (err) {
                reject(new Error(err));
            } else {
                resolve(assertion);
            }
        })
    );

module.exports = {

    //--------------------------------
    // SAML OKTA Strategy API
    //--------------------------------
    auth: async function (req, res) {

        //--------------------------------
        // Request SAML assertion from OKTA issuer
        //--------------------------------
        let assertion;
        try {

            assertion = await authenticate(req, res);

            sails.log.info(`User authenticated: ${assertion.username}`);

            await login(req, assertion);

        } catch (error) {
            sails.log.error(error);
            return res.redirect('/login-sso');
        }

        //--------------------------------
        // Store user in database
        //--------------------------------
        try {
            const { nameID, email, firstName, lastName, groups } = assertion;

            const valuesToSet = {
                Email: email,
                FirstName: firstName,
                LastName: lastName,
                Groups: groups,
                Connected: true
            };

            const user = await User.updateOne({ Username: nameID }, valuesToSet);

            if (!user) {
                sails.log.info(`Adding user to collection: ${nameID}`);
                await User.findOrCreate({ Username: nameID }, {
                    Username: nameID,
                    ...valuesToSet
                });
            } else {
                sails.log.info(`Updated user in collection: ${nameID}`);
            }

        } catch (error) {
            sails.log.error(error);
            return res.redirect('/login-sso');
        }

        return res.redirect('/');

    },

    login: async function (req, res) {

        //--------------------------------
        // Request SAML assertion from OKTA issuer
        //--------------------------------
        let assertion;
        try {

            assertion = await authenticate(req, res);

            sails.log.info(`User authenticated: ${assertion.nameID}`);

            await login(req, assertion);

        } catch (error) {
            sails.log.error(error);
            return res.redirect('/login-sso');
        }

        return res.redirect('/');

    },


    logout: async function (req, res) {

        //--------------------------------
        // Request SAML assertion from OKTA issuer
        //--------------------------------
        try {

            const { nameID } = await authenticate(req, res);

            req.logout();

            sails.log.info(`User logged out: ${nameID}`);

            await User.update({ Username: nameID }, { Connected: false });

        } catch (error) {
            sails.log.error(error);
        }

        return res.redirect('/login-sso');

    }
};