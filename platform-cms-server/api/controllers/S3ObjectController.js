const assert = require('assert');
const { parse } = require('path');

const { indexPdf } = sails.helpers;

module.exports = {

    getS3ObjectIndex: async function (req, res) {

        try {
            const { key } = req.body;
            const  { base } = parse(key);

            const s3ObjectUrl = await GatewayService.createPresignedUrl(key, 'GET');
            const [ filename ] = base.split('#');
            const fileBuffer = await S3Service.getObject(filename, s3ObjectUrl);

            const FileIndex = await indexPdf(key, fileBuffer);
    
            const indexedS3Object = await S3Object.updateOne({ key }, { FileIndex });

            // !indexedS3Object -- more than one record matches (if such records exist), no records match
            assert(indexedS3Object, "Failed to update S3Object");

            const { id } = indexedS3Object;

            S3Object.publish([ id ], {
                id,
                verb: 'updated',
                data: indexedS3Object
            });
            return res.json(indexedS3Object);                                
        } catch(err){
            sails.log.error(err);
            return res.badRequest("Failed to index S3 Object");
        }

    }

}