/**
 * BlueprintController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const assert = require("assert").strict;

module.exports = {
  async updateMany(req, res) {
    try {
      assert(req.body instanceof Array);

      const parseBlueprintOptions =
        req.options.parseBlueprintOptions ||
        req._sails.config.blueprints.parseBlueprintOptions;

      // Set the blueprint action for parseBlueprintOptions.
      req.options.blueprintAction = "update";
      req.options.action = `${req.params.model}/update`;

      const Model = req._sails.models[req.params.model];
      const { primaryKey } = Model;

      const updatedRecords = [];
      const populatedRecords = [];
      for (const record of req.body) {
        const request = _.cloneDeep(req);
        request.body = record;

        const queryOptions = parseBlueprintOptions(request);

        const criteria = {
          [primaryKey]: record[primaryKey]
        };

        const originalRecord = await Model.findOne(_.cloneDeep(criteria));
        
        const valuesToSet = _.omit(record, [
          "model",
          Model.primaryKey
        ]);

        const updatedRecord = await Model.updateOne(
          _.cloneDeep(criteria),
          _.cloneDeep(valuesToSet)
        );
        updatedRecords.push(updatedRecord);

        const populatedRecord = await Model.findOne(
          _.cloneDeep(criteria),
          _.cloneDeep(queryOptions.populates)
        );
        populatedRecords.push(populatedRecord);

        if (req._sails.hooks.pubsub) {
          // // The _.cloneDeep calls ensure that only plain dictionaries are broadcast.
          // // TODO -- why is that important?
          Model._publishUpdate(
            primaryKey,
            _.cloneDeep(valuesToSet),
            !req.options.mirror && req,
            {
              previous: _.cloneDeep(originalRecord)
            }
          );
        }
      }

      // If we have the pubsub hook, use the Model's publish method
      // to notify all subscribers about the update.
      if (req._sails.hooks.pubsub && req.isSocket) {
        Model.subscribe(req, _.pluck(updatedRecords, primaryKey));
      }

      return res.ok(populatedRecords);
    } catch (error) {
      return res.badRequest(error.message);
    }
  }
};
