/**
 * ContentFileController
 *
 * @description :: Server-side logic for serving static content
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const { readFile, unlink } = require('fs');
const { promisify } = require('util');

const readFileAsync = promisify(readFile);

const { uploadFiles, indexPdf } = sails.helpers;

module.exports = {

    get: async function (req, res) {
        
        let key;
        try {
            const { path } = req;
            sails.log.info(`Content Requested: ${path}`);
            /////////////////////////////////////////////////////////////////////////
            //  Transform request path into S3 key
            /////////////////////////////////////////////////////////////////////////
            //  /content/[type]/[file]#[uuid]
            //  /[type]/[file]#[uuid]
            /////////////////////////////////////////////////////////////////////////
            key = path.split('/').filter(part => part !== 'content').join('/').substring(1);
        } catch (error) {
            sails.log.error(`Could not parse request path: ${error}`);
            return res.badRequest("Failed to parse requested file path.");
        }

        try {
            const presignedUrl = await GatewayService.createPresignedUrl(key, 'GET');
            return res.json(presignedUrl);
        } catch(error){
            sails.log.error(`Could not create presigned URL: ${error}`);
            return res.badRequest("Failed to access remote file.");
        }

    },

    list: async function(req, res) {

        try{
            const listObjects = await GatewayService.getListObjects();
            return res.json(listObjects);
        } catch(error){
            sails.log.error(error);
            return res.badRequest("Failed to obtain files uploaded to cloud storage.");
        }

    },

    put: async function(req, res) {

        let keyWithDirectory;
        try {
            const { directory, key } = req.params;
            keyWithDirectory = `${directory}/${key}`;
            sails.log.info(`Uploading file...${keyWithDirectory}`);

            const s3ObjectExists = await S3Object.count({ key: keyWithDirectory});
            if(s3ObjectExists){
                sails.log.info('S3Object with key already exists');
                return res.noContent("The requested file is already available in the Content Management System.");
            }
        } catch(error){
            sails.log.error(error);
            return res.badRequest("Invalid request.");        
        }

        let uploadedFilesMetadata;
        try { 
            // listening for file upload
            uploadedFilesMetadata  = await uploadFiles(req.file('fileToUpload')); 
            sails.log.info('uploadedFilesMetadata', uploadedFilesMetadata);
        } catch(error) {
            sails.log.error(error);
            return err.code === 'E_EXCEEDS_UPLOAD_LIMIT' ? res.payloadTooLarge("Upload limit of 500 MB exceeded.") :  res.badRequest();
        }

        for (let uploadedFileMetadata of uploadedFilesMetadata) {
            let temporaryFileBuffer, eTag, FileIndex;
            const { filename, fd: uploadedFilePath, size } = uploadedFileMetadata; 
            try {
                // uploading to s3 and indexing file
                temporaryFileBuffer = await readFileAsync(uploadedFilePath);    
                const presignedUrl = await GatewayService.createPresignedUrl(keyWithDirectory, 'PUT');
                eTag = await S3Service.putObject(filename, presignedUrl, temporaryFileBuffer);
            } catch(err){
                sails.log.error(err);
                return res.badRequest("Failed to upload file to cloud storage and/or index file.");
            } finally {
                delete presignedUrl;
                unlink(uploadedFilePath, (err) => {
                    if(err){
                        sails.log.error('Could not delete temporary file', err);
                    }
                    else {
                        sails.log.info('Temporary file deleted from server');
                    }
                });        
            }

            try {
                // indexing file
                FileIndex = await indexPdf(keyWithDirectory, temporaryFileBuffer);                    
            } catch(error){
                sails.log.error(error);
                FileIndex = '';
            } finally{
                delete temporaryFileBuffer;
            }

            try {
                // retrieving object metadata and creating S3Object
                let { "last-modified": lastModified, "content-length": contentLength } = await GatewayService.getObjectMetadata(keyWithDirectory);

                // "YYYY-MM-DDThh:mm:ss.000Z"
                lastModified = new Date(lastModified);
                lastModified = lastModified.toISOString();

                const s3Objects = await S3Object.find({ limit: 1 });

                const createdS3Object = await S3Object.create({
                    key: keyWithDirectory,
                    eTag,
                    FileIndex,
                    lastModified,  
                    size: parseInt(contentLength)
                }).fetch();
                
                const { id } = createdS3Object;
                const [ firstS3Object ] = s3Objects; // this is a hack
                
                if(firstS3Object){
                    S3Object.publish([firstS3Object.id], {
                        id,
                        verb: 'created',
                        data: createdS3Object
                    });
                }
                                                        
                return res.json(createdS3Object);

            } catch(error){
                sails.log.error(error);
                await GatewayService.deleteObject(keyWithDirectory);
                return res.badRequest("Failed to make file available in the Content Management System.");
            } finally {
                delete eTag;
                delete FileIndex;
            }
        }            
    
    },

    delete: async function(req, res) {

        let keyWithDirectory;
        try {
            const { directory, key } = req.params;
            keyWithDirectory = `${directory}/${key}`;
            sails.log.info(`Deleting file...${keyWithDirectory}`);
        } catch(error){
            sails.log.error(error);
            return res.badRequest("Invalid request.");        
        }        
                        
        try {
            await GatewayService.deleteObject(keyWithDirectory);
            
            const deletedS3Objects = await S3Object.destroy({ where: { key: keyWithDirectory }}).fetch();

            for(let deletedS3Object of deletedS3Objects){
                const { id } = deletedS3Object;

                S3Object.publish([ id ], {
                    id,
                    verb: 'destroyed'
                });
            }
            
            return res.ok({ data: deletedS3Objects});   

        } catch(error){
            sails.log.error(error);
            return res.badRequest("Failed to delete file from cloud storage.");            
        }

    }

};