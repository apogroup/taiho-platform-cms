/* https://github.com/devvid/sails-js-v1-passport-js-cheatsheet#7-add-policies */

const { get } = _;

module.exports = function (req, res, next) {

    // Sockets
    if (req.isSocket) {

        const user = get(req, ['session', 'passport', 'user'], false);

        if (user) {
            return next();
        }

        return res.forbidden();
    }
    // HTTP
    else {
        if (req.isAuthenticated()) {
            return next();
        }

        return res.redirect('/login-sso');

    }

};