const axios = require('axios');
const { stringify } = require('querystring');

const { AWSS3Bucket, AWSApiObjectPath } = sails.config;
const { parseListObjectsXml } = sails.helpers;

// Reusable preconfigured axios instance -- API Gateway
const gateway = axios.create({
    baseURL: sails.config.AWSApiRoot,
});

// Inject AWS Access Token into all requests
gateway.interceptors.request.use(config =>
    (config.headers['x-api-key'] = sails.config.AWSApiAccessToken, config)
);

const { handleAxiosError } = sails.helpers;

module.exports = {

    createPresignedUrl: async function(filePath, operation){

        sails.log.info(`Creating presigned url for: ${filePath} implementing {${operation}}...`);

        const key = decodeURIComponent(`${AWSApiObjectPath}${filePath}`) 

        const query = {
            bucket: AWSS3Bucket,
            key,
            operation
        }
        
        const queryString = `?${stringify(query)}`; // <baseURL><queryString>

        //---------------------------------------
        // [AWSApiRoot]?[query]
        //---------------------------------------
        try {
            const response = await gateway(queryString);
            return response.data.body.signed_url;
        } catch(error) {
            handleAxiosError(error);
            throw new Error('Failed to create presigned url');
        }        

    },

    getListObjects: async function () {
        
        let listObjectsXML;
        try {
            sails.log.info(`Getting bucket list from S3...`);
            const queryString = `?list-type=2&prefix=${AWSApiObjectPath}&start-after=${AWSApiObjectPath}`;
            const { data } = await gateway.get(`${AWSS3Bucket}${queryString}`);
            listObjectsXML = data["body-json"];
        } catch(error) {
            handleAxiosError(error);
            throw new Error('Failed to GET list of S3 Objects'); 
        }

        return await parseListObjectsXml(listObjectsXML);
    },


    getObjectMetadata: async function (key){

        key = key.split('/').map(encodeURIComponent).join('/');

        try {
            sails.log.info(`Getting object metadata...`);
            const { headers } = await gateway.head(`${AWSS3Bucket}/${AWSApiObjectPath}${key}`);
            return headers;             
        } catch(error) {
            handleAxiosError(error);
            throw new Error(`Failed to GET object metadata for key: ${key}`); 
        }
    },

    deleteObject: async function (key) {
        
        key = key.split('/').map(encodeURIComponent).join('/');

        try {
            return await gateway.delete(`${AWSS3Bucket}/${AWSApiObjectPath}${key}`);
        } catch(error){                
            handleAxiosError(error);
            throw new Error(`Failed to DELETE S3 Object with key: ${key}`);
        }

    }

}