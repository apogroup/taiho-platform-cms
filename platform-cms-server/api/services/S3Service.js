const { extname } = require('path');
const axios = require('axios');

// Reusable preconfigured axios instance -- S3
const s3 = axios.create();
const { maxUploadSize } = sails.config.custom;
s3.interceptors.request.use(config =>
    (config.maxContentLength = maxUploadSize, config)
);

const { handleAxiosError } = sails.helpers;

const MIMETypes = {
    '.pdf': 'application/pdf',
    '.pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    '.ppt': 'application/vnd.ms-powerpoint',
    '.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    '.doc': 'application/msword',
    '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    '.xls': 'application/vnd.ms-excel',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.jpeg': 'image/jpeg',
    '.gif': 'image/gif'
};

module.exports = {

    getObject: async function(filename, url) {

        const extension = extname(filename);
        sails.log.info('extension', extension);
        const mimeType = MIMETypes[extension];
        
        sails.log.info(`Requesting object data for: ${filename} ...`);

        const config = {             
            headers: {
            'Content-Type': mimeType
            },
            responseType: 'arraybuffer'
        }

        try {
            return await s3.get(url, config)
        } catch(error){
            handleAxiosError(error);
            throw new Error(`Failed to GET ${filename} to S3`)            
        }

    },

    putObject: async function(filename, url, buffer) {

        sails.log.info('Uploading file to S3...');
        try {
            const extension = extname(filename);
            const mimeType = MIMETypes[extension];
            const headers = { 'Content-Type': mimeType };
            const response = await s3.put(url, buffer, { headers });
            return response.headers.etag;
        } catch(error){
            handleAxiosError(error);
            throw new Error(`Failed to PUT ${filename} to S3`);
        }

    } 

}