module.exports = {

    getUser: function (req, next) {

        if (req.session && req.session.passport && req.session.passport.user) {

            User.findOne({ Username: req.session.passport.user }).exec(function (err, user) {
                if (err) throw err;
                next(user);
            });

        } else {
            next();
        }
    }

};