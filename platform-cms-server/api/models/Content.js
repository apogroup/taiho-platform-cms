/**
 * Content.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    /** ******************************** /
     ** Defaults
    /** ******************************* */

    // _id: String
    // createdAt: Date
    // updatedAt: Date

    /** ******************************** /
     ** Properties
    /** ******************************* */
    
    Descriptor: {
      type: 'string'
    },

    Route: {
      type: 'string'
    },

    Label: {
      type: 'string'
    },

    Type: {
      type: 'string'
    },

    Title: {
      type: 'string'
    },

    Body: {
      type: 'string'
    },

    Footnotes: {
      type: 'string'
    },

    Abbreviations: {
      type: 'string'
    },

    DocumentsOrder: {
      type: 'json',
      columnType: 'array'
    },

    Order: {
      type: 'number'
    },

    /** ******************************** /
     ** State
    /** ******************************* */

    Status: {
      type: 'string',
      defaultsTo: 'enabled',
      isIn: ['enabled', 'disabled', 'archived']
    },


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    /** ******************************** /
     ** One-way
     **
     ** model: one,
    /** ******************************* */

    CreatedBy: {
      model: 'user'
    },

    LastUpdatedBy: {
      model: 'user'
    },

    /** ******************************** /
     ** -to-Many
     **
     ** collection: many,
     ** via: one
    /** ******************************* */

    // History: {
    //     collection: 'Actions',
    //     via: 'Subtag'
    // },

    /** ******************************** /
     ** Many-to-Many
     **
     ** collection: many,
     ** via: many
    /** ******************************* */

    // Abbreviations: {
    //   collection: 'abbreviations',
    // },

    Documents: {
      collection: 'document',
      via: 'Contents'
    },

    References: {
      collection: 'reference',
      via: 'Contents'
    },

    Figures: {
      collection: 's3object',
      via: 'Contents'
    },

    /** ******************************** /
     ** Reflexive
     **
     ** collection: many,
     ** via: many
    /** ******************************* */

    Parents: {
      collection: 'content',
      via: 'Children'
    },

    Children: {
      collection: 'content',
      via: 'Parents'
    },

  },

};