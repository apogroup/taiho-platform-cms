/**
 * Subtag.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        /** ******************************** /
         ** Defaults
        /** ******************************* */

        // _id: String
        // createdAt: Date
        // updatedAt: Date

        /** ******************************** /
         ** Properties
        /** ******************************* */

        Subtag: {
            type: 'string'
        },

        /** ******************************** /
         ** State
        /** ******************************* */

        Disabled: {
            type: 'boolean',
            defaultsTo: false
        },


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

        /** ******************************** /
         ** One-way
         **
         ** model: one,
        /** ******************************* */
        
        Tag: {
            model: 'tag'
        },

        CreatedBy: {
            model: 'user'
        },

        LastUpdatedBy: {
            model: 'user'
        },

        /** ******************************** /
         ** One-to-Many
         **
         ** collection: many,
         ** via: one
        /** ******************************* */

        // History: {
        //     collection: 'Actions',
        //     via: 'Subtag'
        // },

        /** ******************************** /
         ** Many-to-Many
         **
         ** collection: many,
         ** via: many
        /** ******************************* */

        ReferenceVersions: {
            collection: 'referenceversion',
            via: 'Subtags'
        },

    },

};