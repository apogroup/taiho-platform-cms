/**
 * Document.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        /** ******************************** /
         ** Defaults
        /** ******************************* */

        // _id: String
        // createdAt: Date
        // updatedAt: Date

        /** ******************************** /
         ** Properties
        /** ******************************* */

        /** ******************************** /
         ** State
        /** ******************************* */

        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

        /** ******************************** /
         ** One-way
         **
         ** model: one
        /** ******************************* */

        MasterVersion: {
            model: 'documentversion'
        },

        CreatedBy: {
            model: 'user'
        },

        LastUpdatedBy: {
            model: 'user'
        },

        /** ******************************** /
         ** One-to-Many
         **
         ** collection: many,
         ** via: one
        /** ******************************* */

        /** CHILDREN */
        Versions: {
            collection: 'documentversion',
            via: 'Document'
        },

        // History: {
        //     collection: 'Actions',
        //     via: 'Document'
        // },

        /** ******************************** /
         ** Many-to-Many
         **
         ** collection: many,
         ** via: many
        /** ******************************* */

        Contents: {
            collection: 'content',
            via: 'Documents'
        },

        // Congresses: {
        //     collection: 'congress',
        //     via: 'Documents'
        // }

    },

};

