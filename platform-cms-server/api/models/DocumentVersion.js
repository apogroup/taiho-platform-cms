/**
 * DocumentVersion.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        /** ******************************** /
         ** Defaults
        /** ******************************* */

        // _id: String
        // createdAt: Date
        // updatedAt: Date

        /** ******************************** /
         ** Properties
        /** ******************************* */

        VersionId: {
            type: 'string',
        },

        Title: {
            type: 'string'
        },

        Filename: {
            type: 'string'
        },

        /** ******************************** /
         ** State
        /** ******************************* */

        Disabled: {
            type: 'boolean',
            defaultsTo: false
        },

        Cache: {
            type: 'boolean',
            defaultsTo: false
        },

        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

        /** ******************************** /
         ** One-way
         **
         ** model: one
        /** ******************************* */

        /** PARENT */
        Document: {
            model: 'document',
        },

        FilePath: {
            model: 's3object'
        },

        SourceFilePath: {
            model: 's3object'
        },

        ThumbnailPath: {
            model: 's3object'
        },

        CreatedBy: {
            model: 'user'
        },

        LastUpdatedBy: {
            model: 'user'
        },

        /** ******************************** /
         ** One-to-Many
         **
         ** collection: many,
         ** via: one
        /** ******************************* */

        // History: {
        //     collection: 'Actions',
        //     via: 'Document'
        // },

        /** ******************************** /
         ** Many-to-Many
         **
         ** collection: many,
         ** via: many
        /** ******************************* */

        References: {
          collection: 'reference',
          via: 'DocumentVersions'
        },

    },

};

