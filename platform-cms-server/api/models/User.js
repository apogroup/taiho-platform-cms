/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    // primaryKey: 'Email',

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        /** ******************************** /
         ** Defaults
        /** ******************************* */

        // _id: String
        // createdAt: Date
        // updatedAt: Date

        /** ******************************** /
         ** Properties
        /** ******************************* */

        Email: {
            type: 'string',
            unique: true
        },

        Username: {
            type: 'string'
        },

        FirstName: {
            type: 'string'
        },

        LastName: {
            type: 'string'
        },

        Groups: {
            type: 'json',
            columnType: 'array'
        },

        /** ******************************** /
         ** State
        /** ******************************* */

        Connected: {
            type: 'boolean',
            defaultsTo: false
        },

        Disabled: {
            type: 'boolean',
            defaultsTo: false
        },

        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

        /** ******************************** /
         ** One-to-Many
         **
         ** collection: many,
         ** via: one
        /** ******************************* */

        // History: {
        //     collection: 'Actions',
        //     via: 'User'
        // },

    },

    /** ******************************* */
    /*  Override global cache busting   */
    /** ******************************* */
    beforeCreate(record, proceed) {
        return proceed();
    },

    beforeUpdate(values, proceed) {
        return proceed();
    },

    beforeDestroy(criteria, proceed) {
        return proceed();
    }
    /** ******************************* */

};