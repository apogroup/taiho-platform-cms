/**
 * Congress.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
  
      //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
      //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
      //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
  
      /** ******************************** /
       ** Defaults
      /** ******************************* */
  
      // _id: String
      // createdAt: Date
      // updatedAt: Date
  
      /** ******************************** /
       ** Properties
      /** ******************************* */
      
      Event: {
        type: 'string'
      },
  
      Location: {
        type: 'string'
      },
  
      Dates: {
        type: 'string'
      },
  
      StartDate: {
        type: 'string',
        columnType: 'datetime'
      },
  
      EndDate: {
        type: 'string',
        columnType: 'datetime'
      },
  
      Specialty: {
        type: 'string',
        allowNull: true
      },
  
      Society: {
        type: 'string',
        allowNull: true
      },
  
      Attendance: {
        type: 'string',
        allowNull: true
      },
  
      Attendees: {
        type: 'string',
        allowNull: true
      },    
  
      //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
      //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
      //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
  
      /** ******************************** /
       ** One-way
       **
       ** model: one,
      /** ******************************* */
  
      CreatedBy: {
        model: 'user'
      },
  
      LastUpdatedBy: {
        model: 'user'
      },

      Image: {
        model: 's3object'
      },
      
      /** ******************************** /
       ** -to-Many
       **
       ** collection: many,
       ** via: one
      /** ******************************* */
  
      // History: {
      //     collection: 'Actions',
      //     via: 'Subtag'
      // },

      Links: {
        collection: 'link',
        via: 'Congress'
      },
  
      /** ******************************** /
       ** Many-to-Many
       **
       ** collection: many,
       ** via: many
      /** ******************************* */
  
      // Documents: {
      //   collection: 'document',
      //   via: 'Congresses'
      // },

    
    },
  
  };