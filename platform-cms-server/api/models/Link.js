/**
 * Link.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
  
      //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
      //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
      //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
  
      /** ******************************** /
       ** Defaults
      /** ******************************* */
  
      // _id: String
      // createdAt: Date
      // updatedAt: Date
  
      /** ******************************** /
       ** Properties
      /** ******************************* */
      
      Title: {
        type: 'string'
      },
  
      Tagline: {
        type: 'string'
      },
  
      Disabled: {
        type: 'boolean',
        defaultsTo: false
      },
  
      Href: {
        type: 'string'
      },
  
      Button: {
        type: 'string'
      },
    
      //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
      //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
      //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
  
      /** ******************************** /
       ** One-way
       **
       ** model: one,
      /** ******************************* */
  
      CreatedBy: {
        model: 'user'
      },
  
      LastUpdatedBy: {
        model: 'user'
      },

      Congress: {
        model: 'congress'
      }
  
      /** ******************************** /
       ** Many-to-Many
       **
       ** collection: many,
       ** via: many
      /** ******************************* */
      
    },
  
  };