/**
 * ReferenceVersion.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        /** ******************************** /
         ** Defaults
        /** ******************************* */

        // _id: String
        // createdAt: Date
        // updatedAt: Date

        /** ******************************** /
         ** Properties
        /** ******************************* */

        UniqueID: {
          type: 'number'
        },

        VersionId: {
          type: 'string',
        },
        
        Citation: {
            type: 'string'
        },

        ShortCitation: {
            type: 'string'
        },

        DisplayName: {
            type: 'string',
        },

        Title: {
            type: 'string'
        },

        Filename: {
            type: 'string'
        },

        /** ******************************** /
         ** State
        /** ******************************* */

        Annotated: {
            type: 'boolean',
            defaultsTo: false
        },

        Disabled: {
            type: 'boolean',
            defaultsTo: false
        },

        Cache: {
            type: 'boolean',
            defaultsTo: false
        },

        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

        /** ******************************** /
         ** One-way
         **
         ** model: one
        /** ******************************* */

        Reference: {
            model: 'reference'
        },
        
        CreatedBy: {
            model: 'user'
        },

        LastUpdatedBy: {
            model: 'user'
        },

        FilePath: {
            model: 's3object'
        },
        
        AnnotatedFilePath: {
            model: 's3object'
        },
        
        SourceFilePath: {
            model: 's3object'
        },

        ThumbnailPath: {
            model: 's3object'
        },

        /** ******************************** /
         ** One-to-Many
         **
         ** collection: many,
         ** via: one
        /** ******************************* */

        // History: {
        //     collection: 'Actions',
        //     via: 'Reference'
        // },

        /** ******************************** /
         ** Many-to-Many
         **
         ** collection: many,
         ** via: many
        /** ******************************* */

        Tags: {
            collection: 'tag',
            via: 'ReferenceVersions'
        },

        Subtags: {
            collection: 'subtag',
            via: 'ReferenceVersions'
        },

    },

};

