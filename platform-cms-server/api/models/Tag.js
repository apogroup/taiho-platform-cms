/**
 * Tag.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {

        //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
        //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
        //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

        /** ******************************** /
         ** Defaults
        /** ******************************* */

        // _id: String
        // createdAt: Date
        // updatedAt: Date

        /** ******************************** /
         ** Properties
        /** ******************************* */
        
        Tag: {
            type: 'string',
        },

        /** ******************************** /
         ** State
        /** ******************************* */

        Disabled: {
            type: 'boolean',
            defaultsTo: false
        },


        //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
        //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
        //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

        /** ******************************** /
         ** One-way
         **
         ** model: one
        /** ******************************* */

        CreatedBy: {
            model: 'user'
        },

        LastUpdatedBy: {
            model: 'user'
        },

        /** ******************************** /
         ** One-to-Many
         **
         ** collection: many,
         ** via: one
        /** ******************************* */

        Subtags: {
          collection: 'subtag',
          via: 'Tag'
        },

        // History: {
        //     collection: 'Actions',
        //     via: 'Tag'
        // },

        /** ******************************** /
         ** Many-to-Many
         **
         ** collection: many,
         ** via: many
        /** ******************************* */

        ReferenceVersions: {
            collection: 'referenceversion',
            via: 'Tags'
        },

    },

};

