const { maxUploadSize } = sails.config.custom;

module.exports = {


  friendlyName: 'Upload files',


  description: 'Upload files to temporary uploads directory',


  inputs: {

    upstream: {
      type: "ref",
      description: "Upstream representation of the file, from req",
      required: true
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

    uploadError: {
      description: 'No files found from upload'
    }

  },


  fn: async function (inputs, exits) {
        //-------------------------------------------------------
        // File(s) to upload: [ UploadedFileMetadata {
        //     fd: 'C:\\Users\\ldelia\\Work\\taiho\\taiho-platform-cms\\platform-cms-server\\.tmp\\uploads\\3cf65591-35a9-4192-9ab7-77e43fe48d61.pdf',
        //     size: 183401,
        //     type: 'application/pdf',
        //     filename: '008_rakel_2015.pdf',
        //     status: 'finished',
        //     field: 'fileToUpload',
        //     extra: undefined } ]
        //-------------------------------------------------------

      const uploadedFilesMetadata = await (new Promise((resolve, reject) => {

        inputs.upstream.upload({maxBytes: maxUploadSize}, function(err, uploadedFiles) {

            if(err){
                reject(err);
            }

            resolve(uploadedFiles);
        
        });
      }));

      sails.log.info('File(s) to upload:', uploadedFilesMetadata);

      if(!uploadedFilesMetadata.length){
          throw 'uploadError';
      }

      return exits.success(uploadedFilesMetadata);

  }


};

