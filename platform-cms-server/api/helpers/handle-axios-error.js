module.exports = {


  friendlyName: 'Handle axios error',


  description: 'Handle errors caught by axios',

  sync : true,

  inputs: {

    error: {

      type: 'ref',
      description: 'The error returned by axios',
      required: true 

    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: function (inputs, exits) {

    const { error } = inputs;

    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        sails.log.error(error.response.data);
        sails.log.error(error.response.status);
        // sails.log.error(error.response.headers);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        sails.log.error(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        sails.log.error('Error', error.message);
    }
    sails.log.error(error.config);

    return exits.success();

  }


};

