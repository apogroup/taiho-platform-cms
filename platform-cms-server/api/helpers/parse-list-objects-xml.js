const { AWSApiObjectPath } = sails.config;

const xml2js = require("xml2js");
const parser = new xml2js.Parser();

module.exports = {
  friendlyName: "Parse list objects xml",

  description: "Parse and return listObjects from xml string",

  inputs: {
    xml: {
      type: "string",
      description: "XML representation of GET list objects from S3",
      required: true
    }
  },

  exits: {
    success: {
      description: 'All done.'
    },

    parsingError: {
      description: 'Failed to parse XML'
    }
  },

  fn: async function(inputs, exits) {
    try {
      let listObjects = await new Promise((resolve, reject) => {
        parser.parseString(inputs.xml, (err, body) => {
          try {
            if (err) {
              throw err;
            }
            const { Contents } = body.ListBucketResult;
            resolve(Contents);
          } catch (err) {
            reject(err);
          }
        });
      });
  
        listObjects = listObjects
          // picking relevant properties
          .map(content => {
            const { ETag, Key, LastModified, Size } = content;
  
            return {
              ETag,
              Key,
              LastModified,
              Size
            };
          })
          // renaming and picking first item per property due to explicitArray: true
          .map(({ ETag, Key, LastModified, Size }) => {
            const [eTag] = ETag;
            const [key] = Key;
            const [lastModified] = LastModified;
            const [size] = Size;
  
            return {
              eTag,
              key,
              lastModified,
              size
            };
          })
          // transforming values per S3Object schema
          .map(content => {
            let { key, size } = content;
  
            // API Gateway returns full object path, i.e. <folder>/<subfolder>/<directory>/<key>
            key = key.replace(decodeURIComponent(AWSApiObjectPath), "");
  
            // parseString() returns string, need to return Int for size
            size = parseInt(size);
  
            return {
              ...content,
              key,
              size
            };
          })
  
        return exits.success(listObjects);  
    } catch(error){
      sails.log.error(error);
      throw 'parsingError';
    }
  }
};
