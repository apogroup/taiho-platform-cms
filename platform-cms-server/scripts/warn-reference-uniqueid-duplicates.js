const {
  load
} = require('cheerio');

module.exports = {

  friendlyName: 'Find UniqueID Duplicates Reference',
  description: '',

  fn: async function (inputs, exits) {

    try {

      /////////////////////////////////////////////////////////////////////////
      // Each Reference find UniqueID duplicates
      /////////////////////////////////////////////////////////////////////////

      console.log('\n\n/////////////////////////////////////////////////////////////////////////////////\n');
      console.log('    FIND AND WARN Reference WITH UniqueID DUPLICATION');
      console.log('\n/////////////////////////////////////////////////////////////////////////////////\n');

      let refUniqueIdDuplicates = {};

      await Reference.stream()
        .eachRecord(async (reference, proceed) => {

          const {
            id,
            UniqueID
          } = reference;

          // console.log(`\nObjectId:    ${ id }`);
          //-------------------------------------------------------------------
          // Look at each record and try to find duplicates by UniqueID
          //-------------------------------------------------------------------

          try {

            //-------------------------------------------------------------------
            // Match by UniqueID 
            //-------------------------------------------------------------------
            const matchesByUniqueID = await Reference.find({
              UniqueID: UniqueID,
              id: {
                '!=': id
              }
            });

            if (matchesByUniqueID.length <= 0) {

              return proceed();

            } else {
              console.log('\n-----------------------------------------------------------------------------\n');
              sails.log.warn(`Reference ${ id } UniqueID is duplicated in another Reference`);

              if (refUniqueIdDuplicates[UniqueID]) {
                refUniqueIdDuplicates[UniqueID] = [...refUniqueIdDuplicates[UniqueID], ...matchesByUniqueID];
              } else {
                refUniqueIdDuplicates[UniqueID] = [...matchesByUniqueID];
              }
            }

          } catch (err) {

            sails.log.error(`Failed comparing Reference document ${id} ${UniqueID}`);
            return exits.error(err);

          }

          return proceed();
        });

      console.log('\n\n\n-----------------------------------------------------------------------------\n');
      sails.log.info(`UniqueID Duplication (Reference references)`);
      console.log('\n-----------------------------------------------------------------------------\n');
      sails.log.debug(refUniqueIdDuplicates);
      console.log('\n-----------------------------------------------------------------------------\n');


    } catch (err) {

      return exits.error(err);

    }

    return exits.success();

  }



};
