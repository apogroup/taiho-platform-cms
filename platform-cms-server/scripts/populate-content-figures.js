const { load } = require('cheerio');

module.exports = {

	friendlyName: 'Populate content figures',

	description: '',

	fn: async function (inputs, exits) {

		try {

			/////////////////////////////////////////////////////////////////////////
			// Find all Content
			/////////////////////////////////////////////////////////////////////////

			await Content.stream()
				.eachRecord(async ({ id, Body }, proceed) => {

					sails.log.info(`Populating figures: ${id}`);

					//-------------------------------------------------------------------
					// Initialize S3Object id Array 
					//-------------------------------------------------------------------
					let s3objectObjectIds = [];

					//-------------------------------------------------------------------
					// Try to parse HTML with Cherrio
					// Select each div with class 'figure-preview'
					// Get Figure S3Object Object Id from data-figure attribute
					//-------------------------------------------------------------------

					try {
						//-------------------------------------------------------------------
						// Load HTML from Content document Body property
						//-------------------------------------------------------------------
						const $ = load(Body);

						//-------------------------------------------------------------------
						// Map Body for figure-preview elements
						// Get id from data-figure attribute
						// Reduce to remove duplicates
						//-------------------------------------------------------------------
						s3objectObjectIds = await $('div.figure-preview')
							.map((i, element) => $(element).attr('data-figure'))
							.get()
							.reduce((s3objects, s3object) =>
								s3objects.includes(s3object) ? s3objects : s3objects.concat(s3object),
								[]
							);

						sails.log.info('Figures S3Object documents');
						sails.log.debug(s3objectObjectIds);

						//-------------------------------------------------------------------
						// Now update the Content document with figures Array
						//-------------------------------------------------------------------
						let updatedContent = await Content.updateOne({ _id: id }, {
								Figures: [...s3objectObjectIds]
							});

						if (updatedContent) {
							sails.log('Updated Content: ', updatedContent);
						}

					} catch (err) {
						sails.log.error('Failed to parse Figures from content and update content');
						return exits.error(err);
					}

					return proceed();
				})
		} catch (err) {
			return exits.error(err);
		}

		return exits.success();

	}

};

