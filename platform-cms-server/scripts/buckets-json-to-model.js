const { promisify } = require('util')
const fs = require('fs');
const readFile = promisify(fs.readFile);
const stat = promisify(fs.stat);

const { capitalize, chain, reduce } = require('lodash');

module.exports = {


  friendlyName: 'Buckets json to model',


  description: 'Insert records into database using buckets.json',


  inputs: {
    filepath: {
      description: 'Path to buckets.json file from a given platform',
      type: 'string',
      required: true
    }
  },


  fn: async function ({ filepath }, exits) {

    let buckets;


    /////////////////////////////////////////////////////////////////////////
    // Attempt to read and parse JSON file from supplied path
    /////////////////////////////////////////////////////////////////////////
    try {

      sails.log('Checking if path is a file...');
      const fileStat = await stat(filepath);
      if (!fileStat.isFile()) {
        throw new Error('Path does not point to a file');
      }

      sails.log('Reading file...');
      const bucketsJSON = await readFile(filepath, { encoding: 'utf8' });

      sails.log('Parsing JSON from file...');
      buckets = JSON.parse(bucketsJSON);

    } catch (error) {

      sails.log.error(`Error reading JSON from supplied file path: ${filepath}`);
      return exits.error(error);

    }

    /////////////////////////////////////////////////////////////////////////
    // Transform JSON object into array of buckets
    /////////////////////////////////////////////////////////////////////////
    //
    // { 
    //   [key: string]: {
    //     [title: string]: string,
    //     [description: string]: string,
    //     [route: string]: string,
    //     [icon: string]: string,
    //     [sidebar: string]: object[],
    //   }
    // } => Array<{
    //   [Key: string]: string,
    //   [Title: string]: string,
    //   [Description: string]: string,
    //   [Route: string]: string,
    //   [Sidebar: string]: object[]
    // }>
    //
    /////////////////////////////////////////////////////////////////////////
    try {

      buckets = reduce(buckets, (buckets, bucket, Key) => {

        const fields = chain(bucket)
          .pick(
            ['title', 'route', 'description', ...(Key === 'scientific-lexicon' ? ['sidebar'] : [])]
          )
          .mapKeys((field, key) => {
            return capitalize(key);
          });

        buckets.push({
          ...fields.value(),
          Key
        });

        return buckets;

      }, []);

    } catch (error) {

      sails.log.error('Could not create Bucket from JSON entry');
      return exits.error(error);

    }

    /////////////////////////////////////////////////////////////////////////
    // Add buckets to collection that do not already exist
    /////////////////////////////////////////////////////////////////////////
    try {

      for (let bucket of buckets) {

        const { Key } = bucket;

        sails.log(`Adding bucket to collection: ${Key}...`);

        const bucketExists = await Bucket.count({ Key }) > 0;

        if (bucketExists) {

          sails.log.warn('Skipping bucket because it already exists');

        } else {

          await Bucket.create(bucket);
          sails.log('Bucket added to collection');

        }

      }

    } catch (error) {

      sails.log.error('Could not add buckets to collection');
      return exits.error(error);

    }

    return exits.success();


  }


};

