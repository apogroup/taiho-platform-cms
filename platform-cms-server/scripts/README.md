# Associate ReferenceVersion to S3Object

1. Navigate to platform CMS dashboard's Files tab to populate S3Object collection in Mongo
2. In Compass, export ReferenceVersion collection to JSON
4. Navigate to ```platform-cms-server```
3. Run ```sails run associate-s3-objects --dump=[path/to/dump/json] ```

# Populate Content records with References

## Preface
1. Open MongoDB Shell (```mongo```)
2. Select appropriate database (for example, ```use taiho_platform_cms```)
3. Unset 'References' property on all Content documents with ```db.content.updateMany({}, { $unset: { References: '' } });```

1. Navigate to ```platform-cms-server```
2. Run ```sails run populate-content-references```


# Set Content Superscript Ref Attr
Find all Content documents, parses properties to HTML, find superscript type reference and update with attributes with Reference metadata, update Content document

1. Navigate to ```platform-cms-server```
2. Run ```sails run set-content-superscript-ref-attr```


# Clean S3Objects, Remove S3Objects with no associations, and WARN where key duplication

1. Navigate to ```platform-cms-server```
2. Run ```sails run clean-s3object-warn-duplicates```

# Ingest buckets.json and add to collection

1. Navigate to ```platform-cms-server```
2. Run ```sails run buckets-json-to-model --filepath=[/path/to/buckets/json]```