const { load } = require('cheerio');

module.exports = {

	friendlyName: 'Set Content superscript Reference attributes',
	description: '',

	fn: async function (inputs, exits) {

		try {


			/////////////////////////////////////////////////////////////////////////
			// Find all Reference
			/////////////////////////////////////////////////////////////////////////
			
			let references = await Reference.find();

			/////////////////////////////////////////////////////////////////////////
			// Stream Content Documents and Update for eachRecord
			/////////////////////////////////////////////////////////////////////////
			await Content.stream()
				.eachRecord(async ({ id, Descriptor, Title, Body, Footnotes, Abbreviations }, proceed) => {

					sails.log.info(`Updating Content Document: ${id} ${Descriptor}`);

					//-------------------------------------------------------------------
					// Try to parse HTML with Cherrio
					// Update attributes of each superscript
					// Update Content document with new HTML
					//-------------------------------------------------------------------

					try {
						//-------------------------------------------------------------------
						// Load HTML from Content document properties
						//-------------------------------------------------------------------
						
						let $Title 			= load(Title);
						let $Body  			= load(Body);
						let $Footnotes 		= load(Footnotes);
						let $Abbreviations	= load(Abbreviations);

						//-------------------------------------------------------------------
						// Use Cherrio to get Array of superscript elements in loaded HTML
						//-------------------------------------------------------------------
						
						let title 			= $Title('sup[type="reference"]');
						let body 			= $Body('sup[type="reference"]');
						let footnotes 		= $Footnotes('sup[type="reference"]');
						let abbreviations 	= $Abbreviations('sup[type="reference"]');

						//-------------------------------------------------------------------
						// Helper Method: Pair superscript text (ref uid), find Reference 
						// and update superscript attributes
						//-------------------------------------------------------------------
						
						let superscriptParser = ($, element) => {
							let elementText = $(element).text();
							let uniqueId = parseInt(elementText);

							if (Number.isNaN(uniqueId)) {
								throw new Error(`Reference superscript does not contain a number (${uniqueId})`);
							}

							let reference = references.find((ref) => ref.UniqueID == uniqueId);

							if (!reference) {
								throw new Error(`Reference with unique id ${uniqueId}`);
							}

							return $(element)
							  .attr('data-reference-uid', `${uniqueId}`)
							  .attr('data-reference', `${reference.id}`)
							  .removeAttr('title');
						}

						//-------------------------------------------------------------------
						// Iterate over superscripts and assign attributes from 
						// paired Reference for each property
						//-------------------------------------------------------------------
						
						let parsedTitle = await title.map((i, element) => {
						  return superscriptParser($Title, element);
						});

						let parsedBody = await body.map((i, element) => {
						  return superscriptParser($Body, element);
						});

						let parsedFootnotes = await footnotes.map((i, element) => {
						  return superscriptParser($Footnotes, element);
						});

						let parsedAbbreviations = await abbreviations.map((i, element) => {
						  return superscriptParser($Abbreviations, element);
						});

						//-------------------------------------------------------------------
						// Now update the Content document
						//-------------------------------------------------------------------
						let updatedContent = await Content.updateOne({ _id: id }, {
								Title: $Title('body').html(),
								Body: $Body('body').html(),
								Footnotes: $Footnotes('body').html(),
								Abbreviations: $Abbreviations('body').html()
							});

						if (updatedContent) {
							console.log('Updated Content: ', updatedContent);
						}


					} catch (err) {

						sails.log.error('Failed updating content');
						return exits.error(err);
					
					}

					return proceed();
				});

		} catch (err) {

			return exits.error(err);

		}

		return exits.success();

	}


};

