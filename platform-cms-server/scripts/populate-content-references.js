const { load } = require('cheerio');

module.exports = {


  friendlyName: 'Populate content references',


  description: '',


  fn: async function (inputs, exits) {

    try {
      await Content.stream({
        Type: 'substatement'
      })
        .eachRecord(async ({ id, Title, Body, Footnotes }, proceed) => {

          sails.log.info(`Populating substatement references: ${id}`);

          let UniqueIDs = [];
          try {
            // Parse HTML with Cheerio
            const $ = load([Title, Body, Footnotes].join(''));

            // Map superscript tags to reference numbers
            UniqueIDs = $('sup[type="reference"]')
              .map((i, element) =>
                $(element).text()
              )
              .get()
              .map(reference => {
                const referenceNumber = parseInt(reference);

                if (Number.isNaN(referenceNumber)) {
                  throw new Error('Reference is not a number');
                } else {
                  return referenceNumber;
                }

              })
              // Remove duplicates
              .reduce((references, reference) =>
                references.includes(reference) ? references : references.concat(reference),
                []
              )

          } catch (err) {
            sails.log.error('Failed to parse References from substatement');
            return exits.error(err);
          }

          try {

            // UniqueID[] => Promise<Reference>[]
            References = UniqueIDs.map(UniqueID => Reference.findOne({ UniqueID }));

            // Promise<Reference[]> => ObjectID[]
            References = (await Promise.all(References))

            if (References.includes(undefined)) {
              notFound = UniqueIDs.filter((_, index) => References[index] === undefined);
              throw new Error(`Could not find matching Reference${notFound.length > 1 ? 's' : ''} in database: ${notFound}`);
            } else {
              References = References.map(({id}) => id);
            }

          } catch (err) {
            sails.log.error('Could not fetch matching Reference');
            return exits.error(err);
          }

          if (References.length) {
            sails.log.info(`References found: [${References}]`);
          } else {
            sails.log.info(`No References Found`);
          }

          sails.log.info(`Updating substatement`);

          try {
            await Content.updateOne({
              id
            }).set({
              References
            });
          } catch (err) {
            sails.log.error('Could not write References to substatement');
            return exits.error(err);
          }

          return proceed();
        })
    } catch (err) {
      return exits.error(err);
    }

    return exits.success();

  }


};

