
const util = require('util');

const fs = require('fs');
const readFile = util.promisify(fs.readFile);

module.exports = {


  friendlyName: 'Associate ReferenceVersions with S3Objects',


  description: '',


  inputs: {
    dump: {
      description: 'Path to JSON file exported from Compass',
      type: 'string',
    }
  },


  fn: async function ({ dump }, exits) {

    try {
      var json = await readFile(dump);
    } catch (err) {
      sails.log.error(`Cannot open file: ${dump}`);
      return exits.error(err);
    }

    try {
      var referenceVersions = JSON.parse(json)
    } catch (err) {
      sails.log.error(`Cannot parse JSON: ${dump}`);
      json.destroy(err);
      return exits.error(err);
    }

    try {
      // Flatten Object into key for lookup
      referenceVersions = referenceVersions.map(version => ({
        ...version,
        FilePath: version.FilePath.key,
        AnnotatedFilePath: version.AnnotatedFilePath.key,
        ThumbnailPath: version.ThumbnailPath.key
      }))
        // ThumbnailPath: Clean out null or {} values that may have existed
        .map(version => {
          if (version.ThumbnailPath && version.ThumbnailPath.key) {
            return version;
          } else {
            delete version.ThumbnailPath;
            return version;
          }
        })
        // Pull out ObjectID and remove Compass encoding
        .map(version => {
          const id = version['_id']['$oid'];
          version.id = id;
          delete version['_id'];
          return version;
        })
    } catch (err) {
      sails.log.error(`Cannot process file: ${dump}`);
      json.destroy(err);
      return exits.error(err);
    }


    // Manually associate ReferenceVersion to S3Objects by ObjectID
    for await (let element of referenceVersions) {
      const { id, FilePath, AnnotatedFilePath, ThumbnailPath } = element;

      try {

        let valueToSet = {
            FilePath: null,
            AnnotatedFilePath: null,
            ThumbnailPath: null,
            SourceFilePath: null
        }

        // FilePath
        let filePath = FilePath ? await S3Object.findOne({
          key: FilePath
        }) : undefined;

        valueToSet.FilePath = filePath && filePath.id ? filePath.id : null;

        // AnnotatedFilePath
        let annotatedFilePath = AnnotatedFilePath ? await S3Object.findOne({
          key: AnnotatedFilePath
        }) : undefined;

        valueToSet.AnnotatedFilePath = annotatedFilePath && annotatedFilePath.id ? annotatedFilePath.id : null;

        // ThumbnailPath
        let thumbnailPath = ThumbnailPath ? await S3Object.findOne({
          key: ThumbnailPath
        }) : undefined;

        valueToSet.ThumbnailPath = thumbnailPath && thumbnailPath.id ? thumbnailPath.id : null;

        // Set references to S3Objects
        const referenceVersion = await ReferenceVersion.updateOne({
          id
        }).set(valueToSet);

        sails.log.info(`Updated ReferenceVersion: ${referenceVersion.id}`);

      } catch (err) {
        sails.log.error(`Could not update ReferenceVersions`);
        json.destroy(err);
        return exits.error(err);
      }

    };

    // console.log(referenceVersions);
    return exits.success();

  }


};

