const {
  load
} = require('cheerio');

module.exports = {

    friendlyName: 'Clean S3Objects & Remove S3Objects with No Associations & Find Key Duplicates S3Object',
    description: '',

    fn: async function (inputs, exits) {

    try {

        // /////////////////////////////////////////////////////////////////////////
        // // Find all S3Object Documents
        // /////////////////////////////////////////////////////////////////////////

        // let s3objects = await S3Object.find();

        /////////////////////////////////////////////////////////////////////////
        // 1 . Each S3Objet:  Unset eTag
        /////////////////////////////////////////////////////////////////////////

        console.log('\n/////////////////////////////////////////////////////////////////////////////////\n');
        console.log('    UNSET S3Object eTag');
        // console.log(`    S3Object Collection Size: ${ s3objects.length }`);
        console.log('\n/////////////////////////////////////////////////////////////////////////////////\n');

        await S3Object.stream()
           .eachRecord(async (document, proceed) => {

                const { id, key, eTag } = document;

                // console.log(`\nObjectId:    ${ id }`);
                try {

                    let updatedS3Object = await S3Object.updateOne({ id: id }, { eTag: '' });

                    if(updatedS3Object){
                        sails.log.info('Unset eTag of S3Object', id );
                    } else {
                        sails.log.error('Error updating eTag of S3Object', id);
                    }

                } catch (err) {

                    sails.log.error(`Failed unsetting S3Object document ${id} ${key}`);
                    return exits.error(err);

                }

                return proceed();
        });

        /////////////////////////////////////////////////////////////////////////
        // 2. Destroy S3Object where no associations in DocumentVersion or ReferenceVersion
        /////////////////////////////////////////////////////////////////////////

        console.log('\n\n/////////////////////////////////////////////////////////////////////////////////\n');
        console.log('    DESTROY S3Object WITH NO ASSOCIATIONS IN DocumentVersion or ReferenceVersion OR IN S3Object.Contents');
        // console.log(`    S3Object Collection Size: ${ s3objects.length }`);
        console.log('\n/////////////////////////////////////////////////////////////////////////////////\n');

        let destroyS3ObjectQueue = []; // stream chunks the response in 30 so create and delete operations need to happen post

        await S3Object.stream().populate('Contents')
        .eachRecord(async (document, proceed) => {

            const { id, key, eTag, Contents } = document;

            // console.log(`\nObjectId:    ${ id }`);

            //-------------------------------------------------------------------
            // Look at each record and try to find duplicates by key and eTag
            //-------------------------------------------------------------------

            try {

                const referenceversions = await ReferenceVersion.find({}).where({
                    or: [
                        { FilePath: id },
                        { AnnotatedFilePath: id },
                        { SourceFilePath: id },
                        { ThumbnailPath: id }                        
                    ]
                });

                const documentversions = await DocumentVersion.find({}).where({
                    or: [
                        { FilePath: id },
                        { SourceFilePath: id },
                        { ThumbnailPath: id }                        
                    ]
                });

                console.log('\n-----------------------------------------------------------------------------\n');
                if(referenceversions.length <= 0 && documentversions.length <= 0 && Contents.length <= 0) {

                    sails.log.warn(`Queue Destroy S3Object with no associations: ${ id }`);

                    destroyS3ObjectQueue.push(id);

                    // const destroyedS3Object = await S3Object.destroyOne({ id: id });

                    // if(destroyedS3Object) {
                    //     sails.log.warn(`Destroyed S3Object: ${ id }`);
                    // } else {
                    //     sails.log.error(`Cannot destroy S3Object that does not exist: ${ id }`);
                    // }

                } else {
                    sails.log.info(`Skipping S3Object with associations: ${ id }`);
                    console.log(`        ReferenceVersion Associated: ${ referenceversions.length }`);
                    console.log(`        DocumentVersion Associated: ${ documentversions.length }`);
                    console.log(`        Contents Associated (by Figures): ${ Contents.length }`);
                }

                console.log('\n');

                // console.log('\n---------------------------------------------------------------------------**\n\n');
            
            } catch (err) {

                sails.log.error(`Failed destroy S3Object document with no associations ${id} ${key}`);
                return exits.error(err);

            }

            return proceed();
        });

        if(destroyS3ObjectQueue.length >= 1) {
            console.log('\n-----------------------------------------------------------------------------');
            console.log(`Destroying queue of S3Object documents with no associations: ${ destroyS3ObjectQueue.length }`);
            console.log('-----------------------------------------------------------------------------\n');

            for(let s3objectId of destroyS3ObjectQueue) {
                
                const destroyedS3Object = await S3Object.destroyOne({ id: s3objectId });

                if (destroyedS3Object) {
                    sails.log.warn(`Destroyed S3Object: ${ s3objectId }`);
                } else {
                    sails.log.error(`Cannot destroy S3Object that does not exist: ${ s3objectId }`);
                }
            
            }
        }


        /////////////////////////////////////////////////////////////////////////
        // 3. Each S3Object find key duplicates
        /////////////////////////////////////////////////////////////////////////

        console.log('\n\n/////////////////////////////////////////////////////////////////////////////////\n');
        console.log('    FIND AND WARN S3Object WITH key DUPLICATION');
        // console.log(`    S3Object Collection Size: ${ s3objects.length }`);
        console.log('\n/////////////////////////////////////////////////////////////////////////////////\n');

        let s3objKeyDuplicates = {};

        await S3Object.stream()
        .eachRecord(async (document, proceed) => {

            const { id, key, eTag } = document;

            // console.log(`\nObjectId:    ${ id }`);
            //-------------------------------------------------------------------
            // Look at each record and try to find duplicates by key and eTag
            //-------------------------------------------------------------------

            try {

                //-------------------------------------------------------------------
                // Match by Key 
                //-------------------------------------------------------------------
                // const matchesByKey = s3objects.filter((item) => item.key !== '' && item.key == key).filter((item) => item.id !== id);
                const matchesByKey = await S3Object.find({ 
                    key: key, 
                    id: { '!=' : id }
                });

                if (matchesByKey.length <= 0) {

                    return proceed();

                } else {
                    console.log('\n-----------------------------------------------------------------------------\n');
                    sails.log.warn(`S3Object ${ id } key is duplicated in another S3Object`);
                    // console.log('\n-----------------------------------------------------------------------------\n');
                    // sails.log.warn(document);
                    // console.log('\n-----------------------------------------------------------------------------\n');
                    // sails.log([...matchesByKey]);
                    // console.log('\n---------------------------------------------------------------------------**\n');
                    // console.log('\n\n\n');

                    if (s3objKeyDuplicates[key]) { 
                        s3objKeyDuplicates[key] = [...s3objKeyDuplicates[key], ...matchesByKey];
                    } else {
                        s3objKeyDuplicates[key] = [...matchesByKey];
                    }
                }
            
            } catch (err) {

                sails.log.error(`Failed comparing S3Object document ${id} ${key}`);
                return exits.error(err);

            }

            return proceed();
        });

        console.log('\n\n\n-----------------------------------------------------------------------------\n');
        sails.log.info(`Key Duplication (S3Object references)`);
        console.log('\n-----------------------------------------------------------------------------\n');
        sails.log.debug(s3objKeyDuplicates);
        console.log('\n-----------------------------------------------------------------------------\n');


    } catch (err) {

        return exits.error(err);

    }

    return exits.success();

    }



};
