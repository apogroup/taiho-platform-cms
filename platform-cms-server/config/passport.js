//--------------------------------
// Require PASSPORT
//--------------------------------

var passport = require('passport'),
    SamlStrategy = require('passport-saml').Strategy;

//--------------------------------
// SAML ASSERTION(S) CONFIG
//--------------------------------

var OKTA;

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    OKTA = {
        saml: {
            path: '/login/sso',
            entryPoint: 'https://dev-429449.oktapreview.com/app/apothecom_localhostdeveloper_1/exkdy0y0bv6PYsJJz0h7/sso/saml',
            issuer: 'http://www.okta.com/exkdy0y0bv6PYsJJz0h7',
            cert: 'MIIDpDCCAoygAwIBAgIGAV2Tx75sMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi00Mjk0NDkxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwNzMwMTM1NDI3WhcNMjcwNzMwMTM1NTI3WjCBkjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtNDI5NDQ5MRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1SxbVBF3VWnDtnBbylxSd+0hIhXGmJrNfvV+FQXlR2uQqu2wqy4xLB12lF+fO3HLki8nb5xXqGvhfXn5trSk7ENt+qOWjdiYNJm7/mwhdyPZxtzJxJF5cizw+59FYLpxSVLfsGSMeFLgBKAWTrp0D1Z9lXMDUMhGbgX+/R5CmzpCea9pTM2UGtX2T/SJa4h/ohyqM06H5OREcziN5OhHbmqLxDFxzvZ3ytPE23thoQ4G9gSXG2b2XrYVLk9u9/7yhscOgsvxnNgqNIYATD4cN0keXoicm2DdpVEsz+aoP4ZlLbYto/WhdlMMgRG+RwuB4BlIxBusL6BGPqWGz03fcwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAYGBJ+5z5E+p6bvqiW/VIPQ68BFmTnyRnSAi80WXIX7xUS5/fHZ76rR13qtuhQ5V9D1TYYwXgOmrgHirPe3y1xr1wWQOUBec05rPMFsM4QBbUcBgl20EM//1jrG1sSMawG/N1BdJNQepdV0uSt3JX/jSnuVDbw6HJtRwbwlmm5q1ImmJYHdyDDX9WoF1UMwV5Hc2fFCQOA4RPZuf+Hz8hMtSFG5VhiKiUeLu+SHky7GU2iFKRTgBPONIbJmchiHDCsa80QNapHYchNjWqV32iInjYfT+ecgrE4BW0EQY0mxM8UwboMej1gYF7wGvVz4xPhUE0RvYV81Ii3DLSgOqWf'
        }
    }
}

if (process.env.NODE_ENV === 'production') {
    OKTA = {
    	 saml: {
    		 path: '/login/sso',
    		 entryPoint: '',
    		 issuer: '',
    		 cert: ''
    	 }
    }
}



//--------------------------------
// SAML SSO Strategy API
//--------------------------------
passport.serializeUser(function (assertion, done) {
    // sails.log('Serializing assertion: ', assertion);
    done(null, assertion.username);
});
passport.deserializeUser(function (username, done) {
    // sails.log('Deserializing assertion', username);
    User.findOne({ Username: username }, function (err, user) {
        if (err) {
            sails.log.error(err);
        }
        // sails.log('Deserialized user: ', user);
        done(err, user);
    });
});

passport.use('okta-saml', new SamlStrategy({
    path: OKTA.saml.path,
    entryPoint: OKTA.saml.entryPoint,
    issuer: OKTA.saml.issuer,
    cert: OKTA.saml.cert,
    authnContext: 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransportSSO'
},
    function (assertion, done) {
        // sails.log('SAML Assertion', assertion);
        return done(null, assertion);
    })
);