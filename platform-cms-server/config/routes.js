/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  //-------------------------------------
  // Bootstrap Single Page Angular Apps
  //-------------------------------------

  'GET /': 'BootstrapController.index',

  'GET /dashboard': 'BootstrapController.index',

  //-------------------------------------
  // Login/Logout Auth
  //-------------------------------------

  'POST /login/sso': 'AuthController.auth',

  'GET /login': 'AuthController.login',

  'GET /logout': 'AuthController.logout',

  // LOGIN Splash Page, authenticated: false
  'GET /login-sso': 'AuthController.auth',

  'GET /session-user': 'UserController.getUser',

  //-------------------------------------
  // Content API (Policy Protected Files)
  //-------------------------------------

  'GET /content/documents/*': 'ContentFileController.get',
  'GET /content/images/*': 'ContentFileController.get',
  'GET /content/references/*': 'ContentFileController.get',
  
  // 'GET /content/*': 'ContentFileController.get',

  'GET /content/files': 'ContentFileController.list',

  'PUT /content/:directory/:key': 'ContentFileController.put',

  'DELETE /content/:directory/:key': 'ContentFileController.delete',

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/

  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝

  // 'POST /user/connect': 'UserController.connect',
  'PATCH /:model': 'BlueprintController/updateMany',

  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝
  'GET /s3object/generate-index': 'S3ObjectController.getS3ObjectIndex'

};
